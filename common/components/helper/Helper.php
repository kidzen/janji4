<?php

namespace common\components\helper;

use Yii;

/**
 * This is the model class for table "cases".
 */
class Helper
{

    public static function RomanConverter($integer, $upcase = false, $reverse = false)
    {
        if (!$reverse) {
            $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
            $return = '';
            while($integer > 0)
            {
                foreach($table as $rom=>$arb)
                {
                    if($integer >= $arb)
                    {
                        $integer -= $arb;
                        $return .= $rom;
                        break;
                    }
                }
            }
            if(!$upcase) {
                $return = strtolower($return);
            }
        }

        return $return;
    }
    public static function TimeAgo2($time)
    {
        $time1 = new \DateTime($time);
        $time2 = new \DateTime(date('Y-m-d H:i:s'));
        $interval = $time1->diff($time2);

        $d = $interval->format('%d');
        $m = $interval->format('%m');
        $y = $interval->format('%y');
        $h = $interval->format('%h');
        $i = $interval->format('%i');
        $s = $interval->format('%s');
        $tense = '..about ';
        $counter = 0;
        if($y > 0) {
            $tense .= $y . ' year(s) ';
            $counter++;
        }
        if($m > 0 && $counter<1) {
            $tense .= $m . ' month(s) ';
            $counter++;
        }
        if($d > 0 && $counter<1) {
            $tense .= $d . ' day(s) ';
            $counter++;
        }
        if($h > 0 && $counter<1) {
            $tense .= $h . ' hour(s) ';
            $counter++;
        }
        if($i > 0 && $counter<1) {
            $tense .= $i . ' minute(s) ';
            $counter++;
        }
        if($s > 0 && $counter<1) {
            $tense .= $s . ' second(s) ';
            $counter++;
        }
        return $tense . ' ago';
    }
    public static function TimeAgo($time)
    {
        $time_difference = time() - $time;
        if( $time_difference < 1 ) { return 'kurang dari 1 saat lepas'; }
        $condition = array( 12 * 30 * 24 * 60 * 60 =>  'tahun',
                    30 * 24 * 60 * 60       =>  'bulan',
                    24 * 60 * 60            =>  'hari',
                    60 * 60                 =>  'jam',
                    60                      =>  'minit',
                    1                       =>  'saat'
        );
        foreach( $condition as $secs => $str )
        {
            $d = $time_difference / $secs;
            if( $d >= 1 )
            {
                $t = round( $d );
                // return '' . $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' yang lalu';
                return '' . $t . ' ' . $str . ' yang lalu';
            }
        }
    }

    public static function nicetime($date)
    {
        if(empty($date)) {
            return "No date provided";
        }

        $periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths         = array("60","60","24","7","4.35","12","10");

        $now             = time();
        // $unix_date         = strtotime($date);
        $unix_date         = $date;

           // check validity of date
        if(empty($unix_date)) {
            return "Bad date";
        }

        // is it future date or past date
        if($now > $unix_date) {
            $difference     = $now - $unix_date;
            $tense         = "ago";

        } else {
            $difference     = $unix_date - $now;
            $tense         = "from now";
        }

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if($difference != 1) {
            $periods[$j].= "s";
        }

        return "$difference $periods[$j] {$tense}";
    }
}



