<?php

namespace common\components;

use Yii;

/**
 * This is the model class for table "cases".
 */
class DateOfDay
{
	public $year = 2017;
	public  $month = 6;
	public $day = 'Monday';
	// public $daysError = 3;
	// public static function SingleDay($year,$month,$day){
	public  function SingleDay($year,$month,$day){

		$dateString = 'first '.$day.' of '.$year.'-'.$month;
		if (!strtotime($dateString)) {
			throw new \Exception('"'.$dateString.'" is not a valid strtotime');
		}

		$startDay = new \DateTime($dateString);
	    // if ($startDay->format('j') > $daysError) {
	    //     $startDay->modify('- 7 days');
	    // }
		$days = array();
		while ($startDay->format('Y-m') <= $year.'-'.str_pad($month, 2, 0, STR_PAD_LEFT)) {
			$dayDate[] = $startDay->format('j');
			$days[] = clone($startDay);
			$startDay->modify('+ 7 days');
		}
		return $dayDate;
	}

	public function MultipleDay($year,$month,$days[]){
		foreach($days as $day) {
			$this->singleDay()
		}
	}
}
