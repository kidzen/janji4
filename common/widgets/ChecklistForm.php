<?php
namespace common\widgets;

use Yii;
// use kartik\grid\GridView;
// use yii\data\ArrayDataProvider;
use common\components\helper\Helper;
use yii\helpers\Html;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class ChecklistForm extends \yii\bootstrap\Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */

    /**
     * @var array the options for rendering the close button tag.
     */

    const TYPE_FORM = 'form';
    const TYPE_LIST = 'list';

    public $type = self::TYPE_FORM;
    public $array = [];
    public $data = [];


    public function init()
    {
        \frontend\assets\ChecklistFormAsset::register($this->getView());
        parent::init();

        $this->renderTableHeader();
        if($this->type === self::TYPE_FORM) {
            $this->renderListTable(\common\models\DocumentChecklistLookup::getChecklist());
        } else if($this->type === self::TYPE_LIST) {
            $this->renderListTable(\common\models\DocumentChecklistLookup::getChecklist(),$disable = true);
        } else {
            echo 'Jadual tidak dapat dijana.';
        }

        $this->renderTableFooter();
    }
    public function run(){
        if($this->type === self::TYPE_FORM){
            return Html::tag('div',Html::button('Hantar',['class'=>'btn btn-success','type'=>'submit']));
        }
    }

    private function renderListTable($array,$disable = false) {
        // $disableAttributes = '';
        $isCheck0 = 'checked';
        $isCheck1 = '';
        $isCheck2 = '';

        $disable ? $disableAttributes = 'disabled' : $disableAttributes = '';
        foreach($array as $key => $value) {
            if(sizeof($value) > 1) {
                echo '<tr>
                <td rowspan="'.(sizeof($value)+1) .'">'.$key.'</td>
                <td colspan="5">'.$value[0]['category'].'</td>
                </tr>';
                foreach($value as $row => $item) {
                    if(!array_key_exists($item['id'], $this->data)) {
                        echo '<tr data-key="'.$item['id'].'">
                        <td style="width: 10px">' .Helper::RomanConverter($row+1).'</td>
                        <td>'.$item['description'].'</td>
                        <td><input type="radio" name="checklist['.$item['id'].']" value="2" '.$disableAttributes.'></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" value="1" '.$disableAttributes.'></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" checked value="0" '.$disableAttributes.'></td>
                        </tr>';
                    } else {
                        switch ($this->data[$item['id']]['completed']) {
                            case 'TIADA':
                            $isCheck2 = '';
                            $isCheck1 = 'checked';
                            $isCheck0 = '';
                            break;
                            case 'ADA':
                            $isCheck2 = 'checked';
                            $isCheck1 = '';
                            $isCheck0 = '';
                            break;

                            default:
                            $isCheck0 = 'checked';
                            $isCheck1 = '';
                            $isCheck2 = '';
                            break;
                        }
                        echo '<tr data-key="'.$item['id'].'">
                        <td style="width: 10px">' .Helper::RomanConverter($row+1).'</td>
                        <td>'.$item['description'].'</td>
                        <td><input type="radio" name="checklist['.$item['id'].']" '.$isCheck2.' value="2" '.$disableAttributes.'></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" '.$isCheck1.' value="1" '.$disableAttributes.'></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" '.$isCheck0.' value="0" '.$disableAttributes.'></td>
                        </tr>';
                    }
                }
            } else {
                if(!array_key_exists($value[0]['id'], $this->data)) {
                    echo '<tr data-key="'.$value[0]['id'].'">
                    <td>'.$key.'</td>
                    <td colspan="2">'.$value[0]['description'].'</td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" value="2" '.$disableAttributes.'></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" value="1" '.$disableAttributes.'></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" checked value="0" '.$disableAttributes.'></td>
                    </tr>';
                } else {
                    switch ($this->data[$value[0]['id']]['completed']) {
                        case 'TIADA':
                        $isCheck2 = '';
                        $isCheck1 = 'checked';
                        $isCheck0 = '';
                        break;
                        case 'ADA':
                        $isCheck2 = 'checked';
                        $isCheck1 = '';
                        $isCheck0 = '';
                        break;

                        default:
                        $isCheck2 = '';
                        $isCheck1 = '';
                        $isCheck0 = 'checked';
                        break;
                    }
                    echo '<tr data-key="'.$value[0]['id'].'">
                    <td>'.$key.'</td>
                    <td colspan="2">'.$value[0]['description'].'</td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" '.$isCheck2.' value="2" '.$disableAttributes.'></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" '.$isCheck1.' value="1" '.$disableAttributes.'></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" '.$isCheck0.' value="0" '.$disableAttributes.'></td>
                    </tr>';

                }
            }
        }
    }
    private function renderFormTable($array) {
        $isCheck0 = 'checked';
        $isCheck1 = '';
        $isCheck2 = '';

        foreach($array as $key => $value) {
            if(sizeof($value) > 1) {
                echo '<tr>
                <td rowspan="'.(sizeof($value)+1) .'">'.$key.'</td>
                <td colspan="5">'.$value[0]['category'].'</td>
                </tr>';
                foreach($value as $row => $item) {
                    if(!array_key_exists($item['id'], $this->data)) {
                        echo '<tr data-key="'.$item['id'].'">
                        <td style="width: 10px">' .Helper::RomanConverter($row+1).'</td>
                        <td>'.$item['description'].'</td>
                        <td><input type="radio" name="checklist['.$item['id'].']" value="2"></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" value="1"></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" checked value="0"></td>
                        </tr>';
                    } else {
                        switch ($this->data[$item['id']]['completed']) {
                            case 'TIADA':
                            $isCheck2 = '';
                            $isCheck1 = 'checked';
                            $isCheck0 = '';
                            break;
                            case 'ADA':
                            $isCheck2 = 'checked';
                            $isCheck1 = '';
                            $isCheck0 = '';
                            break;

                            default:
                            $isCheck0 = 'checked';
                            $isCheck1 = '';
                            $isCheck2 = '';
                            break;
                        }
                        echo '<tr data-key="'.$item['id'].'">
                        <td style="width: 10px">' .Helper::RomanConverter($row+1).'</td>
                        <td>'.$item['description'].'</td>
                        <td><input type="radio" name="checklist['.$item['id'].']" '.$isCheck2.' value="2"></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" '.$isCheck1.' value="1"></td>
                        <td><input type="radio" name="checklist['.$item['id'].']" '.$isCheck0.' value="0"></td>
                        </tr>';
                    }
                }
            } else {
                if(!array_key_exists($value[0]['id'], $this->data)) {
                    echo '<tr data-key="'.$value[0]['id'].'">
                    <td>'.$key.'</td>
                    <td colspan="2">'.$value[0]['description'].'</td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" value="2"></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" value="1"></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" checked value="0"></td>
                    </tr>';
                } else {
                    switch ($this->data[$value[0]['id']]['completed']) {
                        case 'TIADA':
                        $isCheck2 = '';
                        $isCheck1 = 'checked';
                        $isCheck0 = '';
                        break;
                        case 'ADA':
                        $isCheck2 = 'checked';
                        $isCheck1 = '';
                        $isCheck0 = '';
                        break;

                        default:
                        $isCheck2 = '';
                        $isCheck1 = '';
                        $isCheck0 = 'checked';
                        break;
                    }
                    echo '<tr data-key="'.$value[0]['id'].'">
                    <td>'.$key.'</td>
                    <td colspan="2">'.$value[0]['description'].'</td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" '.$isCheck2.' value="2"></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" '.$isCheck1.' value="1"></td>
                    <td><input type="radio" name="checklist['.$value[0]['id'].']" '.$isCheck0.' value="0"></td>
                    </tr>';

                }
            }
        }
    }

    // private function renderFormTable2($array) {
    //     // if(array_key_exists(1, $this->data)) {
    //     //     // var_dump('expression'.$key);
    //     //     die;
    //     // }
    //     // die;
    //     foreach($array as $key => $value) {
    //         if(sizeof($value) > 1) {
    //             echo '<tr>
    //             <td rowspan="'.(sizeof($value)+1) .'">'.$key.'</td>
    //             <td colspan="5">'.$value[0]['category'].'</td>
    //             </tr>';
    //             foreach($value as $row => $item) {
    //                 echo '<tr data-key="'.$item['id'].'">
    //                 <td style="width: 10px">' .Helper::RomanConverter($row+1).'</td>
    //                 <td>'.$item['description'].'</td>
    //                 <td><input type="radio" name="checklist['.$item['id'].']" value="2"></td>
    //                 <td><input type="radio" name="checklist['.$item['id'].']" value="1"></td>
    //                 <td><input type="radio" name="checklist['.$item['id'].']" checked value="0"></td>
    //                 </tr>';
    //             }
    //         } else {
    //             echo '<tr data-key="'.$value[0]['id'].'">
    //             <td>'.$key.'</td>
    //             <td colspan="2">'.$value[0]['description'].'</td>
    //             <td><input type="radio" name="checklist['.$value[0]['id'].']" value="2"></td>
    //             <td><input type="radio" name="checklist['.$value[0]['id'].']" value="1"></td>
    //             <td><input type="radio" name="checklist['.$value[0]['id'].']" checked value="0"></td>
    //             </tr>';
    //         }
    //     }
    // }

    private function renderTableHeader() {

        $tableHeader = '
        <table class="table table-bordered table-condensed table-hover small kv-table">
        <thead>
        <tr>
        <th style="width: 10px">#</th>
        <th colspan="2">Perkara</th>
        <th style="width: 40px">Ada</th>
        <th style="width: 40px">Tiada</th>
        <th style="width: 40px">Tidak Berkenaan</th>
        </tr>
        </thead>
        <tbody>
        ';
        echo $tableHeader;
    }

    private function renderTableFooter() {

        $tableFooter = '</tbody></table>';
        echo $tableFooter;
    }
}
