<?php
namespace common\widgets;

use Yii;
use kartik\grid\GridView as KartikGridView;
use yii\helpers\ArrayHelper;
// use yii\data\ArrayDataProvider;
// use common\components\helper\Helper;
// use yii\helpers\Html;

/**
 *	Modifying kartik gridview to be box gridview
 *	under development
 *
 *
 */
class BoxGridView extends KartikGridView
{

    public $panelTemplate = <<< HTML
<div class="{prefix}{type} /*box-solid*/">
    {panelHeading}
    {panelBefore}
    <div class="box-body">
    {items}
    </div>
</div>
HTML;

//     public $summary = self::renderSummary().'<button type="button" class="btn btn-box-tool" data-widget="collapse">
//             <i class="fa fa-minus"></i>
//         </button>
// ';
    public $panelHeadingTemplate = <<< HTML
    <div class="pull-right">
        {summary}
    </div>
    <div class="box-tools pull-right">
    </div>
    <h3 class="box-title">
        {heading}
    </h3>
    <div class="clearfix"></div>
HTML;

    public $panelFooterTemplate = <<< HTML
    <div class="kv-box-pager">
        {pager}
    </div>
    {footer}
    <div class="clearfix"></div>
HTML;

    public $panelBeforeTemplate = <<< HTML
    <div class="pull-right">
        <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
            {toolbar}
        </div>
    </div>
    {before}
    <div class="clearfix"></div>
HTML;

    public $panelPrefix = 'box box-';
    // public $summary = "{begin} - {end} {count} {totalCount} {page} {pageCount}";
    // public $summary = '{summary}';

    // public $panel = [
	   //  // 'headingOptions'=>['class'=>'box-header'],
    // ];

    public function init()
    {
    	$test = ArrayHelper::getValue('{summary}');
    	var_dump($this->summary);die;
    	die;
    	// var_dump(self::renderSummary());die;
    	// $tag = ArrayHelper::remove($summaryOptions, 'tag', 'div');
    	// var_dump($this->panel);die;
        // $this->_module = Config::initModule(Module::className());
        // if (empty($this->options['id'])) {
        //     $this->options['id'] = $this->getId();
        // }
        // if (!$this->toggleData) {
        //     parent::init();
        //     return;
        // }
        // $this->_toggleDataKey = '_tog' . hash('crc32', $this->options['id']);
        // $this->_isShowAll = ArrayHelper::getValue($_GET, $this->_toggleDataKey, $this->defaultPagination) === 'all';
        // if ($this->_isShowAll) {
        //     /** @noinspection PhpUndefinedFieldInspection */
        //     $this->dataProvider->pagination = false;
        // }
        // $this->_toggleButtonId = $this->options['id'] . '-togdata-' . ($this->_isShowAll ? 'all' : 'page');
        parent::init();
    }

}
