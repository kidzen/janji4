<?php

namespace common\models;

use Yii;

use \common\models\base\DateSetting as BaseDateSetting;

/**
 * This is the model class for table "date_setting".
 */
class DateSetting extends BaseDateSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'step_refference', 'duration', 'duration_type', 'from', 'description', 'remark'], 'string', 'max' => 255]
        ]);
    }

}
