<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contract;

/**
 * common\models\search\ContractSearch represents the model behind the search form about `common\models\Contract`.
 */
 class ContractSearch extends Contract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'juu_staff_assigned', 'department_staff_assigned', 'template_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['contract_no', 'name', 'awarded_date', 'remark', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contract::find();

        if (Yii::$app->user->isAdminDepartment) {
            $query->joinWith('departmentStaffAssigned');
            $query->andFilterWhere([
                'profile.department' => Yii::$app->user->department,
            ]);
        } else if (Yii::$app->user->isUserDepartment) {
            $query->andFilterWhere([
                'department_staff_assigned' => Yii::$app->user->username,
            ]);
        } else if (Yii::$app->user->isUserJuu) {
            $query->andFilterWhere([
                'juu_staff_assigned' => Yii::$app->user->username,
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // add default order

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'awarded_date' => $this->awarded_date,
            'juu_staff_assigned' => $this->juu_staff_assigned,
            'department_staff_assigned' => $this->department_staff_assigned,
            'template_id' => $this->template_id,
            'status' => $this->status,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'contract_no', $this->contract_no])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
