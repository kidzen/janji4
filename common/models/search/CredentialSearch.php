<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Credential;

/**
 * common\models\search\CredentialSearch represents the model behind the search form about `common\models\Credential`.
 */
 class CredentialSearch extends Credential
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'staff_no'], 'integer'],
            [['password'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Credential::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'staff_no' => $this->staff_no,
        ]);

        $query->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
}
