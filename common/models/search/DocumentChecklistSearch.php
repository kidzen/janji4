<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DocumentChecklist;

/**
 * common\models\search\DocumentChecklistSearch represents the model behind the search form about `common\models\DocumentChecklist`.
 */
 class DocumentChecklistSearch extends DocumentChecklist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'doc_check_id', 'contract_id', 'completed', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['file', 'remark', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DocumentChecklist::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'doc_check_id' => $this->doc_check_id,
            'contract_id' => $this->contract_id,
            'completed' => $this->completed,
            'status' => $this->status,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
