<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DraftDocument;

/**
 * common\models\search\DraftDocumentSearch represents the model behind the search form about `common\models\DraftDocument`.
 */
 class DraftDocumentSearch extends DraftDocument
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contract_id', 'draft_status_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['name', 'file', 'description', 'remark', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DraftDocument::find();

        // if (Yii::$app->user->isAdminDepartment) {
        //     $query->joinWith('departmentStaffAssigned');
        //     $query->andFilterWhere([
        //         'profile.department' => Yii::$app->user->department,
        //     ]);
        // } else if (Yii::$app->user->isUserDepartment) {
        //     $query->andFilterWhere([
        //         'department_staff_assigned' => Yii::$app->user->username,
        //     ]);
        // }

        if(Yii::$app->user->isAdminDepartment) {
            // $query->joinWith('contract.departmentStaffAssigned');
            $query->joinWith('departmentRecords');
            // $query->with('departmentStaffAssigned');
            // $query->andFilterWhere([
            // //     // 'profile.department' => Yii::$app->user->department,
            // //     // 'contract.department_staff_assigned' => Yii::$app->user->username,
            //     'profile.staff_no' => Yii::$app->user->username,
            // ]);

            // $query->andWhere('[[profile.department]]=\''.Yii::$app->user->department.'\'');
            // $query->andWhere('[[contract.department_staff_assigned]]='.Yii::$app->user->username);
        } else if (Yii::$app->user->isUserDepartment) {

            $query->joinWith('departmentRecords');
            // $query->andFilterWhere([
            //     'department_staff_assigned' => Yii::$app->user->username,
            // ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contract_id' => $this->contract_id,
            'draft_status_id' => $this->draft_status_id,
            'status' => $this->status,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
