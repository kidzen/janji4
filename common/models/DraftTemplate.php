<?php

namespace common\models;

use Yii;

use \common\models\custom\DraftTemplate as BaseDraftTemplate;

/**
 * This is the model class for table "draft_template".
 */
class DraftTemplate extends BaseDraftTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique','message'=>'$name sudah wujud, sila kemaskini jika sama'],
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'file', 'remark'], 'string', 'max' => 255],
            [['tempFile'], 'file', 'skipOnEmpty' => true, 'extensions'=>'docx'],
        ];

     //    return array_replace_recursive(parent::rules(),
	    // [
     //        // [['name'], 'required'],
     //        // [['name'], 'unique','message'=>'$name sudah wujud, sila kemaskini jika sama'],
     //        [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
     //        [['deleted_at', 'created_at', 'updated_at'], 'safe'],
     //        [['name', 'description', 'file','remark'], 'string', 'max' => 255],
     //        [['tempFile'], 'file'],
     //        // [['tempFile'], 'file', 'skipOnEmpty' => true, 'extensions'=>'docx'],
     //    ]);
    }

}
