<?php

namespace common\models;

use Yii;

use \common\models\custom\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['username', 'auth_key', 'email'], 'required'],
            [['username', 'role_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['auth_key'], 'string', 'max' => 32],
            [['profile_pic', 'email', 'remark'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['email'], 'unique']
        ]);
    }

}
