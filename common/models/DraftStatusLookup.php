<?php

namespace common\models;

use Yii;

use \common\models\base\DraftStatusLookup as BaseDraftStatusLookup;

/**
 * This is the model class for table "draft_status_lookup".
 */
class DraftStatusLookup extends BaseDraftStatusLookup
{
    /**
     * @inheritdoc
     */
    public static function max()
    {
        return self::find()->last();
    }
    public static function min()
    {
        return self::find()->first();
    }
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'remark'], 'string', 'max' => 255]
        ]);
    }

}
