<?php

namespace common\models;

use Yii;

use \common\models\custom\Contract as BaseContract;

/**
 * This is the model class for table "contract".
 */
class Contract extends BaseContract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['contract_no', 'template_id'], 'required'],
            [['awarded_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['juu_staff_assigned', 'department_staff_assigned', 'template_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['contract_no', 'name', 'remark'], 'string', 'max' => 255]
        ]);
    }

}
