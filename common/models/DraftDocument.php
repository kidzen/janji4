<?php

namespace common\models;

use Yii;

use \common\models\custom\DraftDocument as BaseDraftDocument;

/**
 * This is the model class for table "draft_document".
 */
class DraftDocument extends BaseDraftDocument
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['contract_id', 'draft_status_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'file', 'description', 'remark'], 'string', 'max' => 255]
        ]);
    }

}
