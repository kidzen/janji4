<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "credential".
 *
 * @property integer $id
 * @property integer $staff_no
 * @property string $password
 *
 * @property \common\models\User $staffNo
 */
class Credential extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            'deleted_at' => date('d-M-Y h:i:s A'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('d-M-Y h:i:s A'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'staffNo'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_no', 'password'], 'required'],
            [['staff_no'], 'integer'],
            [['password'], 'string', 'max' => 100],
            [['staff_no'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'credential';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'staff_no' => 'No Pekerja',
            'password' => 'Kata Laluan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaffNo()
    {
        return $this->hasOne(\common\models\User::className(), ['username' => 'staff_no']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-Y h:i:s A'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->username,
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\CredentialQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\CredentialQuery(get_called_class());
        return $query;
    }
}
