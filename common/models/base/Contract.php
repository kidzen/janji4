<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "contract".
 *
 * @property integer $id
 * @property string $contract_no
 * @property string $name
 * @property string $awarded_date
 * @property integer $juu_staff_assigned
 * @property integer $department_staff_assigned
 * @property integer $template_id
 * @property string $remark
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \common\models\Attachment[] $attachments
 * @property \common\models\Profile $juuStaffAssigned
 * @property \common\models\Profile $departmentStaffAssigned
 * @property \common\models\DraftTemplate $template
 * @property \common\models\DocumentChecklist[] $documentChecklists
 * @property \common\models\DraftDocument[] $draftDocuments
 * @property \common\models\Note[] $notes
 */
class Contract extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            // 'deleted_at' => date('d-M-Y h:i:s A'),
            // 'deleted_at' => date('d-M-Y h:i:s A'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('d-M-Y h:i:s A'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'attachments',
            'juuStaffAssigned',
            'departmentStaffAssigned',
            'template',
            'documentChecklists',
            'draftDocuments',
            'notes'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_no'], 'required'],
            [['awarded_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['juu_staff_assigned', 'department_staff_assigned', 'template_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['contract_no', 'name', 'remark'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_no' => 'No Kontrak',
            'name' => 'Tajuk Kontrak',
            'awarded_date' => 'Tarikh SST',
            'juu_staff_assigned' => 'PIC JUU',
            'department_staff_assigned' => 'PIC Jabatan',
            'template_id' => 'Templat ID',
            'remark' => 'Catatan',
            'status' => 'Status',
            'assigned_department' => 'Jabatan Berkait',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(\common\models\Attachment::className(), ['contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuuStaffAssigned()
    {
        return $this->hasOne(\common\models\Profile::className(), ['staff_no' => 'juu_staff_assigned']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentRecords($department)
    {
        return $this->hasMany(\common\models\Profile::className(), ['department' => $department]);
    }

    public function getDepartmentStaffAssigned()
    {
        return $this->hasOne(\common\models\Profile::className(), ['staff_no' => 'department_staff_assigned']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(\common\models\DraftTemplate::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentChecklists()
    {
        return $this->hasMany(\common\models\DocumentChecklist::className(), ['contract_id' => 'id'])
        ->inverseOf('contract');
        ;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDraftDocuments()
    {
        return $this->hasMany(\common\models\DraftDocument::className(), ['contract_id' => 'id']);
    }
    // public function getDraftDocument()
    // {
    //     return $this->hasOne(\common\models\DraftDocument::className(), ['contract_id' => 'id'])->inverseOf('contract');
    // }

    public function getDraftStatus()
    {
        return $this->hasOne(\common\models\DraftStatusLookup::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes()
    {
        return $this->hasMany(\common\models\Note::className(), ['contract_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-Y h:i:s A'),
                'value' => date('d-M-Y h:i:s A'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->username,
                'value' => Yii::$app->user->username,
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\ContractQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\ContractQuery(get_called_class());
        return $query->where(['contract.deleted_by' => 0]);
    }
}
