<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "draft_document".
 *
 * @property integer $id
 * @property string $name
 * @property string $file
 * @property integer $contract_id
 * @property integer $draft_status_id
 * @property string $description
 * @property string $remark
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \common\models\Contract $contract
 * @property \common\models\DraftStatus $draftStatus
 * @property \common\models\DraftParam[] $draftParams
 * @property \common\models\DraftStatus[] $draftStatuses
 */
class DraftDocument extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            'deleted_at' => date('d-M-Y h:i:s A'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('d-M-Y h:i:s A'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'contract',
            'draftStatus',
            'draftParams',
            'draftStatuses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['contract_id', 'draft_status_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'file', 'description', 'remark'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'draft_document';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nama',
            'file' => 'Fail',
            'contract_id' => 'Kontrak ID',
            'draft_status_id' => 'Draf Status ID',
            'description' => 'Keterangan',
            'remark' => 'Catatan',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentRecords()
    {
        return $this->hasMany(\common\models\Profile::className(), ['staff_no' => 'department_staff_assigned'])
        ->andWhere('[[profile.department]]= :department',[':department'=> Yii::$app->user->department])
        ->via('contract');
    }

    public function getContract()
    {
        return $this->hasOne(\common\models\Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDraftStatus()
    {
        return $this->hasOne(\common\models\DraftStatus::className(), ['id' => 'draft_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusLookup()
    {
        return $this->hasMany(\common\models\DraftStatusLookup::className(), ['id' => 'status_id'])
        ->via('draftStatus');
    }

    public function getDraftParams()
    {
        return $this->hasMany(\common\models\DraftParam::className(), ['draft_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDraftStatuses()
    {
        return $this->hasMany(\common\models\DraftStatus::className(), ['draft_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-Y h:i:s A'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->username,
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\DraftDocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\DraftDocumentQuery(get_called_class());
        return $query->where(['draft_document.deleted_by' => 0]);
    }
}
