<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "draft_param".
 *
 * @property integer $id
 * @property integer $draft_document_id
 * @property integer $param_list_id
 * @property string $param
 * @property string $remark
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \common\models\ParamList $paramList
 * @property \common\models\DraftDocument $draftDocument
 */
class DraftParam extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            'deleted_at' => date('d-M-Y h:i:s A'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('d-M-Y h:i:s A'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'paramList',
            'draftDocument'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['draft_document_id', 'param_list_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['param', 'remark'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'draft_param';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'draft_document_id' => 'Draf Pejanjian ID',
            'param_list_id' => 'Param List ID',
            'param' => 'Parameter',
            'remark' => 'Catatan',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(\common\models\ParamListLookup::className(), ['id' => 'param_id'])->via('paramList');
    }

    public function getParamList()
    {
        return $this->hasOne(\common\models\ParamList::className(), ['id' => 'param_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDraftDocument()
    {
        return $this->hasOne(\common\models\DraftDocument::className(), ['id' => 'draft_document_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-Y h:i:s A'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->username,
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\DraftParamQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\DraftParamQuery(get_called_class());
        return $query->where(['draft_param.deleted_by' => 0]);
    }
}
