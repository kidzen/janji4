<?php

namespace common\models;

use Yii;

use \common\models\custom\DocumentChecklistLookup as BaseDocumentChecklistLookup;

/**
 * This is the model class for table "document_checklist_lookup".
 */
class DocumentChecklistLookup extends BaseDocumentChecklistLookup
{
    public $tempFile;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['category', 'name', 'file', 'description', 'remark'], 'string', 'max' => 255],
            [['tempFile'], 'file']
        ]);
    }

}
