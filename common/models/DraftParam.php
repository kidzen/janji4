<?php

namespace common\models;

use Yii;

use \common\models\base\DraftParam as BaseDraftParam;

/**
 * This is the model class for table "draft_param".
 */
class DraftParam extends BaseDraftParam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['draft_document_id', 'param_list_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['param', 'remark'], 'string', 'max' => 255]
        ]);
    }

}
