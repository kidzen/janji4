<?php

namespace common\models;

use Yii;

use \common\models\custom\DocumentChecklist as BaseDocumentChecklist;

/**
 * This is the model class for table "document_checklist".
 */
class DocumentChecklist extends BaseDocumentChecklist
{

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        switch ($this->completed) {
            case '0':
                $this->completed = 'TIDAK BERKAITAN';
                break;
            case '1':
                $this->completed = 'TIADA';
                break;
            case '2':
                $this->completed = 'ADA';
                break;
            default:
                $this->completed = 'STATUS TIDAK DIKETAHUI';
                break;
        }
    }
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['doc_check_id', 'contract_id', 'completed', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['file', 'remark'], 'string', 'max' => 255]
        ]);
    }

}
