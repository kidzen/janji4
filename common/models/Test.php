<?php

namespace common\models;

use Yii;
use \common\models\base\Test as BaseTest;

/**
 * This is the model class for table "test".
 */
class Test extends BaseTest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test'], 'integer']
        ]);
    }
	
}
