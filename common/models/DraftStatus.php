<?php

namespace common\models;

use Yii;

use \common\models\base\DraftStatus as BaseDraftStatus;

/**
 * This is the model class for table "draft_status".
 */
class DraftStatus extends BaseDraftStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status_id', 'draft_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['remark'], 'string', 'max' => 255]
        ]);
    }

}
