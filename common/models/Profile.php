<?php

namespace common\models;

use Yii;

use \common\models\custom\Profile as BaseProfile;

/**
 * This is the model class for table "profile".
 */
class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_no', 'name'], 'required'],
            [['staff_no'], 'integer'],
            [['name', 'position'], 'string', 'max' => 100],
            [['department'], 'string', 'max' => 255],
            [['staff_no'], 'unique']
        ]);
    }

}
