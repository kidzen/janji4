<?php

namespace common\models;

use Yii;

use \common\models\custom\Credential as BaseCredential;

/**
 * This is the model class for table "credential".
 */
class Credential extends BaseCredential
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_no', 'password'], 'required'],
            [['staff_no'], 'integer'],
            [['password'], 'string', 'max' => 100],
            [['staff_no'], 'unique']
        ]);
    }

}
