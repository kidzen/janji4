<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\DraftStatusLookup]].
 *
 * @see \common\models\query\DraftStatusLookup
 */
class DraftStatusLookupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\DraftStatusLookup[]|array
     */
    public function first($db = null)
    {
        $this->orderBy(['id'=>SORT_ASC]);
        return parent::one($db)->id;
    }

    public function last($db = null)
    {
        $this->orderBy(['id'=>SORT_DESC]);
        return parent::one($db)->id;
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\DraftStatusLookup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
