<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\DraftDocument]].
 *
 * @see \common\models\query\DraftDocument
 */
class DraftDocumentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\DraftDocument[]|array
     */
    public function pending($contractId)
    {
        // $this->andWhere('[[contract_id]]='.$contractId);
        // $this->andWhere('[[draft_status_id]]<3');
        // $this->asArray();
        // $this->one();
        // var_dump(parent::all());
        // die;
        // if($this) {
        //     return true;
        // }
        return false;
        return $this;
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\DraftDocument|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
