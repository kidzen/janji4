<?php

namespace common\models\query;

use Yii;
/**
 * This is the ActiveQuery class for [[\common\models\query\Contract]].
 *
 * @see \common\models\query\Contract
 */
class ContractQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/
    /**
     * @inheritdoc
     * @return \common\models\query\Contract[]|array
     */

    public function byDepartment($db = null)
    {

        $this->joinWith('departmentStaffAssigned');
        $this->andWhere('[[profile.department]]=\''.Yii::$app->user->department.'\'');
        $this->andWhere('[[contract.department_staff_assigned]]='.Yii::$app->user->username);

        return $this;
    }

    public function mine($db = null)
    {
        $this->andWhere('[[contract.department_staff_assigned]]='.Yii::$app->user->username);
        return $this;
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\Contract|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
