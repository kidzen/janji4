<?php

namespace common\models\custom;

use Yii;
use common\models\base\Profile as BaseProfile;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "profile".
 *
 * @property integer $id
 * @property integer $staff_no
 * @property string $name
 * @property string $position
 * @property string $department
 *
 * @property \common\models\Contract[] $contracts
 * @property \common\models\User $user
 */
class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     * @return \common\models\query\ProfileQuery the active query used by this AR class.
     */
    public static function findByStaffNo($username)
    {
        return static::findOne(['staff_no' => $username]);
    }

    public static function find()
    {
    	$query = new \common\models\query\ProfileQuery(get_called_class());
    	return $query;
    }
    public static function juuList()
    {
        $arrayList = \yii\helpers\ArrayHelper::map(\common\models\Profile::find()
            ->select('name,department,staff_no,position')
            ->orderBy('staff_no')->asArray()
            // ->dept('JAB UNDANG-UNDANG')
            ->andWhere(['like','department','JAB UNDANG-UNDANG'])
            ->all(), 'staff_no', function($model) {
                return $model['staff_no'].' : '.$model['name'] . '('.$model['department'].')';
            }, 'position');
        return $arrayList;
    }
    public static function deptList($dept = null)
    {
        if(Yii::$app->user->department !== 'JAB UNDANG-UNDANG') {
            $dept = Yii::$app->user->department;
        }
        if($dept){
            $arrayList = \yii\helpers\ArrayHelper::map(\common\models\Profile::find()
                ->select('name,department,staff_no,position')
                ->orderBy('staff_no')->asArray()
                ->dept($dept)
                ->all(), 'staff_no', function($model) {
                    return $model['staff_no'].' : '.$model['name'] . '('.$model['department'].')';
                }, 'position');
        } else {
        	$arrayList = \yii\helpers\ArrayHelper::map(\common\models\Profile::find()
                ->select('name,department,staff_no,position')
                ->orderBy('staff_no')->asArray()
                ->all(), 'staff_no', function($model) {
        			return $model['staff_no'].' : '.$model['name'] . '('.$model['department'].')';
        		}, 'position');
        }
    	return $arrayList;
    }

}
