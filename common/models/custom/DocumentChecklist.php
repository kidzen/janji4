<?php

namespace common\models\custom;

use Yii;
use \common\models\base\DocumentChecklist as BaseDocumentChecklist;

/**
 * This is the model class for table "document_checklist".
 */
class DocumentChecklist extends BaseDocumentChecklist
{
    public $tempFile;
    /**
     * @inheritdoc
     */
    public function getTemplate() {
        $checklistArray2  = \common\models\DocumentChecklistLookup::find()
        ->select('id,category,description')
        ->groupBy('category,id,description')
        ->orderBy('id')
        ->asArray()->all();

        $_tempKey = 0;
        $_tempCat = false;
        $_strg = '';
        foreach ($checklistArray2 as $key => $value) {
            if($_tempCat !== $value['category'] || $value['category'] == '') {
                $_tempKey = $_tempKey+1;
            }
            $group[$key] = $value;
            $group[$key]['bil'] = $_tempKey;
            $_tempCat = $value['category'];
        }
        foreach ($group as $key1 => $value1) {
            $array[$value1['bil']][] = $value1;
        }
        return $array;

    }

}
