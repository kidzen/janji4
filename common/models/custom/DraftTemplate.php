<?php

namespace common\models\custom;

use Yii;
use yii\base\Exception;
use yii\web\UploadedFile;
use \common\models\base\DraftTemplate as BaseDraftTemplate;

/**
 * This is the model class for table "draft_template".
 */
class DraftTemplate extends BaseDraftTemplate
{
    public $tempFile;
    protected $fileName;

    /**
     * @inheritdoc
     */
    public function upload()
    {
        // validate model
        if (!$this->validate()) {
            throw new Exception(' Sila pastikan data diisi dengan betul.');
        }
        // $this->name = strtoupper($this->name);
        $this->tempFile = UploadedFile::getInstance($this, 'tempFile');
        if($this->tempFile){
            $this->fileName = Yii::$app->params['settings']['templateDirectory'] . $this->tempFile->baseName . '_'.time(). '.' . $this->tempFile->extension;
            //check file exist
            if(file_exists($this->fileName)) {
                // $this->addError(' File already exist.');
                throw new Exception(' Template yang sama telah wujud. Sila guna nama lain (rename sebelom upload) atau kemaskini template sedia ada.');
            }

            // upload file
            if(!$this->tempFile->saveAs($this->fileName)){
                throw new Exception(' Template yang sama telah wujud. Sila pastikan konfigurasi server membenarkan proses memuat naik fail.');
            }

            // save to records
            $this->file = $this->fileName;
            if(!$this->saveAll()){
                // delete if fail
                unlink($this->fileName);
                throw new Exception(' Data gagal dikemaskini.');
            }
        } else {
            if(!$this->saveAll()){
                // delete if fail
                throw new Exception(' Data gagal dikemaskini.');
            }
        }


        return true;
    }

}
