<?php
namespace common\models\custom;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\identity\User as IdentityUser;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends IdentityUser
{

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByStaffNo($staff_no)
    {
        return static::findOne(['staff_no' => $staff_no, 'status' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function getProfile()
    {
        return $this->hasOne(\common\models\Profile::className(), ['staff_no' => 'staff_no']);
    }

}
