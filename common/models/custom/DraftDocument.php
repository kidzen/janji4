<?php

namespace common\models\custom;

use Yii;
use \common\models\base\DraftDocument as BaseDraftDocument;
use yii\base\Exception;

/**
 * This is the model class for table "draft_document".
 */
class DraftDocument extends BaseDraftDocument
{
    public $contractId;
    public $templateId;
    public $tempFile;

    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'name' => 'Nama',
            'file' => 'File',
            'template_id' => 'ID Template',
            'contract_id' => 'ID Kontrak',
            'draft_status_id' => 'ID Status Draf',
            'description' => 'Keterangan',
            'signator1' => 'Signator1',
            'signator2' => 'Signator2',
            'remark' => 'Catatan',
            'status' => 'Status',
        ];
    }

    public function allowAlter()
    {
        $min = \common\models\DraftStatusLookup::min() + 1;
        $max = \common\models\DraftStatusLookup::max();
        // var_dump($statusFirst->id);die;
        // var_dump($this);die;

        $forbid = true;
        // $forbid = ($this->draft_status_id > $min || $this->draft_status_id > $max );
        foreach ($this->contract->draftDocuments as $key => $value) {
            // var_dump($value->draftStatus->status0->id);die;
            $forbid = ($value->draftStatus->status0->id > $min) && $forbid;

        }
        return $forbid;
    }

    public function getDraftParamsQuantity()
    {
        return $this->hasMany(\common\models\DraftParam::className(), ['draft_document_id' => 'id'])
        ->select(['draft_document_id', 'counted' => 'count(*)'])
        ->groupBy('draft_document_id')
        ->asArray(true);
    }

    public static function generateDocFile($path,$fileName,$ext = 'doc')
    {
        $name = $fileName;
        $source = $path;
        $phpWord = \PhpOffice\PhpWord\IOFactory::load($source);

        // (Re)write contents
        $writers = ['MsDoc' => 'doc', 'ODText' => 'odt', 'RTF' => 'rtf'];
        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, array_key_exists($ext, $writers));
        $xmlWriter->save("{$path}/{$name}.{$ext}");
    }

//     public static function generateDocxFile($path,$fileName)
//     {
//         $name = $fileName;
//         // $source = 'originalTemplate/doc/accepted/test.doc';
//         // $source = 'originalTemplate/doc/accepted/TEMPLATE BEKALAN BON2.doc';
//         $source = $path;
// // C:\wamp64\www\janji3\frontend\web\originalTemplate\doc\accepted
//         // $name = basename(__FILE__, '.php');
//         // $source = "resources/{$name}.doc";
//         echo date('H:i:s'), " Reading contents from `{$source}`",'<br>';
//         $phpWord = \PhpOffice\PhpWord\IOFactory::load($source);
//         // die;
//         // (Re)write contents
//         $writers = array('MsDoc' => 'doc', 'ODText' => 'odt', 'RTF' => 'rtf');
//         foreach ($writers as $writer => $extension) {
//             echo date('H:i:s'), " Write to {$writer} format", '<br>';
//             $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, $writer);
//             $xmlWriter->save("{$path}/{$name}.{$extension}");
//             // rename("{$name}.{$extension}", "results/{$name}.{$extension}");
//         }
//     }
    public static function generate($draftDocument,$ext = 'docx')
    {
        $PHPWord = new \PhpOffice\PhpWord\PhpWord();
        $paramList = \common\models\DraftParam::find()
        ->where(['draft_document_id'=>$draftDocument->id])
        ->joinWith('paramList.param')
        ->asArray()
        ->all();

        // collect param from database
        foreach ($paramList as $key => $value) {
            $param[$value['paramList']['param']['param']] = $value['param'];
        }

        // prepare directory for saving
        $year = date('Y');
        $month = date('m');
        $fileDir = "draft\\{$year}\\{$month}";
        if (!file_exists($fileDir)) {
            mkdir($fileDir, 0777, true);
        }

        // overide temporary directory for generator operation
        $tempSystemDir = \Yii::getAlias('@webroot').'\runtime\temp';
        if (!file_exists($tempSystemDir)) {
            mkdir($tempSystemDir, 0777, true);
        }
        \PhpOffice\PhpWord\Settings::setTempDir($tempSystemDir);


        // prepare filename for saving
        // invalid char for file name [ \ / : * ? " < > | ]
        $contractTitle = isset($param['contractTitle']) ? $param['contractTitle'] : 'contractTitle';
        $contractNo = isset($param['contractNo']) ? $param['contractNo'] : 'contractNo';
        $title = str_replace([ "\\", "/", ":", "*", "?", "\"", "<", ">", "|", "]"], '_', $contractNo);
        $titleFile = $title.'_'.time();
        $fileName = "{$titleFile}";

        // get template file
        $templateFileDir = $draftDocument->contract->template->file;
        $source = realpath($templateFileDir);

        // load template
        $document = $PHPWord->loadTemplate($source);
        foreach ($param as $key => $value) {
            if(!empty($value)){
                $document->setValue($key, $value);
            }
        }

        // Save file
        $filePath = "{$fileDir}\\{$fileName}.{$ext}";

        $document->saveAs($filePath);
        // self::generateDocFile($filePath,$fileName);

        // remove old file
        if (file_exists($filePath)) {
            if(file_exists($draftDocument->file)){
                unlink($draftDocument->file);
            }
            $draftDocument->file = $filePath;
            $draftDocument->draft_status_id = isset($draftDocument->draft_status_id) ? $draftDocument->draft_status_id : 1;
            if($draftDocument->save(false)){
                return true;
            } else {
                Yii::$app->notify->fail(' Fail gagal dijana.');
                // Throw new Exception(' Lokasi fail gagal dikemaskini.');
            }
        }
        return false;
    }

    public static function updateStatus($data) {
        // var_dump($data);die;
        // draft document
        if(!$model = static::findOne($data['draft_id'])) {
            Throw new Exception(' Data tidak dijumpai.');
        }
        if($data['note']) {
            $note = new \common\models\Note();
            $note->contract_id = $model->contract->id;
            $note->notes = $data['notes'];
            $note->save();
        }
        // // if no change dont create new log
        // if($model->draftStatus->status0->id == $data['status_id']){
        //     return true;
        // }
        // // var_dump($model->draftStatus->status0->id);
        // // var_dump($data['status_id']);
        // // die;
        // // log new status
        // $draftStatus = new \common\models\DraftStatus();
        // $draftStatus->draft_id = $data['draft_id'];
        // $draftStatus->status_id = $data['status_id'];
        // $draftStatus->remark = $data['note'];
        // if (!$draftStatus->save()){
        //     Throw new Exception(' Kemaskini status gagal.');
        // }

        // $model->draft_status_id = $draftStatus->id;
        // if(!$model->save()){
        //     Throw new Exception(' Data gagal dikemaskini.');
        // }
        // var_dump($model->contract->status);die;
        $model->contract->juu_staff_assigned = $data['juu_staff_assigned'];
        $model->contract->status = $data['status_id'];
        if(!$model->contract->save()){
            Throw new Exception(' Data kontrak gagal dikemaskini.');
        }

    }
    public static function Approval($id) {
        $model = static::findOne($id);
        if(!$model) {
            Throw new Exception(' Data tidak dijumpai.');
        }
        // check if checklist is empty
        if(empty($model->contract->documentChecklists)){
            Throw new Exception(' Sila pastikan Borang Senarai Semak telah dilengkapkan.');
        }
       // var_dump($model->draft_status_id);die;
        // log new status
        // $draftStatus = new \common\models\DraftStatus();
        // $draftStatus->draft_id = $model->id;
        // // if(!$draftStatus->save(false)){
        // //     Throw new Exception(' Status draf tidak berjaya dikemaskini. Sila pastikan draf diisi dengan betul.');
        // // }
        // if(isset($model->draftStatus)){
        //     if($model->draftStatus->status0->id !== \common\models\DraftStatusLookup::max()){
        //         if($model->draftStatus->status_id === 1) {
        //             $draftStatus->status_id = $model->draftStatus->status_id + 2;
        //         } else {
        //             $draftStatus->status_id = $model->draftStatus->status_id + 1;
        //         }
        //     } else {
        //         Throw new Exception(' Draf telah selesai.');
        //     }
        // } else {
        //     $draftStatus->status_id = 1;
        // }

        // if(!$draftStatus->save(false)){
        //     Throw new Exception(' Status draf tidak berjaya dikemaskini. Sila pastikan draf diisi dengan betul.');
        // }

        // $model->draft_status_id = $draftStatus->id;
        $model->contract->status = 3;
        if(!$model->contract->save()){
            Throw new Exception(' Data draf tidak berjaya dikemaskini.');
        }
        if(!$model->save()){
            Throw new Exception(' Data draf tidak berjaya dikemaskini.');
        }

    }
    public static function Reject($id) {
        $model = static::findOne($id);
        $draftStatus = new \common\models\DraftStatus();
        $draftStatus->draft_id = $model->id;
        if(isset($model->draftStatus)){
            if($model->draftStatus->status0->name !== 'Selesai' || $model->draftStatus->status !== 1){
                $draftStatus->status_id = 2;
            } else {
                Throw new Exception(' Draf telah selesai.');
            }
        } else {
            Throw new Exception(' Tiada sejarah draf terdahulu.');
        }
        if(!$draftStatus->save(false)){
            Throw new Exception(' Status draf tidak berjaya dikemaskini. Sila pastikan draf diisi dengan betul.');
        }
        $model->draft_status_id = $draftStatus->id;
        if(!$model->save()){
            Throw new Exception(' Data draf tidak berjaya dikemaskini.');
        }
    }
}
