<?php

namespace common\models\custom;

use Yii;
use \common\models\base\Contract as BaseContract;
use \common\models\DraftStatus;

/**
 * This is the model class for table "contract".
 */
class Contract extends BaseContract
{
    public $created_date;
    public $last_update;
    public $assigned_department;
    /**
     * @return \yii\db\ActiveQuery
     */

    public function accessToAlter() {
        if(Yii::$app->user->isAdmin) {
            return true;
        }
        if(!isset($this->dirtyAttributes['status'])){
            if($this->status == \common\models\DraftStatusLookup::max()) {
                return false;
            }
        }
        return  true;
    }
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if(!$this->accessToAlter() && !Yii::$app->user->isAdmin) {
            return false;
        }
        if(empty($this->juu_staff_assigned) && empty($this->department_staff_assigned)){
            $this->juu_staff_assigned = Yii::$app->params['settings']['defaultJuuStaffAssigned'];
            $this->department_staff_assigned = Yii::$app->user->username;
        }

        return true;
    }
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        // var_dump($changedAttributes);die;

        if(!$insert && isset($changedAttributes['status'])) {
        // case : no active doc, status remain same : save normally
            if(!$this->activeDocument && ($changedAttributes['status'] == $this->oldAttributes['status'])){
                return true;
            }

        // case : status changed : do something
            if(!($changedAttributes['status'] == $this->oldAttributes['status'])){
            // create new draft status
                $draftStatus = new DraftStatus();
                $draftStatus->status_id = $changedAttributes['status'];

            // case : active doc : renew draft_status_id
                if($this->activeDocument) {
                    $draftStatus->draft_id = $this->activeDocument->id;
                    $draftStatus->status_id = $this->status;
                } else {
                    throw new \yii\web\MethodNotAllowedHttpException(' Tiada draf perjanjian aktif');
                }
                if(!$draftStatus->save()){
                    throw new \yii\web\BadRequestHttpException("Gagal kemaskini status");
                }
                $activeDraft = \common\models\DraftDocument::findOne($this->activeDocument->id);
                $activeDraft->draft_status_id = $draftStatus->id;
                $activeDraft->save(false);
            // your code here like $changedAttributes['myField'];
            }
        } else {
            unset($this->status);
        }
    }
    //rename to access
    public function can($operation) {
        $assignedDepartment = $this['departmentStaffAssigned']['department'];
        $isAssignedDepartment = (Yii::$app->user->department == $assignedDepartment);
        $isAssignedDepartmentUser = (Yii::$app->user->username == $this['department_staff_assigned']);
        $isAssignedJuuUser = (Yii::$app->user->username == $this['juu_staff_assigned']);
        $isCreator = $this['created_by'] == Yii::$app->user->username;
        $isUnderJuu = ($this['status'] > Yii::$app->params['settings']['departmentAlterLimit']);
        $isDone = ($this['draftStatus']['name'] == 'Selesai');
        $isJuu = (Yii::$app->user->isJuu || Yii::$app->user->isAdmin);
        $permisionToDelete = ($isCreator && !$isUnderJuu || $isAssignedJuuUser) && !$isDone;

        switch ($operation) {
            case 'view':
                return true;
            break;
            case 'operate':
                if(!$isUnderJuu) {
                    if($isAssignedDepartment && ($isAssignedDepartmentUser || Yii::$app->user->isAdminDepartment)) {
                        return true;
                    }
                } else {
                    if(($isAssignedJuuUser && !$isDone) || Yii::$app->user->isAdmin) {
                        return true;
                    }
                }
                return false;
            break;
            case 'edit':
                if($isDone) {
                    return false;
                }
                if(Yii::$app->user->isAdmin) {
                    return true;
                }
                // if(Yii::$app->user->isAdmin || (!($this->can('bypass') || !$this->can('requireHighLevelBypassAuthority')))) {
                //     return false;
                // }
                if($isAssignedJuuUser || (!($isUnderJuu) && $isAssignedDepartment) ) {
                    return true;
                }
                return false;
            break;
            case 'delete':
                if($isDone) {
                    return false;
                }
                if(Yii::$app->user->isAdmin) {
                    return true;
                }

                // $permisionToDelete = ($isCreator && !($this['status'] > Yii::$app->params['settings']['departmentAlterLimit']) || (Yii::$app->user->isJuu || Yii::$app->user->isAdmin));
                // var_dump(Yii::$app->params['settings']['departmentAlterLimit']);die;
                // var_dump($isCreator && !($this['status'] > Yii::$app->params['settings']['departmentAlterLimit']));die;
                // if(Yii::$app->user->isAdmin || $permisionToDelete) {
                if($permisionToDelete) {
                    return true;
                }
            break;
            case 'author':
                if(!$isUnderJuu) {
                    if($isCreator) {
                        return true;
                    }
                } else {
                    if(($isAssignedJuuUser && !$isDone) || Yii::$app->user->isAdmin) {
                        return true;
                    }
                }
            break;
            case 'bypass':
                return $isAssignedJuuUser;
            break;
            case 'requireHighLevelBypassAuthority':
                return  $isUnderJuu;
            break;
            default:
                throw new \yii\web\NotSupportedException("Sila tentukan kebenaran operasi.");
            break;
        }
        return false;

    }

    public function getDraftStatuses()
    {

        return $this->hasMany(\common\models\DraftStatus::className(), ['draft_id' => 'id'])
        ->via('draftDocuments')
        ->orderBy(['draft_status.id'=>SORT_DESC])
        // ->orderBy('draft_status.id','desc')
        ;
    }

    public function getActiveDocument() {
        $max = \common\models\DraftStatusLookup::max();
        // $min = \common\models\DraftStatusLookup::min()+1;

        if($this->draftDocuments){
            // foreach ($this->draftDocuments as $key => $value) {
            //     $activeDocument = $this->getDraftDocuments()
            //     ->joinWith('statusLookup')
            //     ->andWhere(['<','status_id', $max ])
            //     ->one();
            // }
            $activeDocument = $this->hasOne(\common\models\DraftDocument::className(), ['contract_id' => 'id'])
            ->joinWith('statusLookup')
            ->andWhere(['<','status_id', $max ])
            ->one();
        } else {
            $activeDocument = null;
        }
        // var_dump($activeDocument);die;
        // if(Yii::$app->user->isJuu) {
        //     $activeDocument = $this->hasOne(\common\models\DraftDocument::className(), ['contract_id' => 'id'])
        //     ->joinWith('statusLookup')
        //     ->andWhere(['<','status_id', $max ])
        //     ->one();
        // } else {
        //     $activeDocument = $this->hasOne(\common\models\DraftDocument::className(), ['contract_id' => 'id'])
        //     ->joinWith('statusLookup')
        //     ->andWhere(['or',['<','status_id', $min ],['status_id' =>null]])
        //     ->orWhere(['<','draft_status_id',$min])
        //     ->one();
        // }
        // var_dump($activeDocument);die;
        return $activeDocument;
    }

    public function getIncomplete() {
        $draft = \common\models\DraftDocument::findOne(['contract_id'=>$this->id]);
        return $draft['id'];
    }

    public static function getChecklists($id)
    {
        if(static::findOne($id)->documentChecklists){
            $checklistArray2 = static::findOne($id)->documentChecklists;
            foreach ($checklistArray2 as $key => $value) {
                $array[$value->docCheck->id] = $value->attributes;
                $array[$value->docCheck->id]['description'] = $value->docCheck->category;
                $array[$value->docCheck->id]['category'] = $value->docCheck->description;
            }
        } else {
            $array = [];
        }
        // var_dump($array);
        return $array;
    }
    //backup remove if Not use
    // public static function getChecklists2($id)
    // {
    //     if(static::findOne($id)->documentChecklists){
    //         $checklistArray2  = \common\models\DocumentChecklistLookup::find()
    //         ->select('document_checklist.id,document_checklist_lookup.category,document_checklist_lookup.description,document_checklist.completed')
    //         ->joinWith('documentChecklists.contract')
    //         ->where(['contract.id' => $id])
    //         ->groupBy('document_checklist_lookup.category,document_checklist.id,document_checklist_lookup.description,document_checklist.completed')
    //         ->orderBy('id')
    //         ->asArray()
    //         ->all();

    //         $_tempKey = 0;
    //         $_tempCat = false;
    //         $_strg = '';
    //         foreach ($checklistArray2 as $key => $value) {
    //             if($_tempCat !== $value['category'] || $value['category'] == '') {
    //                 $_tempKey = $_tempKey+1;
    //             }
    //             $group[$key] = $value;
    //             $group[$key]['bil'] = $_tempKey;
    //             $_tempCat = $value['category'];
    //         }
    //         foreach ($group as $key1 => $value1) {
    //             $array[$value1['bil']][] = $value1;
    //         }

    //     } else {
    //         $checklistArray2  = \common\models\DocumentChecklistLookup::find()
    //         ->select('id,category,description')
    //         ->groupBy('category,id,description')
    //         ->orderBy('id')
    //         ->asArray()->all();

    //         $_tempKey = 0;
    //         $_tempCat = false;
    //         $_strg = '';
    //         foreach ($checklistArray2 as $key => $value) {
    //             if($_tempCat !== $value['category'] || $value['category'] == '') {
    //                 $_tempKey = $_tempKey+1;
    //             }
    //             $group[$key] = $value;
    //             $group[$key]['bil'] = $_tempKey;
    //             $_tempCat = $value['category'];
    //         }
    //         foreach ($group as $key1 => $value1) {
    //             $array[$value1['bil']][] = $value1;
    //         }

    //     }
    //     return $array;
    // }

    public function afterFind() {
        // require recheck
        parent::afterFind();
        // setlocale(LC_ALL, 'Malay_Malaysia');
        $assignedDepartment = $this['departmentStaffAssigned']['department'];


        if(Yii::$app->db->driverName == 'oci') {
            $created = \DateTime::createFromFormat("d#M#y H#i#s*A", $this->created_at);
            $updated = \DateTime::createFromFormat("d#M#y H#i#s*A", $this->updated_at);
            // $time = \DateTime::createFromFormat("U", time());
            // die;
        } else {
            $created = new \DateTime($this->created_at);
            $updated = new \DateTime($this->updated_at);
        }
        if($created) {
            // $this->created_date = $created->format('d M Y');
            $this->created_date = strtoupper($created->format('d-M-y'));

            // $update_date = $created->format('d-m-Y');
            // $update_date = $created->format('d-m-Y H:i:s');
            // var_dump($this->updated_at);die;
            // var_dump($updated);die;
            $update_date = $updated->format('U');
            // $update_date = $created->format('Y-m-d H:i:s');

            // $this->last_update = '.. '.\common\components\helper\Helper::nicetime($update_date);
            // $this->last_update = '.. '.\common\components\helper\Helper::timeAgo2($update_date);
            $this->last_update = '.. '.\common\components\helper\Helper::timeAgo($update_date);
            $this->assigned_department = $this['departmentStaffAssigned']['department'];
        }

    }
}
