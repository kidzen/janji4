<?php

namespace common\models\custom;

use Yii;
use common\models\base\Credential as BaseCredential;
// use yii\behaviors\TimestampBehavior;
// use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "credential".
 *
 * @property integer $id
 * @property string $staff_no
 * @property string $password
 *
 * @property \common\models\User $staffNo
 */
class Credential extends BaseCredential
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->password = Yii::$app->security->generatePasswordHash($this->password);
    }
}
