<?php

namespace common\models;

use Yii;

use \common\models\base\Attachment as BaseAttachment;

/**
 * This is the model class for table "attachment".
 */
class Attachment extends BaseAttachment
{
    public $tempFile;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['contract_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'file', 'remark'], 'string', 'max' => 255]
        ]);
    }

}
