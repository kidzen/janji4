<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DocumentChecklist */

$this->title = 'Create Document Checklist';
$this->params['breadcrumbs'][] = ['label' => 'Document Checklist', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-checklist-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
