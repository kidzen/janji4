<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\DraftDocument */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Document', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-document-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Document'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'id',
        'name',
        'file',
        'contract_id',
        [
            'attribute' => 'draftStatus.id',
            'label' => 'Draft Status',
        ],
        'description',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerJANJIApprover->totalCount){
    $gridColumnJANJIApprover = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
                        'approver_id',
            'approve_level_id',
            'approved',
            'approved_at',
            'approved_by',
            'remark',
            'status',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerJANJIApprover,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-janji.approver']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Janjiapprover'),
        ],
        'export' => false,
        'columns' => $gridColumnJANJIApprover
    ]);
}
?>

    </div>
    <div class="row">
        <h4>DraftStatus<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnDraftStatus = [
        'id',
        'status_id',
        'draft_id',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model->draftStatus,
        'attributes' => $gridColumnDraftStatus    ]);
    ?>
</div>
