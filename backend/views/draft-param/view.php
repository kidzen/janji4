<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\DraftParam */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Draft Param', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-param-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Param'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'id',
        'draft_document_id',
        [
            'attribute' => 'paramList.id',
            'label' => 'Param List',
        ],
        'param',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>ParamList<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnParamList = [
        'id',
        'template_id',
        'param_id',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model->paramList,
        'attributes' => $gridColumnParamList    ]);
    ?>
</div>
