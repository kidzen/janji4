<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DraftParam */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="draft-param-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id')->textInput(['placeholder' => 'Id']) ?>

    <?= $form->field($model, 'draft_document_id')->textInput(['placeholder' => 'Draft Document']) ?>

    <?= $form->field($model, 'param_list_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\ParamList::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Param list'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'param')->textInput(['maxlength' => true, 'placeholder' => 'Param']) ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>

    <?= $form->field($model, 'deleted_at')->textInput(['maxlength' => true, 'placeholder' => 'Deleted At']) ?>

    <?= $form->field($model, 'deleted_by')->textInput(['placeholder' => 'Deleted By']) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true, 'placeholder' => 'Created At']) ?>

    <?= $form->field($model, 'created_by')->textInput(['placeholder' => 'Created By']) ?>

    <?= $form->field($model, 'updated_at')->textInput(['maxlength' => true, 'placeholder' => 'Updated At']) ?>

    <?= $form->field($model, 'updated_by')->textInput(['placeholder' => 'Updated By']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
