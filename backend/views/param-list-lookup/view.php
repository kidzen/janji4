<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\ParamListLookup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Param List Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-list-lookup-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Param List Lookup'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'id',
        'name',
        'param',
        'hint',
        'description',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerParamList->totalCount){
    $gridColumnParamList = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'template_id',
                        'remark',
            'status',
            'deleted_at',
            'deleted_by',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerParamList,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-param-list']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Param List'),
        ],
        'export' => false,
        'columns' => $gridColumnParamList
    ]);
}
?>

    </div>
</div>
