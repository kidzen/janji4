<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\DraftTemplate */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Template', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-template-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Template'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'id',
        'name',
        'description',
        'file',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerJANJIApproveLevel->totalCount){
    $gridColumnJANJIApproveLevel = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'approve_level',
                        'remark',
            'status',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerJANJIApproveLevel,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-janji.approve-level']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Janjiapprove Level'),
        ],
        'export' => false,
        'columns' => $gridColumnJANJIApproveLevel
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerJANJIDraftDocument->totalCount){
    $gridColumnJANJIDraftDocument = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'file',
                        'contract_id',
            'draft_status_id',
            'description',
            'signator1',
            'signator2',
            'remark',
            'status',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerJANJIDraftDocument,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-janji.draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Janjidraft Document'),
        ],
        'export' => false,
        'columns' => $gridColumnJANJIDraftDocument
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerContract->totalCount){
    $gridColumnContract = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'contract_no',
            'name',
            'awarded_date',
            'juu_staff_assigned',
            'department_staff_assigned',
                        'remark',
            'status',
            'deleted_at',
            'deleted_by',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContract,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contract']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Contract'),
        ],
        'export' => false,
        'columns' => $gridColumnContract
    ]);
}
?>

    </div>
</div>
