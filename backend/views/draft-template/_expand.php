<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('DraftTemplate'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Janjiapprove Level'),
        'content' => $this->render('_dataJANJIApproveLevel', [
            'model' => $model,
            'row' => $model->jANJIApproveLevels,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Janjidraft Document'),
        'content' => $this->render('_dataJANJIDraftDocument', [
            'model' => $model,
            'row' => $model->jANJIDraftDocuments,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Contract'),
        'content' => $this->render('_dataContract', [
            'model' => $model,
            'row' => $model->contracts,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
