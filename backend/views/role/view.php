<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Role', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Role'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'id',
        'name',
        'description',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerUSERPENDAKWAANUser->totalCount){
    $gridColumnUSERPENDAKWAANUser = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'username',
            'staff_no',
            'auth_key',
            'profile_pic',
                        'password_hash',
            'password_reset_token',
            'email:email',
            'remark',
            'status',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUSERPENDAKWAANUser,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-userpendakwaan.user']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Userpendakwaanuser'),
        ],
        'export' => false,
        'columns' => $gridColumnUSERPENDAKWAANUser
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerJANJIUser->totalCount){
    $gridColumnJANJIUser = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'username',
            'staff_no',
            'auth_key',
            'profile_pic',
                        'password_hash',
            'password_reset_token',
            'email:email',
            'remark',
            'status',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerJANJIUser,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-janji.user']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Janjiuser'),
        ],
        'export' => false,
        'columns' => $gridColumnJANJIUser
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerUser->totalCount){
    $gridColumnUser = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'username',
            'auth_key',
            'profile_pic',
                        'email:email',
            'remark',
            'status',
            'deleted_at',
            'deleted_by',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUser,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-user']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('User'),
        ],
        'export' => false,
        'columns' => $gridColumnUser
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerDAKWAUser->totalCount){
    $gridColumnDAKWAUser = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'username',
            'staff_no',
            'auth_key',
            'profile_pic',
                        'password_hash',
            'password_reset_token',
            'email:email',
            'remark',
            'status',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDAKWAUser,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-dakwa.user']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Dakwauser'),
        ],
        'export' => false,
        'columns' => $gridColumnDAKWAUser
    ]);
}
?>

    </div>
</div>
