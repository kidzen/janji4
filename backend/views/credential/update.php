<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Credential */

$this->title = 'Update Credential: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Credential', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="credential-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
