<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Contract */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Attachment',
        'relID' => 'attachment',
        'value' => \yii\helpers\Json::encode($model->attachments),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Note',
        'relID' => 'note',
        'value' => \yii\helpers\Json::encode($model->notes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id')->textInput(['placeholder' => 'Id']) ?>

    <?= $form->field($model, 'contract_no')->textInput(['maxlength' => true, 'placeholder' => 'Contract No']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'awarded_date')->textInput(['maxlength' => true, 'placeholder' => 'Awarded Date']) ?>

    <?= $form->field($model, 'juu_staff_assigned')->textInput(['placeholder' => 'Juu Staff Assigned']) ?>

    <?= $form->field($model, 'department_staff_assigned')->textInput(['placeholder' => 'Department Staff Assigned']) ?>

    <?= $form->field($model, 'template_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\DraftTemplate::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Draft template'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>

    <?= $form->field($model, 'deleted_at')->textInput(['maxlength' => true, 'placeholder' => 'Deleted At']) ?>

    <?= $form->field($model, 'deleted_by')->textInput(['placeholder' => 'Deleted By']) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true, 'placeholder' => 'Created At']) ?>

    <?= $form->field($model, 'created_by')->textInput(['placeholder' => 'Created By']) ?>

    <?= $form->field($model, 'updated_at')->textInput(['maxlength' => true, 'placeholder' => 'Updated At']) ?>

    <?= $form->field($model, 'updated_by')->textInput(['placeholder' => 'Updated By']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Attachment'),
            'content' => $this->render('_formAttachment', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->attachments),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Note'),
            'content' => $this->render('_formNote', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->notes),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
