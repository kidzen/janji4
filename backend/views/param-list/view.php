<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\ParamList */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Param List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-list-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Param List'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'id',
        'template_id',
        [
            'attribute' => 'param.name',
            'label' => 'Param',
        ],
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerDraftParam->totalCount){
    $gridColumnDraftParam = [
        ['class' => 'yii\grid\SerialColumn'],
            'id',
            'draft_document_id',
                        'param',
            'remark',
            'status',
            'deleted_at',
            'deleted_by',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftParam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-param']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Draft Param'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftParam
    ]);
}
?>

    </div>
    <div class="row">
        <h4>ParamListLookup<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnParamListLookup = [
        'id',
        'name',
        'param',
        'hint',
        'description',
        'remark',
        'status',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
    echo DetailView::widget([
        'model' => $model->param,
        'attributes' => $gridColumnParamListLookup    ]);
    ?>
</div>
