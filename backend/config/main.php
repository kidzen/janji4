<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log','common\components\Settings',],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'class' => 'common\components\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            // 'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'janji3-frontend',
        ],
        'access' => [
            'class' => 'common\components\AccessRule',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',

            // format settings for displaying each date attribute (ICU format example)
            'displaySettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'php:d-M-y',
                \kartik\datecontrol\Module::FORMAT_TIME => 'hh:mm:ss a',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'dd-MM-yyyy hh:mm:ss a',
            ],

            // format settings for saving each date attribute (PHP format example)
            'saveSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'php:d-M-y',
                \kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'php:d-M-Y H:i:s',
            ],

            // set your display timezone
            'displayTimezone' => 'Asia/Kuala_Lumpur',

            // set your timezone for date saved to db
            'saveTimezone' => 'Asia/Kuala_Lumpur',

            // automatically use kartik\widgets for each of the above formats
            'autoWidget' => true,

            // default settings for each widget from kartik\widgets used when autoWidget is true
            'autoWidgetSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => [
                'type'=>2, 'pluginOptions'=>['autoclose'=>true,'todayHighlight' => true,//'todayBtn' => true,
                ]], // example
                \kartik\datecontrol\Module::FORMAT_DATETIME => [
                // 'type'=>2, 'pluginOptions'=>['autoclose'=>true,'todayHighlight' => true,//'todayBtn' => true,
                ], // setup if needed
                \kartik\datecontrol\Module::FORMAT_TIME => [], // setup if needed
            ],
        ],
        // If you use tree table
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',

        ],
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20','*'],
        ],
        // 'webshell' => [
        //     'class' => 'samdark\webshell\Module',
        //     'yiiScript' => Yii::getAlias('@root'). '/yii', // adjust path to point to your ./yii script
        //     // 'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.2'],
        //     // 'checkAccessCallback' => function (\yii\base\Action $action) {
        //         // return true if access is granted or false otherwise
        //     //     return true;
        //     // }
        // ],
    ],    'params' => $params,
];
