<?php

namespace backend\models;

use Yii;
use \backend\models\base\Attachment as BaseAttachment;

/**
 * This is the model class for table "attachment".
 */
class Attachment extends BaseAttachment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'contract_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'file', 'remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
