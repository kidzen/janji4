<?php

namespace backend\models;

use Yii;
use \backend\models\base\DocumentChecklist as BaseDocumentChecklist;

/**
 * This is the model class for table "document_checklist".
 */
class DocumentChecklist extends BaseDocumentChecklist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'doc_check_id', 'contract_id', 'completed', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['file', 'remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
