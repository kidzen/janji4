<?php

namespace backend\models;

use Yii;
use \backend\models\base\DocumentChecklistLookup as BaseDocumentChecklistLookup;

/**
 * This is the model class for table "document_checklist_lookup".
 */
class DocumentChecklistLookup extends BaseDocumentChecklistLookup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['category', 'name', 'file', 'description', 'remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
