<?php

namespace backend\models;

use Yii;
use \backend\models\base\Credential as BaseCredential;

/**
 * This is the model class for table "credential".
 */
class Credential extends BaseCredential
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'number'],
            [['staff_no'], 'string', 'max' => 20],
            [['password'], 'string', 'max' => 6]
        ]);
    }
	
}
