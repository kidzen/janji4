<?php

namespace backend\models;

use Yii;
use \backend\models\base\Contract as BaseContract;

/**
 * This is the model class for table "contract".
 */
class Contract extends BaseContract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'juu_staff_assigned', 'department_staff_assigned', 'template_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['contract_no'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['contract_no', 'name', 'remark'], 'string', 'max' => 255],
            [['awarded_date'], 'string', 'max' => 7],
            [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.']
        ]);
    }
	
}
