<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "draft_document".
 *
 * @property integer $id
 * @property string $name
 * @property string $file
 * @property integer $contract_id
 * @property integer $draft_status_id
 * @property string $description
 * @property string $remark
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \backend\models\DraftStatus $draftStatus
 * @property \backend\models\JANJIApprover[] $jANJIApprovers
 */
class DraftDocument extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            'deleted_at' => date('d-M-y h.i.s a'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'draftStatus',
            'jANJIApprovers'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contract_id', 'draft_status_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'file', 'description', 'remark'], 'string', 'max' => 255],
            [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'draft_document';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'file' => 'File',
            'contract_id' => 'Contract ID',
            'draft_status_id' => 'Draft Status ID',
            'description' => 'Description',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDraftStatus()
    {
        return $this->hasOne(\backend\models\DraftStatus::className(), ['id' => 'draft_status_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJANJIApprovers()
    {
        return $this->hasMany(\backend\models\JANJIApprover::className(), ['draft_document_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-y h.i.s a'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->username,
            ],
        ];
    }
}
