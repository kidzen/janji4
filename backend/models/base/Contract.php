<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "contract".
 *
 * @property integer $id
 * @property string $contract_no
 * @property string $name
 * @property string $awarded_date
 * @property integer $juu_staff_assigned
 * @property integer $department_staff_assigned
 * @property integer $template_id
 * @property string $remark
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \backend\models\Attachment[] $attachments
 * @property \backend\models\DraftTemplate $template
 * @property \backend\models\Note[] $notes
 * @property \backend\models\JANJINote[] $jANJINotes
 */
class Contract extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            'deleted_at' => date('d-M-y h.i.s a'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'attachments',
            'template',
            'notes',
            'jANJINotes'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'juu_staff_assigned', 'department_staff_assigned', 'template_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['contract_no'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['contract_no', 'name', 'remark'], 'string', 'max' => 255],
            [['awarded_date'], 'string', 'max' => 7],
            [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_no' => 'Contract No',
            'name' => 'Name',
            'awarded_date' => 'Awarded Date',
            'juu_staff_assigned' => 'Juu Staff Assigned',
            'department_staff_assigned' => 'Department Staff Assigned',
            'template_id' => 'Template ID',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(\backend\models\Attachment::className(), ['contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(\backend\models\DraftTemplate::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes()
    {
        return $this->hasMany(\backend\models\Note::className(), ['contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getJANJINotes()
    // {
    //     return $this->hasMany(\backend\models\JANJINote::className(), ['contract_id' => 'id']);
    // }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-y h.i.s a'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->username,
            ],
        ];
    }
}
