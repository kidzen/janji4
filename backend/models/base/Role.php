<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "role".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $remark
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \backend\models\User[] $users
 * @property \backend\models\USERPENDAKWAANUser[] $uSERPENDAKWAANUsers
 * @property \backend\models\JANJIUser[] $jANJIUsers
 * @property \backend\models\DAKWAUser[] $dAKWAUsers
 */
class Role extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            'deleted_at' => date('d-M-y h.i.s a'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'users',
            'uSERPENDAKWAANUsers',
            'jANJIUsers',
            'dAKWAUsers'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(\backend\models\User::className(), ['role_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSERPENDAKWAANUsers()
    {
        return $this->hasMany(\backend\models\USERPENDAKWAANUser::className(), ['role_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJANJIUsers()
    {
        return $this->hasMany(\backend\models\JANJIUser::className(), ['role_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDAKWAUsers()
    {
        return $this->hasMany(\backend\models\DAKWAUser::className(), ['role_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-y h.i.s a'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->username,
            ],
        ];
    }
}
