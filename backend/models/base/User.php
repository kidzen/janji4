<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "user".
 *
 * @property integer $id
 * @property integer $username
 * @property string $auth_key
 * @property string $profile_pic
 * @property integer $role_id
 * @property string $email
 * @property string $remark
 * @property integer $status
 * @property string $deleted_at
 * @property integer $deleted_by
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \backend\models\Role $role
 */
class User extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->username,
            'deleted_at' => date('d-M-y h.i.s a'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'role'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username', 'role_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['username', 'auth_key', 'email'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['auth_key'], 'string', 'max' => 32],
            [['profile_pic', 'email', 'remark'], 'string', 'max' => 255],
            [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'profile_pic' => 'Profile Pic',
            'role_id' => 'Role ID',
            'email' => 'Email',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(\backend\models\Role::className(), ['id' => 'role_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('d-M-y h.i.s a'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => \Yii::$app->user->username,
            ],
        ];
    }
}
