<?php

namespace backend\models;

use Yii;
use \backend\models\base\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'username', 'role_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['username', 'auth_key', 'email'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['auth_key'], 'string', 'max' => 32],
            [['profile_pic', 'email', 'remark'], 'string', 'max' => 255],
            [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.']
        ]);
    }
	
}
