<?php

namespace backend\models;

use Yii;
use \backend\models\base\Profile as BaseProfile;

/**
 * This is the model class for table "profile".
 */
class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'number'],
            [['staff_no'], 'string', 'max' => 20],
            [['name', 'position'], 'string', 'max' => 100],
            [['department'], 'string', 'max' => 255]
        ]);
    }
	
}
