<?php

namespace backend\models;

use Yii;
use \backend\models\base\Settings as BaseSettings;

/**
 * This is the model class for table "settings".
 */
class Settings extends BaseSettings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'integer'],
            [['key', 'value'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
