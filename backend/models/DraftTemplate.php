<?php

namespace backend\models;

use Yii;
use \backend\models\base\DraftTemplate as BaseDraftTemplate;

/**
 * This is the model class for table "draft_template".
 */
class DraftTemplate extends BaseDraftTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'file', 'remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
