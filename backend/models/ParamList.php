<?php

namespace backend\models;

use Yii;
use \backend\models\base\ParamList as BaseParamList;

/**
 * This is the model class for table "param_list".
 */
class ParamList extends BaseParamList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'template_id', 'param_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
