<?php

namespace backend\models;

use Yii;
use \backend\models\base\Note as BaseNote;

/**
 * This is the model class for table "note".
 */
class Note extends BaseNote
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'contract_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['notes', 'remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
