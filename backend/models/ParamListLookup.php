<?php

namespace backend\models;

use Yii;
use \backend\models\base\ParamListLookup as BaseParamListLookup;

/**
 * This is the model class for table "param_list_lookup".
 */
class ParamListLookup extends BaseParamListLookup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'param', 'hint', 'description', 'remark'], 'string', 'max' => 255],
            [['id'], 'unique']
        ]);
    }
	
}
