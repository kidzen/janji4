<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ChecklistFormAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/dist';
    public $css = [
    ];
    public $js = [
        // 'js/checklist-form.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
