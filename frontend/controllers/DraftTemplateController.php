<?php

namespace frontend\controllers;

use Yii;
use common\models\DraftTemplate;
use common\models\search\DraftTemplateSearch;
use yii\web\Controller;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DraftTemplateController implements the CRUD actions for DraftTemplate model.
 */
class DraftTemplateController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DraftTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DraftTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DraftTemplate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerContract = new \yii\data\ArrayDataProvider([
            'allModels' => $model->contracts,
        ]);
        $providerParamList = new \yii\data\ArrayDataProvider([
            'allModels' => $model->paramLists,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerContract' => $providerContract,
            'providerParamList' => $providerParamList,
        ]);
    }

    /**
     * Creates a new DraftTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DraftTemplate();
        // $model->rules();die;
        if(Yii::$app->request->isPost && $model->loadAll(Yii::$app->request->post())) {
            try {
                // if(!Yii::$app->request->post('ParamList')){
                //     // $this->addError(' Draft parameter cannot be blank.');
                //     throw new Exception(' Sila isi parameter template.');
                // }
                // $model->tempFile = UploadedFile::getInstance($model, 'tempFile');
                // var_dump($model->tempFile);die;
                if(!$model->upload()) {
                    throw new Exception(' Proses muat naik fail gagal.');
                }

                \Yii::$app->notify->success();
                return $this->redirect(['index']);
            } catch (Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());

            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);

        // if(Yii::$app->request->post()) {
        //     try {
        //         $model->loadAll(Yii::$app->request->post());
        //         if(!Yii::$app->request->post('ParamList')){
        //             // $model->addError(' Draft parameter cannot be blank.');
        //             throw new Exception(' Sila isi parameter template.');
        //         }

        //         $model->tempFile = \yii\web\UploadedFile::getInstance($model, 'file');
        //         // $fileName2 = 'originalTemplate/template/' . $model->tempFile->baseName . '.' . $model->tempFile->extension;
        //         $fileName = 'uploads/' . $model->tempFile->baseName . '_'.time(). '.' . $model->tempFile->extension;
        //         if(file_exists($fileName)) {
        //             // $model->addError(' File already exist.');
        //             throw new Exception(' Template yang sama telah wujud.');
        //         }
        //         if($model->tempFile->saveAs($fileName)){
        //             // return 'success';
        //             $model->file = $fileName;
        //             if($model->save()){
        //                 unlink($fileName);
        //             }
        //         }
        //         return $this->redirect(['index']);
        //     } catch (Exception $e) {
        //         // var_dump($e);die;
        //         \Yii::$app->notify->fail($e->getMessage());
        //         // return $this->render('create', [
        //         //     'model' => $model,
        //         // ]);

        //     }
        // }
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
        // die;

        // if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
            // return $this->render('create', [
            //     'model' => $model,
            // ]);
        // }
    }

    /**
     * Updates an existing DraftTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->post() && $model->loadAll(Yii::$app->request->post())) {
            try {
                if(!$model->upload()) {
                    throw new Exception(' Proses muat naik fail gagal.');
                }
                \Yii::$app->notify->success();
                return $this->redirect(['index']);
            } catch (Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());

            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
        // if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Deletes an existing DraftTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }


    /**
     * Finds the DraftTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DraftTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DraftTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Action to load a tabular form grid
    * for Contract
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddContract()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Contract');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formContract', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Action to load a tabular form grid
    * for ParamList
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddParamList()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('ParamList');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formParamList', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
