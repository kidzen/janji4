<?php

namespace frontend\controllers\custom;

use Yii;
use common\models\DraftDocument;
use common\models\search\DraftDocumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;

/**
 * DraftDocumentController implements the CRUD actions for DraftDocument model.
 */
class DraftDocumentController extends Controller
{

    // public function actionApproveStatus($id =1)
    // {
    //     $dbtransac = \Yii::$app->db->beginTransaction();
    //     try {
    //         DraftDocument::approval($id);
    //         \Yii::$app->notify->success();
    //         $dbtransac->commit();
    //     } catch (Exception $e) {
    //         \Yii::$app->notify->fail($e->getMessage());
    //         $dbtransac->rollBack();
    //     } catch (\yii\db\Exception $e) {
    //         \Yii::$app->notify->fail($e->getMessage());
    //         $dbtransac->rollBack();
    //     }

    //     return $this->redirect(['index']);
    // }

    // public function actionRejectStatus($id =1)
    // {
    //     $model = DraftDocument::findOne($id);
    //     $dbtransac = \Yii::$app->db->beginTransaction();
    //     try {
    //         DraftDocument::reject($id);
    //         // Throw new Exception(' Data draf tidak berjaya dikemaskini.');
    //         \Yii::$app->notify->success();
    //         $dbtransac->commit();
    //     } catch (Exception $e) {
    //         \Yii::$app->notify->fail($e->getMessage());
    //         $dbtransac->rollBack();
    //     } catch (\yii\db\Exception $e) {
    //         \Yii::$app->notify->fail($e->getMessage());
    //         $dbtransac->rollBack();
    //     }

    //     return $this->redirect(['index']);

    // }
}
