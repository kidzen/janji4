<?php

namespace frontend\controllers;

use Yii;
use common\models\Contract;
use common\models\search\ContractSearch;
use yii\web\Controller;
use frontend\controllers\RequestController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ContractController extends RequestController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy([ 'contract.id' => SORT_DESC]);
        $simplified = Yii::$app->params['settings']['simplifiedVersion'];
        if($simplified) {
            $dataProvider->query->with('juuStaffAssigned','departmentStaffAssigned','template');
        } else {
            $dataProvider->query->with(\common\models\Contract::relationNames());
        }
        return $this->render('index', [
            'simplified' => $simplified,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contract model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        // var_dump($model);die;
        $providerAttachment = new \yii\data\ArrayDataProvider([
            'allModels' => $model->attachments,
        ]);
        $providerDocumentChecklist = new \yii\data\ArrayDataProvider([
            'allModels' => $model->documentChecklists,
        ]);
        $providerDraftDocument = new \yii\data\ArrayDataProvider([
            'allModels' => $model->draftDocuments,
        ]);
        $providerNote = new \yii\data\ArrayDataProvider([
            'allModels' => $model->notes,
        ]);
        $providerDraftStatuses = new \yii\data\ArrayDataProvider([
            'allModels' => $model->draftStatuses,
        ]);
        return $this->render('view', [
            'model' => $model,
            'providerAttachment' => $providerAttachment,
            'providerDocumentChecklist' => $providerDocumentChecklist,
            'providerDraftDocument' => $providerDraftDocument,
            'providerDraftStatuses' => $providerDraftStatuses,
            'providerNote' => $providerNote,
        ]);
    }

    /**
     * Creates a new Contract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contract();
        // if(Yii::$app->request->post()){
        //     var_dump(Yii::$app->request->post());
        //     $model->loadAll(Yii::$app->request->post());
        //     var_dump($model->saveAll());
        //     die;
        // }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contract model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // var_dump(Yii::$app->request->post());die;
        // var_dump($model->loadAll(Yii::$app->request->post()));die;

        // if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll(['departmentStaffAssigned'])) {
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll(['departmentStaffAssigned','documentChecklists'])) {
            Yii::$app->notify->success();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contract model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->deleteWithRelated()) {
            Yii::$app->notify->success();
        }
        return $this->redirect(['index']);
    }


    /**
     * Finds the Contract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Action to load a tabular form grid
    * for Attachment
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddAttachment()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Attachment');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formAttachment', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Action to load a tabular form grid
    * for DocumentChecklist
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddDocumentChecklist()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('DocumentChecklist');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formDocumentChecklist', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Action to load a tabular form grid
    * for DraftDocument
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddDraftDocument()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('DraftDocument');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formDraftDocument', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Action to load a tabular form grid
    * for Note
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddNote()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Note');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formNote', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
