<?php

namespace frontend\controllers;

use Yii;
use common\models\Contract;
use common\models\search\ContractSearch;
use common\models\DraftDocument;
use common\models\search\DraftDocumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class RequestController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionContractList()
    {
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('contract/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new Contract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateContract()
    {
        $model = new Contract();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/contract/view','id'=>$model->id]);
            // return $this->redirect(['contract-list']);
        } else {
            return $this->render('contract/create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateChecklist($id)
    {

        $contract = \common\models\Contract::findOne($id);
        $data = \common\models\Contract::getchecklists($contract->id);
        // var_dump($data);
        // die;
        //check access
        if(!$contract->can('edit')) {
            Throw new \yii\web\ForbiddenHttpException(' Anda tidak dibenarkan melepasi tahap ini. Sila berhubung dengan pegawai atasan untuk sebarang modifikasi.',403);
            return $this->redirect(Yii::$app->request->referrer);
        }

        if(Yii::$app->request->post()){
            $doc_checklist = Yii::$app->request->post('checklist');
        // var_dump($doc_checklist);
        // var_dump(\common\models\Contract::getchecklists($contract->id));
        // die;
            $dbTransac = Yii::$app->db->beginTransaction();
            try {
                //check if checklist is empty

                //
                if($doc_checklist) {
                    $valid = true;
                    // $existedDocumentChecklist = $contract->getDocumentChecklists()
                    // ->select('id,doc_check_id')
                    // ->asArray()->all();
                    // // \yii\helpers\ArrayHelper::isIn()
                    // var_dump($doc_checklist);
                    // var_dump($existedDocumentChecklist);
                    // die;
                    foreach ($doc_checklist as $key => $value) {
                        # code...
                        Yii::info($value);
                        if($value) {
                            if(array_key_exists($key, $data)){
                                $model = \common\models\DocumentChecklist::findOne($data[$key]['id']);
                            } else {
                                $model = new \common\models\DocumentChecklist();
                            }
                            $model->doc_check_id = $key;
                            $model->contract_id = $id;
                            $model->completed = $value;
                            // $valid = true && $valid;
                            $valid = $model->save() && $valid;
                        } else {
                            if(array_key_exists($key, $data)){
                                $model = \common\models\DocumentChecklist::findOne($data[$key]['id']);
                                $valid = $model->deleteWithRelated() && $valid;
                                // $valid = $model->delete() && $valid;
                            }
                        }
                        if(!$valid){
                            die;
                            throw new Exception(" Proses menyimpan data gagal.", 1);
                            $dbTransac->rollback();
                        }
                    }
                } else {
                    throw new Exception(" Tiada data untuk dikemaskini.", 1);
                    $dbTransac->rollback();
                }
                Yii::$app->notify->success(" Proses kemaskini data berjaya.");
                $dbTransac->commit();
                return $this->redirect(['/contract/view','id'=>$contract->id]);
            } catch (Exception $e) {
                Yii::$app->notify->fail($e->getMessage());
                return $this->render('documentChecklistForm', [
                    'model' => $contract,
                ]);
            }
        }
        // return $this->redirect(['update-checklist']);
        return $this->render('documentChecklistForm', [
            // 'checklistArray2' => $checklistArray2,
            'model' => $contract,
            'data'=>$data
            // 'array' => $array,
        ]);
    }

    public function actionUpdateChecklistAjax()
    {
        $flag = true;
        $doc_checklist = Yii::$app->request->post('doc_checklist');
        if($doc_checklist) {
            foreach ($doc_checklist as $key => $value) {
                # code...
                // Yii::warning($value['key']);
                $model = new \common\models\DocumentChecklist();
                $model->doc_check_id = $value['key'];
                $model->contract_id = $value['contract_id'];
                $model->completed = $value['value'];
                $flag = $model->save() && $flag;
            }
            Yii::warning($flag);
            return $flag;
        }
        return false;
    }

    public function actionCreateDraft($id,$ext = 'doc')
    {
        $model = new DraftDocument();
        $contract = \common\models\Contract::findOne($id);
        $templateId = $contract->template_id;
        $paramListId = \common\models\ParamListLookup::find()
        ->select('param_list_lookup.*')
        ->joinWith('paramLists')
        ->where(['param_list.template_id'=>$templateId])
        ->asArray()->all();

        $paramList = [];
        $paramLabelList = [];
        $paramIdList = [];
        foreach ($paramListId as $key => $value) {
            $paramList[$key] = $value['param'];
            $paramLabelList[$value['param']] = $value['name'];
            $paramIdList[$value['param']] = $value['id'];
        }

        $dynaModel = new \common\components\DynamicModel($paramList);
        $dynaModel->addRule($paramList, 'safe');
        if(Yii::$app->params['settings']['draftParamRequired']) {
          $dynaModel->addRule($paramList, 'required');
        }
        $dynaModel->attributeLabels = $paramLabelList;

        if ($post = Yii::$app->request->post()) {
            //reinit posted data before load into model
            $dynamicModelValue = $post['DynamicModel'];
            $model->load($post);
            $contract->load($post);
            $dynaModel->load($post);

            $validate = $model->validate() && $contract->validate() && $dynaModel->validate();
            if(!$validate) {
                Throw new Exception(' Sila pastikan data diisi dengan betul.');
            }

            $dbtransac = \Yii::$app->db->beginTransaction();
            try {
                //check for pending draft before create new draft
                // var_dump(DraftDocument::find()->pending($id)->asArray()->one());die;
                if(DraftDocument::find()->pending($id)) {
                    Throw new Exception(' Draf sedia ada masih menunggu pengesahan.');
                }

                //save contract
                if(!$contract->save()){
                    Throw new Exception(' Data kontrak tidak berjaya dikemaskini.');
                }
                //save draft
                $model->link('contract',$contract);

                // create draft status log
                $draftStatus = new \common\models\DraftStatus();
                $draftStatus->draft_id = $model->id;
                $draftStatus->status_id = 1;
                $draftStatus->link('draft',$model);
                // if(!$draftStatus->link('draft',$model)){
                //     Throw new Exception(' Status draf tidak berjaya dikemaskini. Sila pastikan draf diisi dengan betul.');
                // }

                // create draft param
                foreach ($dynamicModelValue as $key => $value) {
                    $draftParam = new \common\models\DraftParam();
                    $draftParam->draft_document_id = $model->id;
                    $draftParam->param_list_id = $paramIdList[$key];
                    $draftParam->param = $value;
                    if(!$draftParam->save()){
                        Throw new Exception(' Data draf tidak berjaya dikemaskini. Sila pastikan draf diisi dengan betul.');
                    }
                }


                if(!DraftDocument::generate($model,$ext)){
                    Throw new Exception(' Dokumen gagal dijana.');
                }
                $dbtransac->commit();
                // return $this->redirect(['/draft-document/index']);
            } catch (Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());
                $dbtransac->rollBack();
                return $this->render('_formDraft', [
                    'model' => $model,
                    'contract' => $contract,
                    'paramList'=>$paramList,
                    'dynaModel'=>$dynaModel,
                    'templateId'=>$templateId
                ]);
            } catch (\yii\db\Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());
                $dbtransac->rollBack();
                return $this->render('_formDraft', [
                    'model' => $model,
                    'contract' => $contract,
                    'paramList'=>$paramList,
                    'dynaModel'=>$dynaModel,
                    'templateId'=>$templateId
                ]);
            }
        } else {
            return $this->render('_formDraft', [
                'model' => $model,
                'contract' => $contract,
                'paramList'=>$paramList,
                'dynaModel'=>$dynaModel,
                'templateId'=>$templateId
            ]);
        }
        return $this->redirect(['/contract/view/','id'=>$id]);
    }

    public function actionGenerateDraft($id =1,$ext ='doc')
    {
        $model = DraftDocument::findOne($id);
        if(DraftDocument::generate($model,$ext)) {
            Yii::$app->notify->success(' Fail berjaya dijana.');
            // $this->redirect($model->file);
            // return $this->redirect($model->file);
        }
        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionUpdateStatus($id)
    {
        $contract = \common\models\Contract::findOne($id);
        // if(!($draftId && $id && $statusId && $contract)) {
        if(!$contract) {
            Yii::$app->notify->fail();
            return $this->redirect(['/request/contract-list']);
        }
        $paramList = ['draft_id','contract_id','status_id','note','juu_staff_assigned'];
        $paramLabelList = [
            'Status'=>'draft_id',
            'No Kontrak'=>'contract_id',
            'Kepada'=>'juu_staff_assigned',
            'Status'=>'status_id',
            'Nota'=>'note',
        ];
        $model = new \common\components\DynamicModel($paramList);
        $model->addRule($paramList, 'safe');
        $model->addRule(['draft_id','contract_id','status_id'], 'required');
        $model->attributeLabels = $paramLabelList;
        $model->draft_id = $contract['activeDocument']['id'];
        $model->contract_id = $id;
        $model->status_id = $contract->status;
        $model->juu_staff_assigned = $contract->juu_staff_assigned;

        if(Yii::$app->request->isPost) {
            $posted = Yii::$app->request->post('DynamicModel');
            $dbtransac = \Yii::$app->db->beginTransaction();
            try {
                DraftDocument::updateStatus($posted);
                \Yii::$app->notify->success();
                $dbtransac->commit();
            } catch (Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());
                $dbtransac->rollBack();
            }
            return $this->redirect(['/contract/view/','id'=>$id]);
        }

        return $this->render('/request/contract/_formUpdateStatus',[
            'model'=>$model
        ]);

    }

    public function actionSend($draftId)
    {
        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            DraftDocument::approval($draftId);
            \Yii::$app->notify->success();
            $dbtransac->commit();
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(['/request/contract-list']);
    }

    public function actionReject($draftId =1)
    {
        $model = DraftDocument::findOne($draftId);
        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            DraftDocument::reject($draftId);
            // Throw new Exception(' Data draf tidak berjaya dikemaskini.');
            \Yii::$app->notify->success();
            $dbtransac->commit();
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(['contract-list']);

    }

    public function actionDownloadDocument($id)
    {
        $doc = \common\models\DraftDocument::findOne($id);

        return $this->redirect($doc->file);
    }

    public function actionUploadTemplate($id)
    {
        // $doc = \common\models\DraftDocument::findOne();

        // return $this->render('index2',['file' => $file]);
    }

    public function actionUploadChecklist($id)
    {
        return $this->redirect(['/contract/update','id'=>$id]);
    }



    public function actionAddNote()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Note');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('/contract/_formNote', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRegisterUser($staffNo,$role)
    {
        $profile = \common\models\Profile::findByStaffNo($staffNo);

        if ($profile->user) {
            $profile->user->role_id = $role;
            $profile->user->save(false);
            return $this->redirect(['/user/view','id'=>$profile->user->id]);
        } else if(!$profile->user){
            $user = new \common\models\User();
            $user->role_id = $role;
            $user->username = $staffNo;
            $user->generateAuthKey();
            $user->save(false);
            $profile->link('user',$user);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



}
