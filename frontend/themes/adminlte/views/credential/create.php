<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Credential */

$this->title = 'Tambah Credential';
$this->params['breadcrumbs'][] = ['label' => 'Credential', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credential-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
