<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Credential */

$this->title = 'Kemaskini Credential: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Credential', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Kemaskini';
?>
<div class="credential-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
