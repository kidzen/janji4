<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Credential */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Credential', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credential-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Credential'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'staffNo.username',
            'label' => 'Staff No',
        ],
        'password',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'profile_pic',
        'role_id',
        'email',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->staffNo,
        'attributes' => $gridColumnUser    ]);
    ?>
</div>
