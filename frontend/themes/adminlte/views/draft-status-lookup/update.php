<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DraftStatusLookup */

$this->title = 'Kemaskini Draft Status Lookup: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Status Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Kemaskini';
?>
<div class="draft-status-lookup-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
