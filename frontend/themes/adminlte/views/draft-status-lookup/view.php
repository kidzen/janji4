<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftStatusLookup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draft Status Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-status-lookup-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Status Lookup'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerDraftStatus->totalCount){
    $gridColumnDraftStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'draft.name',
                'label' => 'Draft'
            ],
            'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Status Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftStatus
    ]);
}
?>

    </div>
</div>
