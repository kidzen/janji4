<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DraftStatusLookup */

$this->title = 'Tambah Draft Status Lookup';
$this->params['breadcrumbs'][] = ['label' => 'Draft Status Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-status-lookup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
