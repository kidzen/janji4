<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\base\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header">
                    <h2  class="box-title"><?= Html::encode($this->title) ?></h2>
                </div>
                <div class="box-body">

                    <p>
                        <?= Html::a('Tambah Settings', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'key',
                            'value',

                            [
                                'class' => 'yii\grid\ActionColumn'
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    <!-- </div> -->
</div>
