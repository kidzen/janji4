<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftTemplate */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Templat Draf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-template-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Templat Draf'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'file',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerContract->totalCount){
    $gridColumnContract = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'contract_no',
            'name',
            'awarded_date',
            [
                'attribute' => 'juuStaffAssigned.name',
                'label' => 'PIC JUU'
            ],
            [
                'attribute' => 'departmentStaffAssigned.name',
                'label' => 'PIC Jabatan'
            ],
                        'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerContract,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contract']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kontrak'),
        ],
        'export' => false,
        'columns' => $gridColumnContract
    ]);
}
?>

    </div>

    <div class="row">
<?php
if($providerParamList->totalCount){
    $gridColumnParamList = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'param.name',
                'label' => 'Param'
            ],
            'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerParamList,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-param-list']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Param List'),
        ],
        'export' => false,
        'columns' => $gridColumnParamList
    ]);
}
?>

    </div>
</div>
