<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DraftTemplate */

$this->title = 'Tambah Templat Draf';
$this->params['breadcrumbs'][] = ['label' => 'Templat Draf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
