<div class="form-group" id="add-draft-param">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'DraftParam',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'param_list_id' => Yii::$app->user->isAdmin ?
        [
        // 'visible' => ,
            'label' => 'Param list',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\ParamList::find()->with('param')->orderBy('id')->asArray()->all(), 'id', 'param.name'),
                'options' => ['placeholder' => 'Pilih Param list'],
            ],
            'columnOptions' => ['width' => '200px']
        ] :
        [
            'visible' => !Yii::$app->user->isAdmin,
            'type' => TabularForm::INPUT_STATIC,
            'value'=>function ($model) {
                $draftParam =
                \common\models\DraftParam::find()
                ->select('param_list_lookup.name,draft_param.param_list_id')
                ->joinWith('param')
                ->where(['draft_param.id'=>$model['id']])
                ->asArray()
                ->one();
                return $draftParam['param']['name'];
            }
        ],
        'param' => ['type' => TabularForm::INPUT_TEXT],
        'remark' => ['type' => TabularForm::INPUT_TEXT],
    // 'status' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'visible' => Yii::$app->user->isAdmin,
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Hapus', 'onClick' => 'delRowDraftParam(' . $key . '); return false;', 'id' => 'draft-param-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => (Yii::$app->user->isAdmin) ? Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Tambah Draft Param', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowDraftParam()']) : '',
        ]
    ]
]);
echo  "    </div>\n\n";
?>

