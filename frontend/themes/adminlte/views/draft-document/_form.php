<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'DraftParam',
        'relID' => 'draft-param',
        'value' => \yii\helpers\Json::encode($model->draftParams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
// \mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
//     'viewParams' => [
//         'class' => 'DraftStatus',
//         'relID' => 'draft-status',
//         'value' => \yii\helpers\Json::encode($model->draftStatuses),
//         'isNewRecord' => ($model->isNewRecord) ? 1 : 0
//     ]
// ]);
?>

<div class="draft-document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?php //echo $form->field($model, 'tempFile')->fileInput() ?>

    <?= $form->field($model, 'contract_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Pilih Kontrak','disabled' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php /*echo $form->field($model, 'draft_status_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\DraftStatus::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Pilih Status Draf','disabled' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);*/ ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?php /* echo $form->field($model, 'status')->textInput(['placeholder' => 'Status'])*/ ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('DraftParam'),
            'content' => $this->render('_formDraftParam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->draftParams),
            ]),
        ],
        // [
        //     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('DraftStatus'),
        //     'content' => $this->render('_formDraftStatus', [
        //         'row' => \yii\helpers\ArrayHelper::toArray($model->draftStatuses),
        //     ]),
        // ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Kemaskini', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
