<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->getDraftStatuses()->with('status0','createdBy')->orderBy('draft_status.id')->asArray()->all(),
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'status0.name',
            'label' => 'Status'
        ],
        [
            'attribute' => 'created_at',
            'label' => 'Masa'
        ],
        [
            'attribute' => 'createdBy.name',
            'label' => 'Oleh'
        ],
        [
            'attribute' => 'remark',
            'label' => 'Catatan'
        ],
        // 'created_at',
        // 'createdBy.name',
        // 'remark',
        [
            'class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
            'controller' => 'draft-status',

        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
