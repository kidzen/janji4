<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DraftDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Draf Perjanjian';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="draft-document-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?php if(Yii::$app->user->isAdmin): ?>
            <?= Html::a('Tambah Draft Document', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
        <?= Html::a('Carian', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
                'attribute' => 'contract_id',
                'label' => 'Kontrak',
                'value' => function($model){
                    if ($model->contract)
                    {return $model->contract->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Kontrak', 'id' => 'grid-draft-document-search-contract_id']
            ],
        [
                'attribute' => 'draft_status_id',
                'label' => 'Status Draf',
                'value' => function($model){
                    if ($model->draftStatus)
                    {return $model->draftStatus->status0->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftStatus::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Status Draf', 'id' => 'grid-draft-document-search-draft_status_id']
            ],
        'description',
        'remark',
        //'status',
        [
            'class' => 'kartik\grid\ActionColumn',
            'template'=>'{regenerate} {download} {view} {update} {delete}',
            'visibleButtons' => [
                // 'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
            'buttons'=>[
                'download' => function ($url, $model) {
                    if ($model['file']) {
                        return Html::a('<span class="glyphicon glyphicon-download" style="color:green;"></span>', $model['file'], ['title' => Yii::t('yii', 'Muat turun draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']);
    //                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
                    }
                },
                'regenerate' => function ($url, $model) {
                    if ($model['draftParams'] && $model->allowAlter()) {
                        return Html::a('<span class="glyphicon glyphicon-repeat" style="color:green;"></span>', ['request/generate-draft', 'id' => $model['id']], ['title' => Yii::t('yii', 'Jana semula draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,]);
    //                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
                    }
                },

            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Muat Turun Maklumat</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
