<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draf Perjanjian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-document-view">

    <h2><?= 'Draf Perjanjian'.' '. Html::encode($this->title) ?></h2>
    <div class="row">


        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?= 'Draf Perjanjian'.' '. Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <p>
                        <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Anda pasti?',
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </p>
                    <?php
                        $gridColumn = [
                            ['attribute' => 'id', 'visible' => false],
                            'name',
                            'file',
                            [
                                'attribute' => 'contract.name',
                                'label' => 'Kontrak',
                            ],
                            [
                                'attribute' => 'draftStatus.status0.name',
                                'label' => 'Status Draf',
                            ],
                            'description',
                            'remark',
                            // //'status',
                        ];
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => $gridColumn
                        ]);
                    ?>
                </div>
            </div>
        </div>
    <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Contract<?= ' '. Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
                    <?php
                    $gridColumnContract = [
                        ['attribute' => 'id', 'visible' => false],
                        'contract_no',
                        'name',
                        'awarded_date',
                        'juu_staff_assigned',
                        'department_staff_assigned',
                        'template.name',
                        'remark',
                        // //'status',
                    ];
                    echo DetailView::widget([
                        'model' => $model->contract,
                        'attributes' => $gridColumnContract    ]);
                    ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
<?php
if($providerDraftParam->totalCount){
    $gridColumnDraftParam = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'paramList.param.name',
                'label' => 'Perkara'
            ],
            [
                'attribute' => 'param',
                'label' => 'Perkara'
            ],
            'remark',
            // //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftParam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-param']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Parameter Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftParam
    ]);
}
?>

    </div>

    <div class="col-sm-12">
<?php
if($providerDraftStatus->totalCount){
    $gridColumnDraftStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'status0.name',
                'label' => 'Status'
            ],
            'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Status Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftStatus
    ]);
}
?>

    </div>
</div>
</div>
