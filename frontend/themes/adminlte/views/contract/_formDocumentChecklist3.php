<?php
use common\widgets\ChecklistForm;
// use kartik\builder\TabularForm;
// use yii\data\ArrayDataProvider;
// use yii\helpers\Html;
// use yii\widgets\Pjax;

?>


<div class="form-group" id="add-document-checklist">
    <div class="box box-primary">
        <div class="box box-header">
            <h2 class="box-title"> Senarai Semak</h2>
        </div>
        <div class="box-body">
            <?=
            ChecklistForm::widget([
                'type' => ChecklistForm::TYPE_FORM,
            ]);
            ?>

        </div>
    </div>
</div>

