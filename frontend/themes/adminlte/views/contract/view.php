<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use common\widgets\BoxGridView;
/* @var $this yii\web\View */
/* @var $model common\models\Contract */
$collapseTemplate = '
<div class="box-tools pull-right">
<button type="button" class="btn btn-box-tool" data-widget="collapse">
<i class="fa fa-minus"></i>
</button>
</div>
';
$expandTemplate = '
<div class="box-tools pull-right">
<button type="button" class="btn btn-box-tool" data-widget="collapse">
<i class="fa fa-plus"></i>
</button>
</div>
';

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Senarai Draf Dalam Proses', 'url' => ['/request/contract-list']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contract-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'KONTRAK'.' : '. Html::encode($this->title); ?></h2>
        </div>
    </div>
    <div class="box box-info">
    <!-- <div > -->
        <div class="box-header">
            <h3 class="box-title">KONTRAK</h3>
            <div class="pull-right">
                <?php if($model['draftStatus']['name'] == 'Selesai') :?>
                    <a href="#statusDone"><span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="Selesai">Selesai</span></a>
                <?php else: ?>
                    <?php if($model['status'] > Yii::$app->params['settings']['departmentAlterLimit']) :?>
                        <a href="#underJuu"><span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="Tindakan JUU">Tindakan JUU</span></a>
                    <?php else: ?>
                        <a href="#underDepartment"><span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="Tindakan Jabatan">Tindakan Jabatan</span></a>
                    <?php endif;?>

                <?php endif;?>
                <?php if($providerNote->totalCount > 0) :?>
                    <a href="#notes"><span data-toggle="tooltip" title="" class="badge bg-blue" data-original-title="<?= $providerNote->totalCount ?> Nota"><?= $providerNote->totalCount ?> Nota</span></a>
                <?php endif;?>
                <?php if($providerAttachment->totalCount > 0) :?>
                    <a href="#attachment"><span data-toggle="tooltip" title="" class="badge bg-maroon" data-original-title="<?= $providerAttachment->totalCount ?> Lampiran"><?= $providerAttachment->totalCount ?> Lampiran</span></a>
                <?php endif;?>
                <?php if($providerDraftDocument->totalCount > 0) :?>
                    <a href="#document"><span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="<?= $providerDraftDocument->totalCount ?> Draf"><?= $providerDraftDocument->totalCount ?> Draf</span></a>
                <?php endif;?>
                <?php if($providerDocumentChecklist->totalCount > 0) :?>
                    <a href="#document"><span data-toggle="tooltip" title="" class="badge bg-purple" data-original-title="<?= $providerDocumentChecklist->totalCount ?> Senarai Semak"><?= $providerDocumentChecklist->totalCount ?> Senarai Semak</span></a>
                <?php endif;?>
                <?php if($providerDraftStatuses->totalCount > 0) :?>
                    <a href="#document"><span data-toggle="tooltip" title="" class="badge" data-original-title="<?= $providerDocumentChecklist->totalCount ?> Log Status"><?= $providerDraftStatuses->totalCount ?> Log Status</span></a>
                <?php endif;?>
            </div>
        </div>
        <div class="box-body">
            <div class="col-sm-12">
                <!-- button -->
                <!-- can edit -->
                <?php if($model->can('edit')) : ?>
                    <span>
                        <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
                    </span>
                    <?php if($model->can('requireHighLevelBypassAuthority') && (Yii::$app->user->isJuu || Yii::$app->user->isAdmin)) : ?>
                        <span>
                            <?php /*echo*/ Html::a('Senarai Semak', ['/request/update-checklist','id'=>$model['id']], ['class' => 'btn btn-primary','title' => Yii::t('yii', 'Senarai Semak')]); ?>
                        </span>
                    <?php endif; ?>
                    <?php //if(!$model->can('requireHighLevelBypassAuthority') && !(Yii::$app->user->isJuu || Yii::$app->user->isAdmin)) : ?>
                    <?php if($model->can('operate')) : ?>
                        <?php if($model['incomplete']):?>
                            <span>
                                <?= Html::a('Isi Draf',  ['/draft-document/update','id'=>$model['incomplete']], ['class' => 'btn btn-primary','title' => Yii::t('yii', 'Senarai Semak')]); ?>
                            </span>
                        <?php else: ?>
                            <span>
                                <?= Html::a('Isi Draf',  ['/request/create-draft','id'=>$model['id']], ['class' => 'btn btn-primary','title' => Yii::t('yii', 'Senarai Semak')]); ?>
                            </span>
                        <?php endif; ?>
                        <span>
                            <?= Html::a('Senarai Semak', ['/request/update-checklist','id'=>$model['id']], ['class' => 'btn btn-primary','title' => Yii::t('yii', 'Senarai Semak')]); ?>
                        </span>
                    <?php endif; ?>
                    <?php if($model['activeDocument'] && !$model->can('requireHighLevelBypassAuthority') && !(Yii::$app->user->isJuu || Yii::$app->user->isAdmin)) : ?>
                        <span>
                            <?= Html::a('Hantar', ['/request/send','draftId'=>$model['activeDocument']['id']], ['class' => 'btn btn-primary','title' => Yii::t('yii', 'Hantar')]); ?>
                        </span>
                    <?php endif; ?>
                    <?php if($model['activeDocument'] && (Yii::$app->user->isJuu || Yii::$app->user->isAdmin)) : ?>
                        <span>
                            <?= Html::a('Kemaskini Status', ['/request/update-status','id'=>$model['id']], ['class' => 'btn btn-primary','title' => Yii::t('yii', 'Kemaskini Status')]); ?>
                        </span>
                    <?php endif; ?>
                <?php endif; ?>
                <!-- end can edit -->
                <!-- can delete -->
                <?php if($model->can('delete')) : ?>
                    <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Anda pasti?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                <?php endif;?>
                <!-- end can delete -->
                <!-- end button -->

                <!-- box primary model -->
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    'contract_no',
                    'name',
                    'awarded_date',
                    [
                        'attribute' => 'juuStaffAssigned.name',
                        'label' => 'PIC JUU',
                    ],
                    [
                        'attribute' => 'departmentStaffAssigned.name',
                        'label' => 'PIC Jabatan',
                    ],
                    [
                        'attribute' => 'template.name',
                        'label' => 'Templat',
                    ],
                    [
                        'attribute' => 'draftStatus.name',
                        'label' => 'Status',
                    ],

                    'remark',
                    'created_at',
                    'created_by',
                    'updated_at',
                    'updated_by',

                ];
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
                ?>
            </div>
            <!-- box juu model -->
            <?php if(isset($model->juuStaffAssigned)) : ?>
                <div class="col-sm-6">

                    <div class="box box-primary box-solid">
                        <div class="box-header">

                            <?php $dept = isset($model->juuStaffAssigned) ? $model->juuStaffAssigned->department : ''; ?>
                            <h3 class="box-title">PIC <?= $dept ?></h3>
                            <?= $collapseTemplate ?>
                        </div>
                        <div class="box-body">
                            <?php
                            $gridColumnProfile = [
                                ['attribute' => 'id', 'visible' => false],
                                'staff_no',
                                'name',
                                'position',
                                'department',
                            ];
                            ?>
                            <?php echo DetailView::widget([
                                'model' => $model->juuStaffAssigned,
                                'attributes' => $gridColumnProfile
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(isset($model->departmentStaffAssigned)) : ?>
                <div class="col-sm-6">
                    <!-- box dept model -->
                    <div class="box box-primary box-solid">
                        <div class="box-header">
                            <?php $dept = isset($model->departmentStaffAssigned)?$model->departmentStaffAssigned->department : ''; ?>
                            <h3 class="box-title">PIC <?= $dept ?></h3>
                            <?= $collapseTemplate ?>
                        </div>
                        <div class="box-body">
                            <?php
                            $gridColumnProfile = [
                                ['attribute' => 'id', 'visible' => false],
                                'staff_no',
                                'name',
                                'position',
                                'department',
                            ];

                            ?>
                            <?php
                            echo DetailView::widget([
                                'model' => $model->departmentStaffAssigned,
                                'attributes' => $gridColumnProfile
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <div class="clearfix"></div>
            <!-- box draf doc model -->
            <div id="document" class="col-sm-12"">
                <?php
                if($providerDraftDocument->totalCount){
                    $gridColumnDraftDocument = [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['attribute' => 'id', 'visible' => false],
                        'name',
                        'file',
                        'description',
                        [
                            'attribute' => 'draftStatus.status0.name',
                            'label' => 'Status Draf'
                        ],
                        'remark',
                            // //'status',
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerDraftDocument,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('DRAF PERJANJIAN'),
                        ],
                        'toggleData' => false,
                        'columns' => $gridColumnDraftDocument
                    ]);
                }
                ?>
            </div>
            <!-- box atachment model -->
            <div id="attachment" class="col-sm-12">

                <?php
                if($providerAttachment->totalCount){
                    $gridColumnAttachment = [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['attribute' => 'id', 'visible' => false],
                        'name',
                        'file',
                        'remark',
                        // //'status',
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerAttachment,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-attachment']],
                        'panel' => [
                        // 'headingOptions'=>['class'=>'box-header'],
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('LAMPIRAN'),
                        ],
                        'toggleData' => false,
                        'columns' => $gridColumnAttachment
                    ]);
                }
                ?>
            </div>
            <!-- end box atachment model -->
            <!-- box draft template model -->
            <?php //if($model->template) : ?>
            <?php if(false) : ?>
               <div class="col-sm-12">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h4 class="box-title">DRAF PERJANJIAN</h4>
                    </div>
                    <div class="box-body">
                        <?php
                        $gridColumnDraftTemplate = [
                            ['attribute' => 'id', 'visible' => false],
                            'name',
                            'description',
                            'file',
                            'remark',
                            // //'status',
                        ];
                        echo DetailView::widget([
                            'model' => $model->template,
                            'attributes' => $gridColumnDraftTemplate
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <!-- end box draft template model -->
        <!-- box doc checklist model -->
        <?php if($providerDocumentChecklist->totalCount) : ?>
            <div id="checklist2" class="col-sm-12" style="diaplay:none">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h4 class="box-title">SENARAI SEMAK</h4>
                            <?= $collapseTemplate ?>
                    </div>
                    <div class="box-body">
                        <?php if($providerDocumentChecklist->totalCount) : ?>
                        <div id="checklistHeader" class="col-sm-12" style="diaplay:none">
                            <div class="box box-primary box-solid">
                                <div class="box-header">
                                    <h4 class="box-title">SENARAI BERKAITAN</h4>
                                        <?= $collapseTemplate ?>
                                </div>
                                <div class="box-body">
                                    <?php
                                        $gridColumnDocumentChecklist = [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            ['attribute' => 'id', 'visible' => false],
                                            [
                                                'attribute' => 'docCheck.description',
                                                'label' => 'Doc Check',
                                                'value'=> function ($model) {
                                                    $category = $model['docCheck']['category'];
                                                    $description = $model['docCheck']['description'];
                                                    return isset($category) ? $category. ' ' . $description : $description;
                                                }
                                            ],
                                                    // 'file',
                                            'completed',
                                            'remark',
                                                    // //'status',
                                        ];
                                        echo Gridview::widget([
                                            'dataProvider' => $providerDocumentChecklist,
                                            'pjax' => true,
                                            'responsiveWrap' => false,
                                            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-checklist']],
                                            // 'panel' => [
                                            //     'type' => GridView::TYPE_PRIMARY,
                                            //     'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('SENARAI SEMAK'),
                                            // ],
                                            'toggleData' => false,
                                            'columns' => $gridColumnDocumentChecklist
                                        ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div id="checklistHeader" class="col-sm-12" style="diaplay:none">
                            <div class="box box-primary box-solid collapsed-box">
                                <div class="box-header">
                                    <h4 class="box-title">BORANG SENARAI SEMAK BERFORMAT</h4>
                                        <?= $expandTemplate ?>
                                </div>
                                <div class="box-body">
                                    <?= \common\widgets\ChecklistForm::widget([
                                        'type'=> \common\widgets\ChecklistForm::TYPE_LIST,
                                        'data'=> \common\models\Contract::getChecklists($model['id']),
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- end box doc checklist model -->

            <div id="notes" class="col-sm-12">
                <?php
                if($providerNote->totalCount){
                    $gridColumnNote = [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['attribute' => 'id', 'visible' => false],
                        'notes',
                        'remark',
                            // //'status',
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerNote,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-note']],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('NOTA'),
                        ],
                        'toggleData' => false,
                        'columns' => $gridColumnNote
                    ]);
                }
                ?>
            </div>

        <?php if($providerDraftStatuses->totalCount) : ?>
            <div id="draftStatus" class="col-sm-12" style="diaplay:none">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h4 class="box-title">LOG STATUS DRAF</h4>
                            <?= $collapseTemplate ?>
                    </div>
                    <div class="box-body">
                        <?php
                            $gridColumnDraftStatuses = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                ['attribute' => 'status0.name'],
                                ['attribute' => 'created_at'],
                                ['attribute' => 'createdBy.name'],
                                'remark',
                                // [
                                //     'attribute' => 'docCheck.description',
                                //     'label' => 'Doc Check',
                                //     'value'=> function ($model) {
                                //         $category = $model['docCheck']['category'];
                                //         $description = $model['docCheck']['description'];
                                //         return isset($category) ? $category. ' ' . $description : $description;
                                //     }
                                // ],
                                        // 'file',
                                // 'completed',
                                        // //'status',
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerDraftStatuses,
                                'pjax' => true,
                                'responsiveWrap' => false,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-checklist']],
                                // 'panel' => [
                                //     'type' => GridView::TYPE_PRIMARY,
                                //     'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('SENARAI SEMAK'),
                                // ],
                                'toggleData' => false,
                                'columns' => $gridColumnDraftStatuses
                            ]);
                        ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        </div>
        <!-- box primary model -->

    </div>
</div>
