<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use common\widgets\BoxGridView;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */
$collapseTemplate = '
<div class="box-tools pull-right">
<button type="button" class="btn btn-box-tool" data-widget="collapse">
<i class="fa fa-minus"></i>
</button>
</div>
';
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Kontrak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contract-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'KONTRAK'.' : '. Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">KONTRAK</h3>
            <div class="box-tools pull-right">
                <?php if($providerNote->totalCount > 0) :?>
                    <a href="#notes"><span data-toggle="tooltip" title="" class="badge bg-blue" data-original-title="<?= $providerNote->totalCount ?> Nota baru"><?= $providerNote->totalCount ?> Nota</span></a>
                <?php endif;?>
                <?php if($providerAttachment->totalCount > 0) :?>
                    <a href="#attachment"><span data-toggle="tooltip" title="" class="badge bg-maroon" data-original-title="<?= $providerAttachment->totalCount ?> Lampiran baru"><?= $providerAttachment->totalCount ?> Lampiran</span></a>
                <?php endif;?>
                <?php if($providerDraftDocument->totalCount > 0) :?>
                    <a href="#document"><span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="<?= $providerDraftDocument->totalCount ?> Draf baru"><?= $providerDraftDocument->totalCount ?> Draf</span></a>
                <?php endif;?>
                <?php if($providerDocumentChecklist->totalCount > 0) :?>
                    <a href="#document"><span data-toggle="tooltip" title="" class="badge bg-purple" data-original-title="<?= $providerDocumentChecklist->totalCount ?> Senarai semak baru"><?= $providerDocumentChecklist->totalCount ?> Senarai semak</span></a>
                <?php endif;?>
            </div>
        </div>
        <div class="box-body">
            <div class="col-sm-12">
                <!-- button -->
                <?php if($model->can('edit')) : ?>
                    <span>
                        <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
                    </span>
                    <?php if(!$model->can('requireHighLevelBypassAuthority') || Yii::$app->user->isJuu) : ?>
                        <span>
                            <?= Html::a('Senarai Semak', ['/request/update-checklist','id'=>$model['id']], ['class' => 'btn btn-primary','title' => Yii::t('yii', 'Senarai Semak')]); ?>
                        </span>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if($model->can('delete')) : ?>
                    <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Anda pasti?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                <?php endif;?>
                <!-- end button -->

                <!-- box primary model -->
                <?php
                $gridColumn = [
                    ['attribute' => 'id', 'visible' => false],
                    'contract_no',
                    'name',
                    'awarded_date',
                    [
                        'attribute' => 'juuStaffAssigned.name',
                        'label' => 'PIC JUU',
                    ],
                    [
                        'attribute' => 'departmentStaffAssigned.name',
                        'label' => 'PIC Jabatan',
                    ],
                    [
                        'attribute' => 'template.name',
                        'label' => 'Templat',
                    ],
                    [
                        'attribute' => 'draftStatus.name',
                        'label' => 'Status',
                    ],

                    'remark',
                    'created_at',
                    'created_by',
                    'updated_at',
                    'updated_by',

                ];
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => $gridColumn
                ]);
                ?>
            </div>
            <!-- box juu model -->
            <?php if(isset($model->juuStaffAssigned)) : ?>
                <div class="col-sm-6">

                    <div class="box box-primary box-solid">
                        <div class="box-header">

                            <?php $dept = isset($model->juuStaffAssigned) ? $model->juuStaffAssigned->department : ''; ?>
                            <h3 class="box-title">PIC <?= $dept ?></h3>
                            <?= $collapseTemplate ?>
                        </div>
                        <div class="box-body">
                            <?php
                            $gridColumnProfile = [
                                ['attribute' => 'id', 'visible' => false],
                                'staff_no',
                                'name',
                                'position',
                                'department',
                            ];
                            ?>
                            <?php echo DetailView::widget([
                                'model' => $model->juuStaffAssigned,
                                'attributes' => $gridColumnProfile
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(isset($model->departmentStaffAssigned)) : ?>
                <div class="col-sm-6">
                    <!-- box dept model -->
                    <div class="box box-primary box-solid">
                        <div class="box-header">
                            <?php $dept = isset($model->departmentStaffAssigned)?$model->departmentStaffAssigned->department : ''; ?>
                            <h3 class="box-title">PIC <?= $dept ?></h3>
                            <?= $collapseTemplate ?>
                        </div>
                        <div class="box-body">
                            <?php
                            $gridColumnProfile = [
                                ['attribute' => 'id', 'visible' => false],
                                'staff_no',
                                'name',
                                'position',
                                'department',
                            ];

                            ?>
                            <?php
                            echo DetailView::widget([
                                'model' => $model->departmentStaffAssigned,
                                'attributes' => $gridColumnProfile
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <div class="clearfix"></div>
            <!-- box draf doc model -->
            <div id="document" class="col-sm-12"">
                <?php
                if($providerDraftDocument->totalCount){
                    $gridColumnDraftDocument = [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['attribute' => 'id', 'visible' => false],
                        'name',
                        'file',
                        [
                            'attribute' => 'draftStatus.id',
                            'label' => 'Status Draf'
                        ],
                        'description',
                        'remark',
                            // //'status',
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerDraftDocument,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('DRAF PERJANJIAN'),
                        ],
                        'toggleData' => false,
                        'columns' => $gridColumnDraftDocument
                    ]);
                }
                ?>
            </div>
            <!-- box atachment model -->
            <div id="attachment" class="col-sm-12">

                <?php
                if($providerAttachment->totalCount){
                    $gridColumnAttachment = [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['attribute' => 'id', 'visible' => false],
                        'name',
                        'file',
                        'remark',
                        // //'status',
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerAttachment,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-attachment']],
                        'panel' => [
                        // 'headingOptions'=>['class'=>'box-header'],
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('LAMPIRAN'),
                        ],
                        'toggleData' => false,
                        'columns' => $gridColumnAttachment
                    ]);
                }
                ?>
            </div>
            <!-- end box atachment model -->
            <!-- box draft template model -->
            <?php //if($model->template) : ?>
            <?php if(false) : ?>
               <div class="col-sm-12">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                        <h4 class="box-title">DRAF PERJANJIAN</h4>
                    </div>
                    <div class="box-body">
                        <?php
                        $gridColumnDraftTemplate = [
                            ['attribute' => 'id', 'visible' => false],
                            'name',
                            'description',
                            'file',
                            'remark',
                            // //'status',
                        ];
                        echo DetailView::widget([
                            'model' => $model->template,
                            'attributes' => $gridColumnDraftTemplate
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <!-- end box draft template model -->
        <!-- box doc checklist model -->
        <div id=+"checklist" class="col-sm-12">
            <?php
            if($providerDocumentChecklist->totalCount){
                $gridColumnDocumentChecklist = [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    [
                        'attribute' => 'docCheck.description',
                        'label' => 'Doc Check',
                        'value'=> function ($model) {
                            $category = $model['docCheck']['category'];
                            $description = $model['docCheck']['description'];
                            return isset($category) ? $category. ' ' . $description : $description;
                        }
                    ],
                            // 'file',
                    'completed',
                    'remark',
                            // //'status',
                ];
                echo Gridview::widget([
                    'dataProvider' => $providerDocumentChecklist,
                    'pjax' => true,
                    'responsiveWrap' => false,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-checklist']],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('SENARAI SEMAK'),
                    ],
                    'toggleData' => false,
                    'columns' => $gridColumnDocumentChecklist
                ]);
            }
            ?>
        </div>
        <?php if($providerDocumentChecklist->totalCount) : ?>
            <div id="checklist2" class="col-sm-12" style="display: none;"">
                <div class="box box-info">
                    <div class="box-header">
                        <h4>Senarai Semak</h4>
                    </div>
                    <div class="box-body">
                        <?= \common\widgets\ChecklistForm::widget([
                            'type'=> \common\widgets\ChecklistForm::TYPE_LIST,
                            'model'=> $providerDocumentChecklist->models,
                            ]); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- end box doc checklist model -->

            <div id="notes" class="col-sm-12">
                <?php
                if($providerNote->totalCount){
                    $gridColumnNote = [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['attribute' => 'id', 'visible' => false],
                        'notes',
                        'remark',
                            // //'status',
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerNote,
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-note']],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('NOTA'),
                        ],
                        'toggleData' => false,
                        'columns' => $gridColumnNote
                    ]);
                }
                ?>
            </div>
        </div>
        <!-- box primary model -->

    </div>
</div>
