<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;


$this->title = 'Kontrak';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="contract-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Carian', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <?php
    $gridColumnSimplified = [
        ['class' => 'yii\grid\SerialColumn'],
        'contract_no',
        'name',
        'awarded_date',
        [
                'attribute' => 'juu_staff_assigned',
                'value' => function($model){
                    if ($model->juuStaffAssigned)
                    {return $model->juuStaffAssigned->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Profile::find()->asArray()->all(), 'staff_no', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Profile', 'id' => 'grid-contract-search-juu_staff_assigned']
            ],
        [
                'attribute' => 'department_staff_assigned',
                // 'label' => 'PIC Jabatan',
                'value' => function($model){
                    if ($model->departmentStaffAssigned)
                    {return $model->departmentStaffAssigned->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Profile::find()->asArray()->all(), 'staff_no', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Profile', 'id' => 'grid-contract-search-department_staff_assigned']
            ],
        [
                'attribute' => 'template_id',
                // 'label' => 'Templat',
                'value' => function($model){
                    if ($model->template)
                    {return $model->template->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftTemplate::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Draft template', 'id' => 'grid-contract-search-template_id']
            ],
        [
            'class' => 'yii\grid\ActionColumn',
            'visible' => false,
        ],
    ];
    // unset($gridColumn[8]);
    // var_dump($gridColumn);die;

    ?>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'contract_no',
        'name',
        'awarded_date',
        [
            'attribute' => 'juu_staff_assigned',
            'label' => 'PIC JUU',
            'value' => function($model){
                if ($model->juuStaffAssigned)
                    {return $model->juuStaffAssigned->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Profile::find()->asArray()->all(), 'staff_no', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Profile', 'id' => 'grid-contract-search-juu_staff_assigned']
        ],
        [
            'attribute' => 'department_staff_assigned',
            'label' => 'PIC Jabatan',
            'value' => function($model){
                if ($model->departmentStaffAssigned)
                    {return $model->departmentStaffAssigned->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Profile::find()->asArray()->all(), 'staff_no', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Profile', 'id' => 'grid-contract-search-department_staff_assigned']
        ],
        [
            'attribute' => 'template_id',
            'label' => 'Templat',
            'value' => function($model){
                if ($model->template)
                    {return $model->template->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftTemplate::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Draft template', 'id' => 'grid-contract-search-template_id']
        ],
        [
            'attribute' => 'draftStatus.name',
            'label' => 'Status',
        ],
        'remark',
        [
            'class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                // 'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => ($simplified)? $gridColumnSimplified:$gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contract']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => ($simplified)? $gridColumnSimplified:$gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Muat Turun Maklumat</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
        ]); ?>

    </div>
