<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->documentChecklists,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'docCheck.name',
                'label' => 'Doc Check'
            ],
        'file',
        'completed',
        'remark',
        //'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
            'controller' => 'document-checklist'
        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
