<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */

?>
<div class="contract-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'contract_no',
        'name',
        'awarded_date',
        [
            'attribute' => 'juuStaffAssigned.name',
            'label' => 'PIC JUU',
        ],
        [
            'attribute' => 'departmentStaffAssigned.name',
            'label' => 'PIC Jabatan',
        ],
        [
            'attribute' => 'template.name',
            'label' => 'Templat',
        ],
        [
            'attribute' => 'draftStatus.name',
            'label' => 'Status',
        ],
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
