<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kontrak'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
    //     [
    //     'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Lampiran'),
    //     'content' => $this->render('_dataAttachment', [
    //         'model' => $model,
    //         'row' => $model->attachments,
    //     ]),
    // ],
                        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Senarai Semak'),
        'content' => $this->render('_dataDocumentChecklist', [
            'model' => $model,
            'row' => $model->documentChecklists,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Draf Perjanjian'),
        'content' => $this->render('_dataDraftDocument', [
            'model' => $model,
            'row' => $model->draftDocuments,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Note'),
        'content' => $this->render('_dataNote', [
            'model' => $model,
            'row' => $model->notes,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
