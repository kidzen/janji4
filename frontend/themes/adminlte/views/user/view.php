<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div class="row">

        <div class="col-sm-12">
            <div class="col-sm-9">
                <h2><?= Html::encode($model->username0->name) ?></h2>
            </div>
            <div class="col-sm-3" style="margin-top: 15px">

                <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Anda pasti?',
                        'method' => 'post',
                    ],
                ])
                ?>
            </div>
        </div>

        <div class="col-sm-12">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                // 'username0.name',
                'credential.staff_no',
                ['attribute' => 'role.name', 'label' => 'Akses'],
                'username0.position',
                'username0.department',
                'email:email',
                [
                    'attribute' => 'role.name',
                    'label' => 'Akses',
                ],
                'profile_pic',
                'auth_key',
                'credential.password',
                // 'remark',
                // //'status',
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]);
            ?>
        </div>
    </div>
</div>
