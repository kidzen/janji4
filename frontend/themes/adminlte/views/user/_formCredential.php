<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Credential */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?= $form->field($Credential, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($Credential, 'password')->passwordInput(['maxlength' => true, 'placeholder' => 'Password']) ?>

</div>
