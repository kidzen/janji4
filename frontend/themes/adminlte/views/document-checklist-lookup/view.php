<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DocumentChecklistLookup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Document Checklist Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-checklist-lookup-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Document Checklist Lookup'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'category',
        'contract_id',
        'name',
        'file',
        'description',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerDocumentChecklist->totalCount){
    $gridColumnDocumentChecklist = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'contract.name',
                'label' => 'Kontrak'
            ],
            'file',
            'completed',
            'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDocumentChecklist,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-checklist']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Senarai Semak'),
        ],
        'export' => false,
        'columns' => $gridColumnDocumentChecklist
    ]);
}
?>

    </div>
</div>
