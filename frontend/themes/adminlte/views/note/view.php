<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Note */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Note', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="note-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Note'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'contract.name',
            'label' => 'Kontrak',
        ],
        'notes',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Contract<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnContract = [
        ['attribute' => 'id', 'visible' => false],
        'contract_no',
        'name',
        'awarded_date',
        'juu_staff_assigned',
        'department_staff_assigned',
        'template_id',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->contract,
        'attributes' => $gridColumnContract    ]);
    ?>
</div>
