<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParamListLookup */

$this->title = 'Tambah Param List Lookup';
$this->params['breadcrumbs'][] = ['label' => 'Param List Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-list-lookup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
