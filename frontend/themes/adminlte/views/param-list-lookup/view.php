<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ParamListLookup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Param List Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-list-lookup-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Param List Lookup'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'param',
        'hint',
        'description',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerParamList->totalCount){
    $gridColumnParamList = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'template.name',
                'label' => 'Templat'
            ],
                        'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerParamList,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-param-list']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Param List'),
        ],
        'export' => false,
        'columns' => $gridColumnParamList
    ]);
}
?>

    </div>
</div>
