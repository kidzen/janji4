<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DraftStatus */

$this->title = 'Kemaskini Draft Status: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Status Draf', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Kemaskini';
?>
<div class="draft-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
