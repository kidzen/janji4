<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftStatus */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Status Draf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-status-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Status Draf'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'status0.name',
            'label' => 'Status',
        ],
        [
            'attribute' => 'draft.name',
            'label' => 'Draft',
        ],
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerDraftDocument->totalCount){
    $gridColumnDraftDocument = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
            'file',
            [
                'attribute' => 'contract.name',
                'label' => 'Kontrak'
            ],
                        'description',
            'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftDocument,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Draf Perjanjian'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftDocument
    ]);
}
?>

    </div>
    <div class="row">
        <h4>DraftStatusLookup<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnDraftStatusLookup = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->status0,
        'attributes' => $gridColumnDraftStatusLookup    ]);
    ?>
    <div class="row">
        <h4>DraftDocument<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnDraftDocument = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        'contract_id',
        'draft_status_id',
        'description',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->draft,
        'attributes' => $gridColumnDraftDocument    ]);
    ?>
</div>
