<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ParamList */

$this->title = 'Kemaskini Param List: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Param List', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Kemaskini';
?>
<div class="param-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
