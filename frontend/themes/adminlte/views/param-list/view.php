<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ParamList */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Param List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-list-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Param List'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'template.name',
            'label' => 'Templat',
        ],
        [
            'attribute' => 'param.name',
            'label' => 'Param',
        ],
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerDraftParam->totalCount){
    $gridColumnDraftParam = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'draftDocument.name',
                'label' => 'Draf Perjanjian'
            ],
                        'param',
            'remark',
            //'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftParam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-param']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Parameter Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftParam
    ]);
}
?>

    </div>
    <div class="row">
        <h4>DraftTemplate<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnDraftTemplate = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'file',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->template,
        'attributes' => $gridColumnDraftTemplate    ]);
    ?>
    <div class="row">
        <h4>ParamListLookup<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnParamListLookup = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'param',
        'hint',
        'description',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->param,
        'attributes' => $gridColumnParamListLookup    ]);
    ?>
</div>
