<?php

use common\widgets\ChecklistForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div>
  <h2>Senarai Semak</h2>
  <div class="box box-info">
    <div class="box-header">
      <div class="box-header">
        <h2 class="box-title">Senarai Semak</h2>
      </div>
      <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
        <div class="col-md-6 col-md-offset-3">

          <div class="col-md-6">
            <?= $form->field($model, 'contract_no')->textInput(['maxlength' => true, 'placeholder' => 'No Kontrak','disabled'=>true]) ?>
          </div>
          <div class="col-md-6">
            <?= $form->field($model, 'awarded_date')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh SST','disabled'=>true]) ?>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Tajuk Kontrak','disabled'=>true]) ?>
          </div>
          <div class="col-md-12">
            <?= $form->field($model->juuStaffAssigned, 'name')->textInput(['maxlength' => true, 'placeholder' => 'PIC JUU','disabled'=>true])->label('PIC JUU') ?>
          </div>
          <div class="col-md-12">
            <?= $form->field($model->departmentStaffAssigned, 'name')->textInput(['maxlength' => true, 'placeholder' => 'PIC Jabatan','disabled'=>true])->label('PIC Jabatan') ?>
          </div>
          <div class="col-md-12">
            <?= $form->field($model->template, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Templat','disabled'=>true])->label('Templat') ?>
          </div>
        </div>

        <?= ChecklistForm::widget([
          'data' => $data,
          'type' => ChecklistForm::TYPE_FORM
          ]) ?>
        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>

