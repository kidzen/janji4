<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Draf Perjanjian'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kontrak'),
        'content' => $this->render('_detailContract', [
            'model' => $model->contract,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Parameter Draf'),
        'content' => $this->render('_dataDraftParam', [
            'model' => $model,
            'row' => $model->draftParams,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Senarai Semak'),
        'active' => true,
        'content' => $this->render('_dataDocumentChecklist', [
            'model' => $model->contract,
            'row' => $model->contract->documentChecklists,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Status Draf'),
        'content' => $this->render('_dataDraftStatus', [
            'model' => $model,
            'row' => $model->draftStatuses,
        ]),
    ],
    // [
    //     'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Lampiran'),
    //     'content' => $this->render('_dataAttachment', [
    //         'model' => $model->contract,
    //         'row' => $model->contract->attachments,
    //     ]),
    // ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Draf Perjanjian'),
        'content' => $this->render('_dataDraftDocument', [
            'model' => $model->contract,
            'row' => $model->contract->draftDocuments,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Note'),
        'content' => $this->render('_dataNote', [
            'model' => $model->contract,
            'row' => $model->contract->notes,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
