<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->documentChecklists,
        'key' => 'id'
    ]);
    $gridColumns = [
        [
            'class' => 'yii\grid\SerialColumn',
            // 'width' => '100px',
                'headerOptions' => ['style' => 'width:20px'],
        ],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'docCheck.category',
                // 'label' => 'Doc Check',
                // 'groupedRow' => true,
                'group' => true,
        ],
        [
                'attribute' => 'docCheck.description',
                'label' => 'Doc Check',
                'contentOptions' => ['style' => 'width:70%; white-space: normal;'],
                // 'groupedRow' => true,
                // 'group' => true,
                // 'subgroup' => 2,
        ],
        [
                'attribute' => 'completed',
                'label' => 'Doc Check',
                'value' => function($model) {
                    switch ($model['completed']) {
                        case '2':
                            return 'Ada';
                            break;

                        case '1':
                            return 'Tiada';
                            break;

                        case '0':
                            return 'Tidak Berkenaan';
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            ],
        // 'file',
        // 'completed',
        // 'remark',
        [
            'class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
            'controller' => 'document-checklist'
        ],
    ];
    echo Html::tag('p',Html::a('Kemaskini', ['/template-generator/update-checklist','id'=>$model['id']], ['class' => 'btn btn-success']));
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        // 'containerOptions' => ['style' => 'overflow: auto'],
        // 'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
