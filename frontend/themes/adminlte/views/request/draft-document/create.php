<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */

$this->title = 'Tambah Draft Document';
$this->params['breadcrumbs'][] = ['label' => 'Draf Perjanjian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
