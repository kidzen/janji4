<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DraftDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Draf Perjanjian';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);

?>
<div class="draft-document-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Draft Document', ['create'], ['class' => 'btn btn-success']) ?>
        <!-- <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['draft-document/reject-status','id'=>1], ['class' => 'btn btn-success','title' => Yii::t('yii', 'Dipulang ke jabatan'), 'data-toggle' => 'tooltip']); ?> -->
        <?= Html::a('Carian', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'contract_id',
                'label' => 'Kontrak',
                'value' => function($model){
                    if ($model->contract)
                    {return $model->contract->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Kontrak', 'id' => 'grid-draft-document-search-contract_id']
            ],
        'name',
        // 'file',
        [
                'attribute' => 'draft_status_id',
                'label' => 'Status Draf',
                'value' => function($model){
                    if ($model->draftStatus)
                    {return $model->draftStatus->status0->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftStatus::find()->with('status0')->asArray()->all(), 'status0.name', 'status0.name'),
                // 'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftStatusLookup::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Status Draf', 'id' => 'grid-draft-document-search-draft_status_id']
            ],
        'description',
        'remark',
        [
            'class' => 'kartik\grid\ActionColumn',;
            'template' => '{approve-status} {reject-status} {generate-draft} {download-draft} {view} {update} {delete}',
            'buttons' => [
                'approve-status'=>function($url,$model){
                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', ['/draft-document/approve-status','id'=>$model['id']], ['title' => Yii::t('yii', 'Hantar'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                    // return Html::a('Hantar',['/template-generator/update-status','id'=>$model->id]);
                },
                'reject-status'=>function($url,$model){
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', ['/draft-document/reject-status','id'=>$model['id']], ['title' => Yii::t('yii', 'Dipulang ke jabatan'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                    // return Html::a('Hantar',['/template-generator/update-status','id'=>$model->id]);
                },
                'generate-draft'=>function($url,$model){
                    // if (!$model['file']) {
                        // return Html::a('Bina Draf',['/template-generator/generate-draft','id'=>$model->id]);
                        return Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span>', ['/template-generator/generate-draft','id'=>$model['id']], ['title' => Yii::t('yii', 'Bina Draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']);
                    // }
                },
                'download-draft'=>function($url,$model){
                    // if (!$model['file']) {
                        // return Html::a('Bina Draf',['/template-generator/generate-draft','id'=>$model->id]);
                        return Html::a('<span class="glyphicon glyphicon-download"></span>', ['/template-generator/download-document','id'=>$model['id']], ['title' => Yii::t('yii', 'Muat Turun Draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']);
                    // }
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        // 'pjax' => true,
        // 'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Muat Turun Maklumat</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
