<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Draf Perjanjian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-document-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draf Perjanjian'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
            'attribute' => 'contract.name',
            'label' => 'Kontrak',
        ],
        [
            'attribute' => 'draftStatus.status0.name',
            'label' => 'Status Draf',
        ],
        'description',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Contract<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnContract = [
        ['attribute' => 'id', 'visible' => false],
        'contract_no',
        'name',
        'awarded_date',
        'juu_staff_assigned',
        'department_staff_assigned',
        'template_id',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model->contract,
        'attributes' => $gridColumnContract    ]);
    ?>
    <div class="row">
        <h4>DraftStatus<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnDraftStatus = [
        ['attribute' => 'id', 'visible' => false],
        'status_id',
        'draft_id',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model->draftStatus,
        'attributes' => $gridColumnDraftStatus    ]);
    ?>

    <div class="row">
<?php
if($providerDraftParam->totalCount){
    $gridColumnDraftParam = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'paramList.id',
                'label' => 'Param List'
            ],
            'param',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftParam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-param']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Parameter Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftParam
    ]);
}
?>

    </div>

    <div class="row">
<?php
if($providerDraftStatus->totalCount){
    $gridColumnDraftStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'status0.name',
                'label' => 'Status'
            ],
                        'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Status Draf'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftStatus
    ]);
}
?>

    </div>
</div>
