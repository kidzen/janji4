<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->draftStatuses,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'status0.name',
                'label' => 'Status'
            ],
        'remark',
        'created_at',
        'createdBy.staffNo.name',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'draft-status',
            'visibleButtons' => [
                'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],

        ],
    ];
    // var_dump($model->attributes);die;
    // switch ($model) {
    //     case 'value':
    //         # code...
    //         break;

    //     default:
    //         # code...
    //         break;
    // }
    echo Html::tag('p',Html::a('Hantar', ['/template-generator/update-checklist','id'=>$model['id']], ['class' => 'btn btn-success']));
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        // 'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
