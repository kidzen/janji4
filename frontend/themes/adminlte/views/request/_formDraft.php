<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Template Generator';
?>

<div class="draft-document-form">
<h1>Jana Draf</h1>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Perincian Kontrak : <?= $contract->name ?></h4>
        </div>
        <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
            <div class="row">

                <?= $form->errorSummary($model); ?>
                <div class="col-md-4">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Nama','autofocus' => true])->label('Nama Draf Perjanjian')->hint('Ruangan ini diperlukan untuk memudahkan carian dokumen draf.') ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($contract, 'name')->textInput([
                        // 'value' =>
                        'disabled'=>true,
                        ])->label('Kontrak') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($contract, 'awarded_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => 'Pilih Tarikh SST',
                                'autoclose' => true,
                            ]
                        ],
                    ]); ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-3">
                    <?= $form->field($contract, 'department_staff_assigned')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \common\models\Profile::deptList(),
                        'options' => ['placeholder' => 'Pilih Pengguna'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'disabled' => !(Yii::$app->user->isAdmin || Yii::$app->user->isJuu)
                        ],
                    ]); ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($model, 'description')->textArea(['maxlength' => true, 'placeholder' => 'Keterangan']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'remark')->textArea(['maxlength' => true, 'placeholder' => 'Catatan']) ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4>Perincian Draf : <?= $contract->template->name?></h4>
                </div>
                <div class="panel-body">
                    <p>** Sila muat turun <?= Html::a('Templat Draf Perjanjian',$contract->template->file) ?> sebagai panduan mengisi draf</p>
                <?php
                    foreach ($paramList as $key => $value) {
                        echo '<div class="col-md-4">';
                        echo $form->field($dynaModel, $value)->textInput(['maxlength' => true])->hint($value);
                        echo '</div>';
                    }
                ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Kemaskini', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
