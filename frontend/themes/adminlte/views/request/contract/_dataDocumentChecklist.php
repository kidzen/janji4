<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->documentChecklists,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'docCheck.description',
            'label' => 'Doc Check',
            'format' => 'raw',
            'value' => function($model) {
                // var_dump($model);die;
                $category = $model['docCheck']['category'];
                $description = $model['docCheck']['description'];
                $result = isset($category)? $category.' :<br>'. $description: $description;
                return $result;
            }
        ],
        'file',
        [
            'attribute' => 'completed',
            'label' => 'Doc Check',
            'format' => 'raw',
            'value' => function($model) {
                switch ($model->completed) {
                    case 0:
                        return 'TIDAK BERKENAAN';
                        break;

                    case 1:
                        return 'TIADA';
                        break;

                    case 2:
                        return 'ADA';
                        break;

                    default:
                        return 'TIDAK BERKENAAN';
                        break;
                }
            }
        ],
        'remark',
        [
            'class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
            'controller' => 'document-checklist'
        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
