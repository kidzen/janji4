<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->draftDocuments,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        [
                'attribute' => 'draftStatus.status0.name',
                'label' => 'Status Draf'
            ],
        'description',
        'remark',
        [
            'class' => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
            'controller' => 'draft-document'
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template'=>'{regenerate} {download} {view} {update} {delete}',
            'visibleButtons' => [
                // 'view'=>Yii::$app->user->isAdmin,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
            ],
            'buttons'=>[
                'download' => function ($url, $model) {
                    if ($model['file']) {
                        return Html::a('<span class="glyphicon glyphicon-download" style="color:green;"></span>', $model['file'], ['title' => Yii::t('yii', 'Muat turun draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']);
                    }
                },
                'regenerate' => function ($url, $model) {
                    if ($model['draftParams'] && $model->allowAlter()) {
                        return Html::a('<span class="glyphicon glyphicon-repeat" style="color:green;"></span>', ['request/generate-draft', 'id' => $model['id']], ['title' => Yii::t('yii', 'Jana semula draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,]);
                    }
                },

            ],
        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
