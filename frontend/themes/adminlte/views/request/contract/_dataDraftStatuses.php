<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->draftStatuses,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'status0.name',
        // [
        //     'attribute' => 'draftStatus.status0.name',
        //     'label' => 'Status Draf'
        // ],
        // 'description',
        'remark',
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'visibleButtons' => [
        //         'view'=>Yii::$app->user->isAdmin,
        //         'update'=>Yii::$app->user->isAdmin,
        //         'delete'=>Yii::$app->user->isAdmin,
        //     ],
        //     'controller' => 'draft-status'
        // ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
