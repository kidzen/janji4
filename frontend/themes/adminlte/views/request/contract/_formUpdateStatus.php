<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>


    <?= $form->field($model, 'draft_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'contract_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'status_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\DraftStatusLookup::find()->asArray()->all(),'id','name'),
        'options' => ['placeholder' => 'Pilih Status'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'juu_staff_assigned')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \common\models\Profile::juulist(),
        'options' => ['placeholder' => 'Pilih Pengguna'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


    <?= $form->field($model, 'note')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <div class="form-group">
        <?= Html::submitButton('Kemaskini', ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
