<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Kontrak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Kontrak'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'contract_no',
        'name',
        'awarded_date',
        [
            'attribute' => 'juuStaffAssigned.username',
            'label' => 'PIC JUU',
        ],
        [
            'attribute' => 'departmentStaffAssigned.username',
            'label' => 'PIC Jabatan',
        ],
        [
            'attribute' => 'template.name',
            'label' => 'Templat',
        ],
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerAttachment->totalCount){
    $gridColumnAttachment = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'name',
            'file',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAttachment,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-attachment']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Lampiran'),
        ],
        'export' => false,
        'columns' => $gridColumnAttachment
    ]);
}
?>

    </div>
    <div class="row">
        <h4>DraftTemplate<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnDraftTemplate = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'file',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model->template,
        'attributes' => $gridColumnDraftTemplate    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'staff_no',
        'auth_key',
        'profile_pic',
        'role_id',
        'password_hash',
        'password_reset_token',
        'email',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model->juuStaffAssigned,
        'attributes' => $gridColumnUser    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'staff_no',
        'auth_key',
        'profile_pic',
        'role_id',
        'password_hash',
        'password_reset_token',
        'email',
        'remark',
    ];
    echo DetailView::widget([
        'model' => $model->departmentStaffAssigned,
        'attributes' => $gridColumnUser    ]);
    ?>

    <div class="row">
<?php
if($providerDocumentChecklist->totalCount){
    $gridColumnDocumentChecklist = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'docCheck.name',
                'label' => 'Doc Check'
            ],
                        'file',
            'completed',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDocumentChecklist,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-document-checklist']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Senarai Semak'),
        ],
        'export' => false,
        'columns' => $gridColumnDocumentChecklist
    ]);
}
?>

    </div>

    <div class="row">
<?php
if($providerDraftDocument->totalCount){
    $gridColumnDraftDocument = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
            'file',
                        [
                'attribute' => 'draftStatus.status0.name',
                'label' => 'Status Draf'
            ],
            'description',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDraftDocument,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-draft-document']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Draf Perjanjian'),
        ],
        'export' => false,
        'columns' => $gridColumnDraftDocument
    ]);
}
?>

    </div>

    <div class="row">
<?php
if($providerNote->totalCount){
    $gridColumnNote = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'notes',
            'remark',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerNote,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-note']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Note'),
        ],
        'export' => false,
        'columns' => $gridColumnNote
    ]);
}
?>

    </div>
</div>
