<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */
/* @var $form yii\widgets\ActiveForm */

// \mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
//     'viewParams' => [
//         'class' => 'Attachment',
//         'relID' => 'attachment',
//         'value' => \yii\helpers\Json::encode($model->attachments),
//         'isNewRecord' => ($model->isNewRecord) ? 1 : 0
//     ]
// ]);
// \mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
//     'viewParams' => [
//         'class' => 'DocumentChecklist',
//         'relID' => 'document-checklist',
//         'value' => \yii\helpers\Json::encode($model->documentChecklists),
//         'isNewRecord' => ($model->isNewRecord) ? 1 : 0
//     ]
// ]);
// \mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
//     'viewParams' => [
//         'class' => 'DraftDocument',
//         'relID' => 'draft-document',
//         'value' => \yii\helpers\Json::encode($model->draftDocuments),
//         'isNewRecord' => ($model->isNewRecord) ? 1 : 0
//     ]
// ]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Note',
        'relID' => 'note',
        'value' => \yii\helpers\Json::encode($model->notes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'contract_no')->textInput(['maxlength' => true, 'placeholder' => 'No Kontrak']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?php if(Yii::$app->db->driverName == 'oci') :?>
        <?= $form->field($model, 'awarded_date')->widget(\kartik\datecontrol\DateControl::classname(), [
            'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
            // 'saveFormat' => 'php:d-M-y',
            // 'ajaxConversion' => true,
            'options' => [
                'type'=>2,
                'pluginOptions' => [
                    'placeholder' => 'Pilih Tarikh SST',
                    'todayHighlight' => true,
                    // 'todayBtn' => true,
                    'autoclose' => true,
                ]
            ],
        ]); ?>
    <?php else :?>
        <?= $form->field($model, 'awarded_date')->widget(\kartik\date\DatePicker::classname(), [
            // 'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
            // 'saveFormat' => 'php:Y-m-d',
            // 'ajaxConversion' => true,
            // 'value' => date('d-M-Y'),
            'options' => [
                'pluginOptions' => [
                    'placeholder' => 'Pilih Tarikh SST',
                    'format' => 'dd-M-yyyy',
                    'todayHighlight' => true,
                    'autoclose' => true,
                ]
            ],
        ]); ?>
    <?php endif;?>
    <?php if(Yii::$app->user->isAdmin) : ?>
        <?= $form->field($model, 'juu_staff_assigned')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \common\models\Profile::juulist(),
            'options' => ['placeholder' => 'Pilih Pengguna'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'department_staff_assigned')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \common\models\Profile::deptList(),
            'options' => ['placeholder' => 'Pilih Pengguna'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    <?php endif; ?>

    <?= $form->field($model, 'template_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\DraftTemplate::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Pilih Draft template'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'remark')->textInput(['maxlength' => true, 'placeholder' => 'Remark']) ?>

    <?php
    $forms = [
        // [
        //     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Lampiran'),
        //     'content' => $this->render('_formAttachment', [
        //         'row' => \yii\helpers\ArrayHelper::toArray($model->attachments),
        //     ]),
        // ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Senarai Semak'),
            'content' => $this->render('_formDocumentChecklist', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->documentChecklists),
            ]),
        ],
        // [
        //     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Draf Perjanjian'),
        //     'content' => $this->render('_formDraftDocument', [
        //         'row' => \yii\helpers\ArrayHelper::toArray($model->draftDocuments),
        //     ]),
        // ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Note'),
            'content' => $this->render('_formNote', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->notes),
            ]),
        ],
    ];
    // echo kartik\tabs\TabsX::widget([
    //     'items' => $forms,
    //     'position' => kartik\tabs\TabsX::POS_ABOVE,
    //     'encodeLabels' => false,
    //     'pluginOptions' => [
    //         'bordered' => true,
    //         'sideways' => true,
    //         'enableCache' => false,
    //     ],
    // ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Kemaskini', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
