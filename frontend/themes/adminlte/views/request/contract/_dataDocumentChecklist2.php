<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use common\components\helper\Helper;
use yii\helpers\Html;

?>
<!-- <div class="col-md-9"> -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Borang Senarai Semak Perjanjian</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive">
        <div class="col-md-3 col-md-offset-1">
            <?= Html::label('JABATAN :','department',['class'=>'control-label']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::textInput('jabatan','BAHAGIAN TEKNOLOGI MAKLUMAT',['class'=>'form-control','disabled'=>true,]) ?>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-3 col-md-offset-1">
            <?= Html::label('PEGAWAI YANG DIHUBUNGI (ext) :','referer',['class'=>'control-label']) ?>
            <!-- <label class="control-label" for="contract-contract_no">PEGAWAI YANG DIHUBUNGI (ext) :</label> -->
        </div>
        <div class="col-md-6">
            <?= Html::textInput('referer','RAJA MASHITAH BINTI RAJA ZAINAL (442)',['class'=>'form-control','disabled'=>true,]) ?>
            <!-- <input type="text" id="contract-contract_no" class="form-control" name="Contract[contract_no]" maxlength="255" disabled value="RAJA MASHITAH BINTI RAJA ZAINAL (442)" placeholder="Contract No" aria-required="true" aria-invalid="false"> -->
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-3 col-md-offset-1">
            <?= Html::label('NO. KONTRAK :','contract_no',['class'=>'control-label']) ?>
            <!-- <label class="control-label" for="contract-contract_no">NO. KONTRAK :</label> -->
        </div>
        <div class="col-md-6">
            <?= Html::textInput('contract_no','A-098780911',['class'=>'form-control','disabled'=>true,'data-key'=>1,'id'=>'contract_no']) ?>
            <!-- <input type="text" id="contract-contract_no" class="form-control" name="Contract[contract_no]" maxlength="255" disabled value="test" placeholder="Contract No" aria-required="true" aria-invalid="false"> -->
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-3 col-md-offset-1">
            <label class="control-label" for="contract-contract_no">TAJUK / NAMA KONTRAK :</label>
        </div>
        <div class="col-md-6">
            <?= Html::textArea('contract_title','PEMBANGUNAN SISTEM KLINIK PANAEL BAGI JABATAN KHIDMAT PENGURUSAN MAJLIS PERBANDARAN SEBERANG PERAI',['class'=>'form-control','disabled'=>true,]) ?>
            <!-- <input type="text-area" id="contract-contract_no" class="form-control" name="Contract[contract_no]" maxlength="255" disabled value="PEMBANGUNAN SISTEM KLINIK PANAEL BAGI JABATAN KHIDMAT PENGURUSAN MAJLIS PERBANDARAN SEBERANG PERAI" placeholder="Contract No" aria-required="true" aria-invalid="false"> -->
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-3 col-md-offset-1">
            <label class="control-label" for="contract-contract_no">NAMA KONTRAKTOR :</label>
        </div>
        <div class="col-md-6">
            <?= Html::textInput('contractor_name','IT CHENTA ENTERPRISE',['class'=>'form-control','disabled'=>true,]) ?>
            <!-- <input type="text" id="contract-contract_no" class="form-control" name="Contract[contract_no]" maxlength="255" disabled value="IT CHENTA ENTERPRISE" placeholder="Contract No" aria-required="true" aria-invalid="false"> -->
        </div>
        <br>
        <br>
        <div id="checklist" class="col-md-5">
            <table class="table table-bordered table-condensed table-hover small kv-table">
            <!-- <table > -->
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th colspan="2">Perkara</th>
                  <th style="width: 40px">Ada</th>
                  <th style="width: 40px">Tiada</th>
                  <th style="width: 40px">Tidak Berkenaan</th>
                </tr>
              </thead>
              <tbody>


                <?php /* sample 1  ?>
                <?php //foreach($checklistArray2 as $key => $value) : ?>
                <?php foreach($array as $key => $value) : ?>

                <?php if(isset($value['items'])) : ?>
                    <tr>
                      <td rowspan="<?= sizeof($value['items'])+1 ?>"><?= $key+1 ?></td>
                      <td colspan="5" class="kv-grouped-row"><?= $value['category'] ?></td>
                    </tr>
                <?php foreach($value['items'] as $row => $item) : ?>
                    <tr>
                      <td style="width: 10px"><?= Helper::RomanConverter($row+1) ?></td>
                      <td><?= $item['description'] ?></td>
                      <td><input type="radio" name="checklist[<?= $item['id'] ?>]" value="1"></td>
                      <td><input type="radio" name="checklist[<?= $item['id'] ?>]" value="1"></td>
                      <td><input type="radio" name="checklist[<?= $item['id'] ?>]" value="1"></td>
                    </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                  <td><?= $key+1 ?></td>
                  <td colspan="2"><?= $value['description'] ?></td>
                  <td><input type="radio" name="checklist[<?= $value['id'] ?>]" value="1"></td>
                  <td><input type="radio" name="checklist[<?= $value['id'] ?>]" value="1"></td>
                  <td><input type="radio" name="checklist[<?= $value['id'] ?>]" value="1"></td>
                </tr>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php  end sample 1 */ ?>

                <?php /* sample 2 */  ?>
                <?php //foreach($checklistArray2 as $key => $value) : ?>
                <?php //var_dump($array); ?>
                <?php foreach($array as $key => $value) : ?>

                <?php if(sizeof($value) > 1) :  ?>
                    <tr>
                      <td rowspan="<?= sizeof($value)+1 ?>"><?= $key ?></td>
                      <td colspan="5"><?= $value[0]['category'] ?></td>
                      <!-- <td colspan="5" class="kv-grouped-row"><?= $value[0]['category'] ?></td> -->
                    </tr>
                    <?php foreach($value as $row => $item) : ?>
                        <tr data-key="<?= $item['id'] ?>">
                          <td style="width: 10px"><?= Helper::RomanConverter($row+1) ?></td>
                          <td><?= $item['description'] ?></td>
                          <td><input type="radio" name="checklist[<?= $item['id'] ?>]" value="2"></td>
                          <td><input type="radio" name="checklist[<?= $item['id'] ?>]" value="1"></td>
                          <td><input type="radio" name="checklist[<?= $item['id'] ?>]" checked value="0"></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>

                <tr data-key="<?= $value[0]['id'] ?>">
                  <td><?= $key ?></td>
                  <td colspan="2"><?= $value[0]['description'] ?></td>
                  <td><input type="radio" name="checklist[<?= $value[0]['id'] ?>]" value="2"></td>
                  <td><input type="radio" name="checklist[<?= $value[0]['id'] ?>]" value="1"></td>
                  <td><input type="radio" name="checklist[<?= $value[0]['id'] ?>]" checked value="0"></td>
                </tr>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php  /* end sample 2 */ ?>

              </tbody>
            </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
      *Nota
      <?= Html::button('Hantar', ['id' => 'submitChecklist','class'=>'pull-right']) ?>
      </div>
    </div>
<!-- </div> -->


<?php
// $url =  Yii::$app->controller->module->requestedRoute;
$url =  'contract/index2';
// var_dump($url);die;
$script = <<<JS

$('#submitChecklist').on('click', function() {

    var grid = $('#checklist');
    var selected = [];

    grid.find("input[type='radio']:checked").each(function () {
        selected.push({
            contract_id:$("input[name='contract_no']").data('key'),
            key:$(this).parent().closest('tr').data('key'),
            value:$(this).attr('value')
        });
    });
    $.post("index.php?r=$url", {doc_checklist:selected});
    $.pjax({container: '#kv-pjax-container-contract'});

    // $.ajax({
    //   url:'index.php?r=/contract/index',
    //   type:'POST',
    //   dataType:'json',
    //   data:{array:JSON.stringify(selected)}
    // });

    // var stringified = JSON.stringify(selected);
    // var parsed = JSON.parse(stringified);

    console.log(selected);
    // console.log(stringified);
    // console.log(parsed);

    return true;

////////////////
/*
    var grid = $('#checklist');
    var keys = [];
    var keys2 = [];

    grid.find("input[type='radio']:checked").each(function () {
        keys.push($(this).parent().closest('tr').data('key'));
        keys2.push($(this).attr('value'));
    });

    console.log(keys);
    console.log(keys2);
*/
});


JS;
$this->registerJs($script);
