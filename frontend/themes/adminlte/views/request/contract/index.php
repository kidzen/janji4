<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Senarai Draf Dalam Proses';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="contract-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('contract/_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'assigned_department',
            'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isJuu,
            'group'=>true,  // enable grouping
            'groupedRow'=>true,
            // 'label' => 'Tarikh Janaan',
        ],
        'contract_no',
        'name',
        'awarded_date',
        [
            'attribute' => 'juu_staff_assigned',
            'label' => 'PIC JUU',
            'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isJuu,
            'value' => function($model){
                if ($model->juuStaffAssigned)
                    {return $model->juuStaffAssigned->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Profile::find()->asArray()->all(), 'staff_no','name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-contract-search-juu_staff_assigned']
        ],
        [
            'attribute' => 'department_staff_assigned',
            'label' => 'PIC Jabatan',
            'value' => function($model){
                if ($model->departmentStaffAssigned)
                    {return $model->departmentStaffAssigned->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Profile::find()->asArray()->all(), 'staff_no','name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-contract-search-department_staff_assigned']
        ],
        // [
        //     'attribute' => 'template_id',
        //     'label' => 'Templat',
        //     'value' => function($model){
        //         if ($model->template)
        //             {return $model->template->name;}
        //         else
        //             {return NULL;}
        //     },
        //     'filterType' => GridView::FILTER_SELECT2,
        //     'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftTemplate::find()->asArray()->all(), 'id', 'name'),
        //     'filterWidgetOptions' => [
        //         'pluginOptions' => ['allowClear' => true],
        //     ],
        //     'filterInputOptions' => ['placeholder' => 'Draft template', 'id' => 'grid-contract-search-template_id']
        // ],
        [
            'attribute' => 'status',
            'label' => 'Status',
            'value' => function($model){
                if ($model->draftStatus)
                    {return $model->draftStatus->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\DraftStatusLookup::find()->asArray()->all(), 'id','name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-contract-search-draft_status']
        ],
        // 'remark',
        [
            'attribute' => 'created_date',
            'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isJuu,
            // 'label' => 'Tarikh Janaan',
        ],
        [
            'attribute' => 'last_update',
            'label' => 'Dikemaskini Pada',
            // 'visible' => Yii::$app->user->isAdmin,
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '<span style="font-size:1.3em"> {send} {update-checklist} {create-draft} {update-status} {view} {regenerate} {download} </span>',
            'visibleButtons' => [
                'view'=>true,
                'update'=>Yii::$app->user->isAdmin,
                'delete'=>Yii::$app->user->isAdmin,
                'update-checklist'=>!(Yii::$app->user->isJuu || Yii::$app->user->isAdmin),
                'update-status'=>Yii::$app->user->isJuu || Yii::$app->user->isAdmin,
                'send'=>!(Yii::$app->user->isJuu || Yii::$app->user->isAdmin),
                'create-draft'=>!(Yii::$app->user->isJuu || Yii::$app->user->isAdmin) ,
            ],
            'buttons' => [
                'view' => function($url,$model){
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/contract/view','id'=>$model['id']], ['title' => Yii::t('yii', 'View'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                },
                'update-checklist' => function($url,$model){
                    if(!$model->can('requireHighLevelBypassAuthority') || Yii::$app->user->isJuu){
                        // if(!$model['documentChecklists']&& !$model->can('requireHighLevelBypassAuthority')){
                        return Html::a('<span class="fa fa-file"></span>', ['update-checklist','id'=>$model['id']], ['title' => Yii::t('yii', 'Senarai Semak'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                        // } else {
                        //     return Html::a('<span class="fa fa-file"></span>', ['/contract/update','id'=>$model['id']], ['title' => Yii::t('yii', 'Senarai Semak'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                        // }
                    }
                },
                'create-draft' => function($url,$model){
                    if(!$model->can('requireHighLevelBypassAuthority') || Yii::$app->user->isJuu){
                        if($model['incomplete']) {
                            return Html::a('<span class="fa fa-paste"></span>', ['/draft-document/update','id'=>$model['incomplete']], ['title' => Yii::t('yii', 'Isi Draf'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                        } else {
                            return Html::a('<span class="fa fa-paste"></span>', ['/request/create-draft','id'=>$model['id'],'ext'=>'docx'], ['title' => Yii::t('yii', 'Isi Draf'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                        }
                    }
                },
                'send' => function($url,$model) {
                    if($model['activeDocument'] && !$model->can('requireHighLevelBypassAuthority')) {
                        return Html::a('<span class="glyphicon glyphicon-export"></span>', ['/request/send','draftId'=>$model['activeDocument']['id']], ['title' => Yii::t('yii', 'Hantar'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                    }
                },
                'update-status' => function($url,$model){
                    if($model['activeDocument']){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/request/update-status','id'=>$model['id']], ['title' => Yii::t('yii', 'Kemaskini Status'), 'data-toggle' => 'tooltip','data-pjax'=>0]);
                    }
                },
                'download' => function ($url, $model) {
                    if ($model['activeDocument']['file']) {
                        return Html::a('<span class="glyphicon glyphicon-download" style="color:green;"></span>', $model['activeDocument']['file'], ['title' => Yii::t('yii', 'Muat turun draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']);
                    }
                },
                'regenerate' => function ($url, $model) {
                    if ($model['activeDocument']['draftParams'] && $model['activeDocument']->allowAlter()) {
                        return Html::a('<span class="glyphicon glyphicon-repeat" style="color:green;"></span>', ['request/generate-draft', 'id' => $model['activeDocument']['id']], ['title' => Yii::t('yii', 'Jana semula draf'), 'data-toggle' => 'tooltip','data-pjax'=>0,]);
                    }
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'rowOptions' => function($model){
            if($model['awarded_date']){
                $sst = \Yii::$app->formatter->asDate($model['awarded_date'], "php:d-m-Y");
                $now = \Yii::$app->formatter->asDate(time(), "php:d-m-Y");
                $diff = date_diff(date_create($sst),date_create($now));
                if($diff->days >= Yii::$app->params['settings']['lateDraftWarningInDays']){
                    return ['class' => 'danger'];
                }
            }
            switch (true) {
                case $model['status'] == \common\models\DraftStatusLookup::max():
                return ['class' => 'success'];
                break;
                case Yii::$app->user->isJuu && $model->can('requireHighLevelBypassAuthority'):
                return ['class' => 'warning'];
                break;
                case Yii::$app->user->isDepartment && !$model->can('requireHighLevelBypassAuthority'):
                return ['class' => 'warning'];
                break;
                case $model['activeDocument']:
                return ['class' => 'info'];
                break;

                default:
                return '';
                break;
            }
        },
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-contract']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            Html::a('Carian', '#', ['class' => 'btn btn-info search-button']),
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Muat Turun Maklumat</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
        ]); ?>

    </div>
