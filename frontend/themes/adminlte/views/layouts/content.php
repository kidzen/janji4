<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use yii\helpers\Html;
// use common\widgets\Alert;
$this->title = '';
?>
<div class="content-wrapper <?= (Yii::$app->controller->getRoute() == 'site/index')?'img-layout':'' ?>">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
        <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
        <h1>
            <?php
            if ($this->title !== null) {
                echo \yii\helpers\Html::encode($this->title);
            } else {
                echo \yii\helpers\Inflector::camel2words(
                    \yii\helpers\Inflector::id2camel($this->context->module->id)
                );
                echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
            } ?>
        </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            [
                'homeLink' => ['label' => Yii::t('yii', 'Home'),'url' => Yii::$app->homeUrl],
                // 'homeLink' => <li><a href="/janji3/frontend/web/index.php">Home</a></li>,
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
            ) ?>
        </section>

        <section class="content">
            <?php
            $delay = 0;
            foreach (Yii::$app->session->getAllFlashes() as $message):;
                echo \kartik\widgets\Growl::widget([
                    'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                    'title' => (!empty($message['title'])) ? \yii\helpers\Html::encode($message['title']) : null,
                    'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                    'body' => (!empty($message['message'])) ? $message['message'] : 'Message Not Set!',
                    'showSeparator' => true,
                'delay' => $delay, //This delay is how long before the message shows
                'pluginOptions' => [
                    'showProgressbar' => true,
                    'delay' => (!empty($message['duration'])) ? $message['duration'] + $delay : 3000, //This delay is how long the message shows for
                    'placement' => [
                        'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                        'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                    ]
                ]
            ]);
                $delay = $delay + 1000;
            endforeach;
            ?>
            <?php //echo Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong>HAK CIPTA TERPERLIHARA &copy; <?= date('Y')?> <a href="http://www.sarraglobal.com">Majlis Perbandaran Seberang Perai</a>.</strong><span class="large-screen">
        Best View : Mozilla Firefox , Chrome ,Safari .Moderate View :Internet Explorer 9 and above. Best Resolution: 1280 x 900</span>
    </footer>

