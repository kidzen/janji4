<?php
use yii\helpers\Url;
// $data = Yii::$app->db->getSchema()->getTableNames();
// $data = Yii::$app->request->hostInfo;
// var_dump($data);
// die();

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <img src="<?= Yii::getAlias('@web').Url::to('/images/MPSP.png') ?>" style="width: 100%" alt="Mohor_rasmi_Majlis_Perbandaran_Seberang_Perai"/>
                <!--<img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" style="width: 200px" alt="TUDM"/>-->
            </div>
        </div>


        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
    -->        <!-- /.search form -->

    <?= dmstr\widgets\Menu::widget(
        [
            'options' => ['class' => 'sidebar-menu','data-widget' => 'tree'], //for version 2.5
            // 'options' => ['class' => 'sidebar-menu'],
            'items' => [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                [
                    'label' => 'Log Masuk', 'icon' => 'share', 'url' => ['/site/login'],
                    'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => 'Test',
                    'visible' => Yii::$app->user->isAdmin && false,
                    'icon' => 'calendar-check-o', 'url' => ['/request/generate-draft']
                ],
                [
                    'label' => 'Permohonan', 'icon' => 'stack-overflow',
                    'visible' => !Yii::$app->user->isGuest,
                    'items' => [
                        ['label' => 'Daftar kontrak', 'icon' => 'calendar-check-o', 'url' => ['/request/create-contract'],],
                        ['label' => 'Draf Perjanjian', 'icon' => 'calendar-check-o', 'url' => ['/request/contract-list'],'visible' => !(Yii::$app->user->isGuest),],
                        ['label' => 'Muat Naik Template', 'icon' => 'balance-scale', 'url' => ['/draft-template/create'],'visible' => !(Yii::$app->user->isUserDepartment || Yii::$app->user->isAdminDepartment),],
                    ]
                ],
                [
                    'label' => 'Rekod', 'icon' => 'stack-overflow',
                    'visible' => !Yii::$app->user->isGuest,
                    'items' => [
                        ['label' => 'Draf Dokumen', 'icon' => 'calendar-check-o', 'url' => ['/draft-document/index']],
                        ['label' => 'Kontrak', 'icon' => 'balance-scale', 'url' => ['/contract/index']],
                        ['label' => 'Senarai Template', 'icon' => 'users', 'url' => ['/draft-template/index'],],
                        // ['label' => 'Senarai Semak Kontrak', 'icon' => 'balance-scale', 'url' => ['/document-checklist/index']],
                    ]
                ],
                [
                    'label' => 'Konfigurasi Sistem', 'icon' => 'dashboard',
                        // 'visible'=>false,
                    'visible' => Yii::$app->user->isAdmin,
                    'items' => [
                        ['label' => 'Senarai Semak', 'icon' => 'balance-scale', 'url' => ['/document-checklist-lookup/index']],
                        ['label' => 'Senarai Status Draf', 'icon' => 'balance-scale', 'url' => ['/draft-status-lookup/index']],
                        ['label' => 'Setting', 'icon' => 'balance-scale', 'url' => ['/settings/index']],
                    ],
                ],
                [
                    'label' => 'Pentadbiran', 'icon' => 'dashboard',
                        // 'visible'=>false,
                    'visible' => Yii::$app->user->isUserDepartment || Yii::$app->user->isAdminDepartment,
                    'items' => [
                        ['label' => 'Pengguna', 'icon' => 'users', 'url' => ['/user/view','id'=>Yii::$app->user->id],],
                    ]
                ],
                [
                    'label' => 'Pentadbiran', 'icon' => 'dashboard',
                        // 'visible'=>false,
                    'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isAdminJuu,
                    'items' => [
                        ['label' => 'Profil', 'icon' => 'users', 'url' => ['/user/view','id'=>Yii::$app->user->id],],
                        ['label' => 'Pengguna', 'icon' => 'user', 'url' => ['/profile/index']],
                        ['label' => 'Pengguna Sistem', 'icon' => 'users', 'url' => ['/user/index'],],
                        ['label' => 'Akses', 'icon' => 'lock', 'url' => ['/role/index'],'visible' => Yii::$app->user->isAdmin],
                    ]
                ],
                [
                    'visible'=>false,
                    'label' => 'Dev tools',
                    'icon' => 'share',
                        // 'url' => '#',
                    'items' => [
                        [
                            'label' => 'Database',
                            'icon' => 'database',
                            'items' => [
                                ['label' => 'User', 'icon' => 'user', 'url' => ['/user/index']],
                                ['label' => 'Profile', 'icon' => 'user', 'url' => ['/profile/index']],
                                ['label' => 'Akses', 'icon' => 'user', 'url' => ['/role/index']],
                                ['label' => 'Kontrak', 'icon' => 'user', 'url' => ['/contract/index']],
                                ['label' => 'Templat Draf', 'icon' => 'user', 'url' => ['/draft-template/index']],
                                ['label' => 'Contract Template Assignment', 'icon' => 'user', 'url' => ['/contract-template-assignment/index']],
                                ['label' => 'Senarai Pemboleh Ubah', 'icon' => 'user', 'url' => ['/param-list/index']],
                                ['label' => 'Draft Param', 'icon' => 'user', 'url' => ['/draft-param/index']],
                                ['label' => 'Draf Perjanjian', 'icon' => 'user', 'url' => ['/draft-document/index']],
                                ['label' => 'Date Setting', 'icon' => 'user', 'url' => ['/date-setting/index']],
                                ['label' => 'Senarai Semak', 'icon' => 'user', 'url' => ['/document-checklist/index']],
                                ['label' => 'Document Checklist Lookup', 'icon' => 'user', 'url' => ['/document-checklist-lookup/index']],
                                ['label' => 'Status Draf', 'icon' => 'user', 'url' => ['/draft-status/index']],
                                ['label' => 'Note', 'icon' => 'user', 'url' => ['/note/index']],
                                ['label' => 'Approve Level', 'icon' => 'user', 'url' => ['/approve-level/index']],
                                ['label' => 'Approver', 'icon' => 'user', 'url' => ['/approver/index']],
                                ['label' => 'Migration', 'icon' => 'user', 'url' => ['/migration/index']],
                            ],
                        ],
                        ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    ],
                ],
                ['label' => 'Manual Sistem', 'icon' => 'info', 'url' => ['/site/info'],],
            ],
        ]
        ) ?>

    </section>

</aside>
