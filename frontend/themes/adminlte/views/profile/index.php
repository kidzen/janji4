<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--     <p>
        <?= Html::a('Tambah Profil', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Carian', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
-->    <div class="search-form" style="display:none">
    <?=  $this->render('_search', ['model' => $searchModel]); ?>
</div>
<?php
$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'visible' => false],
    'staff_no',
    'name',
    'position',
    'department',
    [
        'attribute' => 'user.role_id',
        'label' => 'Akses',
        'value' => function($model) {
            switch ($model['user']['role_id']) {
             case 1 :
             return 'Super Admin';
             break;
             case 2 :
             return 'Admin Juu';
             break;
             case 3 :
             return 'User Juu';
             break;
             case 4 :
             return 'Admin Jabatan';
             break;
             case 5 :
             return 'User Jabatan';
             break;

             default:
             return 'Tidak Aktif';
             break;
         }
     },
 ],
 [
    'header' => 'Daftar Akses Pengguna',
    'class' => 'yii\grid\ActionColumn',
    'visible' => Yii::$app->user->isAdmin,
    'template' => '{register}',
    'buttons' => [
        'register' => function($url,$model) {
            $separator = ' | ';
            $superAdmin = Html::a('Super Admin',['/request/register-user','staffNo'=>$model['staff_no'],'role'=>1]);
            $adminJuu = Html::a('Admin Juu',['/request/register-user','staffNo'=>$model['staff_no'],'role'=>2]);
            $userJuu = Html::a('User Juu',['/request/register-user','staffNo'=>$model['staff_no'],'role'=>3]);
            $adminDept = Html::a('Admin Jabatan',['/request/register-user','staffNo'=>$model['staff_no'],'role'=>4]);
            $userDept = Html::a('User Jabatan',['/request/register-user','staffNo'=>$model['staff_no'],'role'=>5]);

            switch ($model['user']['role_id']) {
                case 1:
                $role = [$adminJuu,$userJuu,$adminDept,$userDept];
                break;
                case 2:
                $role = [$superAdmin,$userJuu,$adminDept,$userDept];
                break;
                case 3:
                $role = [$superAdmin,$adminJuu,$adminDept,$userDept];
                break;
                case 4:
                $role = [$superAdmin,$adminJuu,$userJuu,$userDept];
                break;
                case 5:
                $role = [$superAdmin,$adminJuu,$userJuu,$adminDept];
                break;

                default:
                $role = [$superAdmin,$adminJuu,$userJuu,$adminDept,$userDept];
                break;
            }
            return implode($separator, $role);
        }
    ],
],
        // ['attribute' => 'id', 'visible' => false],
];
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-profile']],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    'export' => false,
        // your toolbar can include the additional full export menu
    'toolbar' => [
        '{export}',
            // '{toggleData}',
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Muat Turun Maklumat</li>',
                ],
            ],
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false
            ]
        ]) ,
    ],
    ]); ?>

</div>
