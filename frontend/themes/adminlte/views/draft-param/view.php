<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DraftParam */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Draft Param', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-param-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Draft Param'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Kemaskini', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Anda pasti?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'draftDocument.name',
            'label' => 'Draf Perjanjian',
        ],
        [
            'attribute' => 'paramList.id',
            'label' => 'Param List',
        ],
        'param',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>ParamList<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnParamList = [
        ['attribute' => 'id', 'visible' => false],
        'template_id',
        'param_id',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->paramList,
        'attributes' => $gridColumnParamList    ]);
    ?>
    <div class="row">
        <h4>DraftDocument<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php
    $gridColumnDraftDocument = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'file',
        'contract_id',
        'draft_status_id',
        'description',
        'remark',
        //'status',
    ];
    echo DetailView::widget([
        'model' => $model->draftDocument,
        'attributes' => $gridColumnDraftDocument    ]);
    ?>
</div>
