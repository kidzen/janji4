<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DraftParam */

$this->title = 'Kemaskini Draft Param: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Draft Param', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Kemaskini';
?>
<div class="draft-param-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
