<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DraftParam */

$this->title = 'Tambah Draft Param';
$this->params['breadcrumbs'][] = ['label' => 'Draft Param', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-param-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
