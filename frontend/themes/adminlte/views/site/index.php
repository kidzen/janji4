<?php

/* @var $this yii\web\View */

$this->registerCss('
  .intro-text{
    text-transform: capitalize;
    word-spacing: 10px;
    text-align: center;
    font-weight: bold;
    font-family: sans-serif;
    text-shadow: 3px 3px #0f0e0e;
  }


  ');
  ?>
  <div class="site-index">

    <div class="jumbotron">
      <h1 class="intro-text" style="color: lightsteelblue;">Sistem Perjanjian Majlis Perbandaran Seberang Perai</h1>
    </div>

  </div>

