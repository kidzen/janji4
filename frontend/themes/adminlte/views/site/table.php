<?php
  $this->registerCss(".vericaltext{
    width:1px;
    word-wrap: break-word;
    font-family: monospace; /* this is just for good looks */
    white-space:pre-wrap; /* this is for displaying whitespaces including Moz-FF.*/
}");
?>
<table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
  <tr>
    <th></th>
    <th colspan="6">tarikh</th>
  </tr>
  <tr>
    <td rowspan="17"><p class="vericaltext">perlesenan</p></td>
    <td colspan="3">status kes</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
  </tr>
  <tr>
    <td rowspan="9">saman diserahkan</td>
    <td colspan="2">mengaku salah</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="2">jumlah kutipan bayaran denda</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td rowspan="2">waran </td>
    <td>we</td>
    <td><br></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>wne</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="2">tarik balik</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="2">bicara </td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="2">batal</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="2">tangguh</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="2">jumlah kes</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">jumlah kes tidak diserahkan</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">jumlah kes keseluruhan</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">jumlah kes mahkamah</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">jumlah kes mpsp</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">jumlah kutipan mpsp</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">jumlah dibawa kehadapan</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3">jumlah kutipan denda</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
