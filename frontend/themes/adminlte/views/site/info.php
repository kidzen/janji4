<?php
use yii\helpers\Html;
?>


        <!-- Content Header (Page header) -->
        <div class="content-header">
          <h1>
            Sistem Perjanjian Perundangan MPSP
            <small>Version 1.0</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Documentation</li>
          </ol>
        </div>

        <!-- Main content -->
        <div class="content body">

<section id="introduction">
  <h2 class="page-header"><a href="#introduction">Pengenalan</a></h2>
  <p class="lead">
    <b>SPP MPSP</b> ialah sistem pengurusan dan pemangkin pembuatan draf perjanjian.</p>
</section><!-- /#introduction -->


<!-- ============================================================= -->

<section id="download">
  <!-- <h2 class="page-header"><a href="#download">Download</a></h2> -->
  <p class="lead">
    SPP MPSP mempunyai dua Modul Utama.
  </p>
  <div class="row">
    <div class="col-sm-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Modul <i>Super Admin</i></h3>
          <span class="label label-primary pull-right"><i class="fa fa-lock"></i></span>
        </div><!-- /.box-header -->
        <div class="box-body">
          <p>Modul ini adalah untuk kegunaan pengendali sistem menggubal sebarang aktiviti/data/konfigurasi sistem tanapa melalui pengkalan data.</p>
          <?= Html::a('<i class="fa fa-download"></i> Akses Modul <i>Super Admin</i> sekarang',['#'],['class'=>'btn btn-primary']) ?>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    <div class="col-sm-6">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Modul Pengguna</h3>
          <span class="label label-danger pull-right"><i class="fa fa-users"></i></span>
        </div><!-- /.box-header -->
        <div class="box-body">
          <p>Modul ini adalah untuk kegunaan <b>Pengguna Jabatan</b> dan <b>Pentadbir Jabatan Undang-undang.</b></p>
          <?= Html::a('Akses Modul Pengguna sekarang',['#'],['class'=>'btn btn-info']) ?>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  <h3>Carta Alir Modul Sistem</h3>
  <pre class="hierarchy bring-up"><code class="language-bash" data-lang="bash">Carta Alir Modul Sistem

SPP MPSP
├── Modul Super Admin (Akses Super Admin Sahaja)
│   ├── Pengkalan Data
│   ├── Konfigurasi
│
├── Modul Pengguna
    ├── Super Admin
    │   ├── Konfigurasi Sistem
    │   ├── Profil (Semua)
    │   ├── Kontrak
    │   ├── Draf perjanjian
    │   ├── Template Draf
    │
    ├── Admin Jabatan JUU
    │   ├── Profil (Personel)
    │   ├── Kontrak
    │   ├── Draf perjanjian
    │   ├── Template Draf
    │
    ├── Pengguna JUU
    │   ├── Profil (Personel)
    │   ├── Kontrak
    │   ├── Draf perjanjian
    │
    ├── Admin Jabatan
    │   ├── Profil (Personel)
    │   ├── Kontrak
    │   ├── Draf perjanjian
    │
    ├── Pengguna Jabatan
        ├── Profil (Personel)
        ├── Kontrak
        ├── Draf perjanjian

</code></pre>
</section>


<!-- ============================================================= -->

<section id="akses">
  <h2 class="page-header"><a href="#akses">Akses</a></h2>
  <h3>Akses Pengguna Sistem</h3>
  <p class="lead">Akses sistem terbahagi kepada 5 :-</p>
  <ul class="bring-up">
    <li><b><?= Html::a('Super Admin','#') ?></b></li>
    <li><b><?= Html::a('Admin Jabatan Undang-undang (JUU)','#') ?></b></li>
    <li><b><?= Html::a('Pengguna Jabatan Undang-undang (JUU)','#') ?></b></li>
    <li><b><?= Html::a('Admin Jabatan','#') ?></b></li>
    <li><b><?= Html::a('Pengguna Jabatan','#') ?></b></li>
  </ul>
  <p>
    <b>Nota:</b> Akses ini adalah tetap. Sebarang perubahan perlu mendapat nasihat pembangun.
  </p>
</section>

<section id="kod-warna">
  <h3>Kod Warna</h3>
  <p class="lead">Kod Warna digunakan bagi memudahkan notifikasi dan keberkesanan pemerhatian pada status data yang dipaparkan. Kod Warna yang digunakan didalam sistem:</p>
  <div class="box box-solid" style="max-width: 300px;">
    <div class="box-body no-padding">
      <table id="layout-skins-list" class="table table-striped bring-up nth-2-center">
        <thead  class="bg-purple">
          <tr>
            <th style="width: 210px;">Keterangan</th>
            <th>Kod Warna</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="2" class="bg-info">Notifikasi</td>
          </tr>
          <tr>
            <td><b>Status Berjaya</b></td>
            <td><a class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></td>
          </tr>
          <tr>
            <td><b>Status Gagal</b></td>
            <td><a class="btn btn-danger btn-xs"><i class="fa fa-eye"></i></a></td>
          </tr>
          <tr>
            <td colspan="2" class="bg-info">Status Data</td>
          </tr>
          <tr>
            <td><b>Status</b> data sedang diproses di peringkat <b>Jabatan</b></td>
            <td><a class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a></td>
          </tr>
          <tr>
            <td><b>Status</b> data sedang diproses di peringkat <b>Jabatan Undang-undang</b></td>
            <td><a class="btn btn-warning btn-xs"><i class="fa fa-eye"></i></a></td>
          </tr>
          <tr>
            <td><b>Status</b> data telah <b>Selesai</b></td>
            <td><a class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></td>
          </tr>

        </tbody>
      </table>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</section>

<section id="user-log">
	<h3></h3>
  <div class="box box-solid">
    <div class="box-body" style="position: relative;">
      <header class="main-header" style="position: relative;">
        <!-- Logo -->
        <a href="index2.html" class="logo" style="position: relative;">MPSP</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar" role="navigation" style="border: 0;max-height: 50px;">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs">Ahmad Ali Shukri</span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                  	<a>Admin jabatan</a>
                  </li>
                  <li>
                  	<a>Profil</a>
                  </li>
                  <li>
                  	<a>Log keluar</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    </div>
  </div>

</section>
<!-- ============================================================= -->

<section id="components" data-spy="scroll" data-target="#scrollspy-components">
  <h2 class="page-header"><a href="#components">Components</a></h2>
  <div class="callout callout-info lead">
    <h4>Reminder!</h4>
    <p>
      AdminLTE uses all of Bootstrap 3 components. It's a good start to review
      the <a href="http://getbootstrap.com">Bootstrap documentation</a> to get an idea of the various components
      that this documentation <b>does not</b> cover.
    </p>
  </div>
  <div class="callout callout-danger lead">
    <h4>Tip!</h4>
    <p>
      If you go through the example pages and would like to copy a component, right-click on
      the component and choose "inspect element" to get to the HTML quicker than scanning
      the HTML page.
    </p>
  </div>
  <h3 id="component-main-header">Main Header</h3>
  <p class="lead">The main header contains the logo and navbar. Construction of the
    navbar differs slightly from Bootstrap because it has components that Bootstrap doesn't provide.
    The navbar can be constructed in two way. This an example for the normal navbar and next we will provide an example for
    the top nav layout.</p>
  <div class="box box-solid">
    <div class="box-body" style="position: relative;">
      <header class="main-header" style="position: relative;">
        <!-- Logo -->
        <a href="index2.html" class="logo" style="position: relative;">MPSP</a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar" role="navigation" style="border: 0;max-height: 50px;">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs">Ahmad Ali Shukri</span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                  	<a>Admin jabatan</a>
                  </li>
                  <li>
                  	<a>Profil</a>
                  </li>
                  <li>
                  	<a>Log keluar</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    </div>
  </div>

  <h4>Top Nav Layout. Main Header Example.</h4>
  <div class="callout callout-info lead">
    <h4>Reminder!</h4>
    <p>To use this main header instead of the regular one, you must add the <code>layout-top-nav</code> class to the body tag.</p>
  </div>
  <div class="box box-solid">
    <div class="box-body layout-top-nav">
      <span class="eg">Top Nav Example</span>
      <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <a href="../../index2.html" class="navbar-brand"><b>Admin</b>LTE</a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                    <li class="divider"></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>
              </ul>
              <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                </div>
              </form>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>
    </div>
  </div>


  <!-- ===================================================================== -->

  <h3 id="component-sidebar">Sidebar</h3>
  <p class="lead">
    The sidebar used in this page to the left provides an example of what your sidebar should like.
    Construction of a sidebar:
  </p>


  <h3 id="component-control-sidebar">Control Sidebar</h3>
  <p class="lead">Control sidebar is the right side bar. It can be used for many purposes and is extremely easy
    to create. The sidebar ships with two different show/hide styles. The first allows the sidebar to
  slide over the content. The second pushes the content to make space for the sidebar. Either of
  these methods can be set through the <a href="#adminlte-options">Javascript options</a>.</p>
  <p class="lead">The following code should be placed within the <code>.wrapper</code> div. I prefer
  to place it right after the footer.</p>
  <p class="lead">Dark Sidebar Markup</p>

  <p class="lead">Light Sidebar Markup</p>

  <p class="lead">Once you create the sidebar, you will need a toggle button to open/close it.
  By adding the attribute <code>data-toggle="control-sidebar"</code> to any button, it will
  automatically act as the toggle button.</p>

  <p class="lead">Toggle Button Example</p>
  <button class="btn btn-primary" data-toggle="control-sidebar">Toggle Right Sidebar</button><br><br>

  <p class="lead">Sidebar Toggle Markup</p>
  <!-- ===================================================================== -->

  <h3 id="component-info-box">Info Box</h3>
  <p class="lead">Info boxes are used to display statistical snippets. There are two types of info boxes.</p>
  <h4>First Type of Info Boxes</h4>
  <!-- Info Boxes -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Messages</span>
          <span class="info-box-number">1,410</span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Bookmarks</span>
          <span class="info-box-number">410</span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Uploads</span>
          <span class="info-box-number">13,648</span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Likes</span>
          <span class="info-box-number">93,139</span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  <p class="lead">Markup</p>

  <h4>Second Type of Info Boxes</h4>
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Bookmarks</span>
          <span class="info-box-number">41,410</span>
          <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
          <span class="progress-description">
            70% Increase in 30 Days
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Likes</span>
          <span class="info-box-number">41,410</span>
          <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
          <span class="progress-description">
            70% Increase in 30 Days
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Events</span>
          <span class="info-box-number">41,410</span>
          <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
          <span class="progress-description">
            70% Increase in 30 Days
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-red">
        <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Comments</span>
          <span class="info-box-number">41,410</span>
          <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
          <span class="progress-description">
            70% Increase in 30 Days
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  <p class="lead">Markup</p>
  <p class="lead">The only thing you need to change to alternate between these style is change the placement of the bg-* class. For the
    first style apply any bg-* class to the icon itself. For the other style, apply the bg-* class to the info-box div.</p>
  <!-- ===================================================================== -->

  <h3 id="component-box">Box</h3>
  <p class="lead">The box component is the most widely used component through out this template. You can
    use it for anything from displaying charts to just blocks of text. It comes in many different
    styles that we will explore below.</p>
  <h4>Default Box Markup</h4>
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Default Box Example</h3>
      <div class="box-tools pull-right">
        <!-- Buttons, labels, and many other things can be placed here! -->
        <!-- Here is a label for example -->
        <span class="label label-primary">Label</span>
      </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
      The body of the box
    </div><!-- /.box-body -->
    <div class="box-footer">
      The footer of the box
    </div><!-- box-footer -->
  </div><!-- /.box -->
  <h4>Box Variants</h4>
  <p class="lead">You can change the style of the box by adding any of the contextual classes.</p>
  <div class="row">
    <div class="col-md-4">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Default Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Primary Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Info Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Warning Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Success Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Danger Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->

  <h4>Solid Box</h4>
  <p class="lead">Solid Boxes are alternative ways to display boxes.
    They can be created by simply adding the box-solid class to the box component.
    You may also use contextual classes with you solid boxes.</p>
  <div class="row">
    <div class="col-md-4">
      <div class="box box-solid box-default">
        <div class="box-header">
          <h3 class="box-title">Default Solid Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-solid box-primary">
        <div class="box-header">
          <h3 class="box-title">Primary Solid Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-solid box-info">
        <div class="box-header">
          <h3 class="box-title">Info Solid Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4">
      <div class="box box-solid box-warning">
        <div class="box-header">
          <h3 class="box-title">Warning Solid Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-solid box-success">
        <div class="box-header">
          <h3 class="box-title">Success Solid Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-solid box-danger">
        <div class="box-header">
          <h3 class="box-title">Danger Solid Box Example</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
  <h4>Box Tools</h4>
  <p class="lead">Boxes can contain tools to deploy a specific event or provide simple info. The following examples makes use
    of multiple AdminLTE components within the header of the box.</p>
  <p>AdminLTE data-widget attribute provides boxes with the ability to collapse or be removed. The buttons
    are placed in the box-tools which is placed in the box-header.</p>
  <div class="row">
    <div class="col-md-4">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Collapsable</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Removable</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Expandable</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div><!-- /.row -->
  <p>We can also add labels, badges, pagination, tooltips, inputs and many more in the box tools. A few examples:</p>
  <div class="row">
    <div class="col-md-4">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Labels</h3>
          <div class="box-tools pull-right">
            <span class="label label-default">Some Label</span>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Input</h3>
          <div class="box-tools pull-right">
            <div class="has-feedback">
              <input type="text" class="form-control input-sm" placeholder="Search...">
              <span class="glyphicon glyphicon-search form-control-feedback text-muted"></span>
            </div>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-md-4">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Tootips on buttons</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  <p>
    If you inserted a box into the document after <code>app.js</code> was loaded, you have to activate
    the collapse/remove buttons explicitly by calling <code>.activateBox()</code>:
  </p>

  <h4>Loading States</h4>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Loading state</h3>
        </div>
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
        <!-- Loading (remove the following to stop the loading)-->
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- end loading -->
      </div><!-- /.box -->
    </div><!-- /.col -->

    <div class="col-md-6">
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Loading state (.box-solid)</h3>
        </div>
        <div class="box-body">
          The body of the box
        </div><!-- /.box-body -->
        <!-- Loading (remove the following to stop the loading)-->
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- end loading -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  <p class="lead">
    To simulate a loading state, simply place this code before the <code>.box</code> closing tag.
  </p>
  <h3 id="component-direct-chat">Direct Chat</h3>
  <p class="lead">The direct chat widget extends the box component to create a beautiful chat interface. This widget
    consists of a required messages pane and an <b>optional</b> contacts pane. Examples:</p>
  <!-- Direct Chat -->
  <div class="row">
    <div class="col-md-3">
      <!-- DIRECT CHAT PRIMARY -->
      <div class="box box-primary direct-chat direct-chat-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Direct Chat</h3>
          <div class="box-tools pull-right">
            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Is this template really for free? That's unbelievable!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                You better believe it!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->
          </div><!--/.direct-chat-messages-->

          <!-- Contacts are loaded here -->
          <div class="direct-chat-contacts">
            <ul class="contacts-list">
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date pull-right">2/28/2015</small>
                    </span>
                    <span class="contacts-list-msg">How have you been? I was...</span>
                  </div><!-- /.contacts-list-info -->
                </a>
              </li><!-- End Contact Item -->
            </ul><!-- /.contatcts-list -->
          </div><!-- /.direct-chat-pane -->
        </div><!-- /.box-body -->
        <div class="box-footer">
          <form action="#" method="post">
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-primary btn-flat">Send</button>
              </span>
            </div>
          </form>
        </div><!-- /.box-footer-->
      </div><!--/.direct-chat -->
    </div><!-- /.col -->

    <div class="col-md-3">
      <!-- DIRECT CHAT SUCCESS -->
      <div class="box box-success direct-chat direct-chat-success">
        <div class="box-header with-border">
          <h3 class="box-title">Direct Chat</h3>
          <div class="box-tools pull-right">
            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-green">3</span>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Is this template really for free? That's unbelievable!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                You better believe it!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->
          </div><!--/.direct-chat-messages-->

          <!-- Contacts are loaded here -->
          <div class="direct-chat-contacts">
            <ul class="contacts-list">
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date pull-right">2/28/2015</small>
                    </span>
                    <span class="contacts-list-msg">How have you been? I was...</span>
                  </div><!-- /.contacts-list-info -->
                </a>
              </li><!-- End Contact Item -->
            </ul><!-- /.contatcts-list -->
          </div><!-- /.direct-chat-pane -->
        </div><!-- /.box-body -->
        <div class="box-footer">
          <form action="#" method="post">
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-flat">Send</button>
              </span>
            </div>
          </form>
        </div><!-- /.box-footer-->
      </div><!--/.direct-chat -->
    </div><!-- /.col -->

    <div class="col-md-3">
      <!-- DIRECT CHAT WARNING -->
      <div class="box box-warning direct-chat direct-chat-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Direct Chat</h3>
          <div class="box-tools pull-right">
            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Is this template really for free? That's unbelievable!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                You better believe it!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->
          </div><!--/.direct-chat-messages-->

          <!-- Contacts are loaded here -->
          <div class="direct-chat-contacts">
            <ul class="contacts-list">
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date pull-right">2/28/2015</small>
                    </span>
                    <span class="contacts-list-msg">How have you been? I was...</span>
                  </div><!-- /.contacts-list-info -->
                </a>
              </li><!-- End Contact Item -->
            </ul><!-- /.contatcts-list -->
          </div><!-- /.direct-chat-pane -->
        </div><!-- /.box-body -->
        <div class="box-footer">
          <form action="#" method="post">
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-warning btn-flat">Send</button>
              </span>
            </div>
          </form>
        </div><!-- /.box-footer-->
      </div><!--/.direct-chat -->
    </div><!-- /.col -->

    <div class="col-md-3">
      <!-- DIRECT CHAT DANGER -->
      <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Direct Chat</h3>
          <div class="box-tools pull-right">
            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-red">3</span>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Is this template really for free? That's unbelievable!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                You better believe it!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->
          </div><!--/.direct-chat-messages-->

          <!-- Contacts are loaded here -->
          <div class="direct-chat-contacts">
            <ul class="contacts-list">
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date pull-right">2/28/2015</small>
                    </span>
                    <span class="contacts-list-msg">How have you been? I was...</span>
                  </div><!-- /.contacts-list-info -->
                </a>
              </li><!-- End Contact Item -->
            </ul><!-- /.contatcts-list -->
          </div><!-- /.direct-chat-pane -->
        </div><!-- /.box-body -->
        <div class="box-footer">
          <form action="#" method="post">
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-flat">Send</button>
              </span>
            </div>
          </form>
        </div><!-- /.box-footer-->
      </div><!--/.direct-chat -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  <p class="lead">Direct Chat Markup</p>

  <p>Of course you can use direct chat with a solid box by adding the class <code>solid-box</code> to the box. Here are a couple of examples:</p>

  <!-- Direct Chat With Solid Boxes -->
  <div class="row">
    <div class="col-md-6">
      <!-- DIRECT CHAT WARNING -->
      <div class="box box-primary box-solid direct-chat direct-chat-primary">
        <div class="box-header">
          <h3 class="box-title">Direct Chat in a Solid Box</h3>
          <div class="box-tools pull-right">
            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Is this template really for free? That's unbelievable!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                You better believe it!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->
          </div><!--/.direct-chat-messages-->

          <!-- Contacts are loaded here -->
          <div class="direct-chat-contacts">
            <ul class="contacts-list">
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date pull-right">2/28/2015</small>
                    </span>
                    <span class="contacts-list-msg">How have you been? I was...</span>
                  </div><!-- /.contacts-list-info -->
                </a>
              </li><!-- End Contact Item -->
            </ul><!-- /.contatcts-list -->
          </div><!-- /.direct-chat-pane -->
        </div><!-- /.box-body -->
        <div class="box-footer">
          <form action="#" method="post">
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-primary btn-flat">Send</button>
              </span>
            </div>
          </form>
        </div><!-- /.box-footer-->
      </div><!--/.direct-chat -->
    </div><!-- /.col -->

    <div class="col-md-6">
      <!-- DIRECT CHAT DANGER -->
      <div class="box box-info box-solid direct-chat direct-chat-info">
        <div class="box-header">
          <h3 class="box-title">Direct Chat in a Solid Box</h3>
          <div class="box-tools pull-right">
            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-aqua">3</span>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">Alexander Pierce</span>
                <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                Is this template really for free? That's unbelievable!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->

            <!-- Message to the right -->
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Sarah Bullock</span>
                <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
              </div><!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                You better believe it!
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->
          </div><!--/.direct-chat-messages-->

          <!-- Contacts are loaded here -->
          <div class="direct-chat-contacts">
            <ul class="contacts-list">
              <li>
                <a href="#">
                  <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                  <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date pull-right">2/28/2015</small>
                    </span>
                    <span class="contacts-list-msg">How have you been? I was...</span>
                  </div><!-- /.contacts-list-info -->
                </a>
              </li><!-- End Contact Item -->
            </ul><!-- /.contatcts-list -->
          </div><!-- /.direct-chat-pane -->
        </div><!-- /.box-body -->
        <div class="box-footer">
          <form action="#" method="post">
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-info btn-flat">Send</button>
              </span>
            </div>
          </form>
        </div><!-- /.box-footer-->
      </div><!--/.direct-chat -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>


<!-- ============================================================= -->

<section id="plugins">
  <h2 class="page-header"><a href="#plugins">Plugins</a></h2>
  <p class="lead">AdminLTE makes use of the following plugins. For documentation, updates or license information, please visit the provided links.</p>
  <div class="row bring-up">
    <div class="col-sm-3">
      <ul class="list-unstyled">
        <li><h4>Charts</h4></li>
        <li><a href="http://www.chartjs.org/" target="_blank">ChartJS</a></li>
        <li><a href="http://www.flotcharts.org/" target="_blank">Flot</a></li>
        <li><a href="http://morrisjs.github.io/morris.js/" target="_blank">Morris.js</a></li>
        <li><a href="http://omnipotent.net/jquery.sparkline/" target="_blank">Sparkline</a></li>
      </ul>
    </div><!-- /.col -->
    <div class="col-sm-3">
      <ul class="list-unstyled">
        <li><h4>Form Elements</h4></li>
        <li><a href="https://github.com/seiyria/bootstrap-slider/">Bootstrap Slider</a></li>
        <li><a href="http://ionden.com/a/plugins/ion.rangeSlider/en.html" target="_blank">Ion Slider</a></li>
        <li><a href="http://bootstrap-datepicker.readthedocs.org/" target="_blank">Date Picker</a></li>
        <li><a href="http://www.daterangepicker.com/" target="_blank">Date Range Picker</a></li>
        <li><a href="http://mjolnic.com/bootstrap-colorpicker/" target="_blank">Color Picker</a></li>
        <li><a href="https://github.com/jdewit/bootstrap-timepicker/" target="_blank">Time Picker</a></li>
        <li><a href="http://fronteed.com/iCheck/" target="_blank">iCheck</a></li>
        <li><a href="https://github.com/RobinHerbots/jquery.inputmask/" target="_blank">Input Mask</a></li>
      </ul>
    </div><!-- /.col -->
    <div class="col-sm-3">
      <ul class="list-unstyled">
        <li><h4>Editors</h4></li>
        <li><a href="https://github.com/bootstrap-wysiwyg/bootstrap3-wysiwyg/" target="_blank">Bootstrap WYSIHTML5</a></li>
        <li><a href="http://ckeditor.com/" target="_blank">CK Editor</a></li>
      </ul>
    </div><!-- /. col -->
    <div class="col-sm-3">
      <ul class="list-unstyled">
        <li><h4>Other</h4></li>
        <li><a href="https://datatables.net/examples/styling/bootstrap.html" target="_blank">DataTables</a></li>
        <li><a href="http://fullcalendar.io/" target="_blank">Full Calendar</a></li>
        <li><a href="http://jqueryui.com/" target="_blank">jQuery UI</a></li>
        <li><a href="http://anthonyterrien.com/knob/" target="_blank">jQuery Knob</a></li>
        <li><a href="http://jvectormap.com/" target="_blank">jVector Map</a></li>
        <li><a href="http://rocha.la/jQuery-slimScroll/" target="_blank">Slim Scroll</a></li>
        <li><a href="http://github.hubspot.com/pace/docs/welcome/" target="_blank">Pace</a></li>
      </ul>
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>


<!-- ============================================================= -->

<section id="browsers">
  <h2 class="page-header"><a href="#browsers">Browser Support</a></h2>
  <p class="lead">AdminLTE supports the following browsers:</p>
  <ul>
    <li>IE9+</li>
    <li>Firefox (latest)</li>
    <li>Safari (latest)</li>
    <li>Chrome (latest)</li>
    <li>Opera (latest)</li>
  </ul>
  <p><b>Note:</b> IE9 does not support transitions or animations. The template will function properly but it won't use animations/transitions on IE9.</p>
</section>


<!-- ============================================================= -->

<section id="upgrade">
  <h2 class="page-header"><a href="#upgrade">Upgrade Guide</a></h2>
  <p class="lead">To upgrade from version 1.x to the lateset version, follow this guide.</p>
  <h3>New Files</h3>
  <p>Make sure you update all CSS and JS files that are related to AdminLTE. Otherwise, the layout will not
    function properly. Most important files are AdminLTE.css, skins CSS files, and app.js.</p>
  <h3>Layout Changes</h3>
  <ol>
    <li>The .wrapper div must be placed immediately after the body tag rather than after the header</li>
    <li>Change the .header div to .main-header <code>&LT;div class="main-header"></code></li>
    <li>Change the .right-side class to .content-wrapper <code>&LT;div class="content-wrapper"></code></li>
    <li>Change the .left-side class to .main-sidebar <code>&LT;div class="main-sidebar"></code></li>
    <li>In the navbar, change .navbar-right to .navbar-custom-menu <code>&LT;div class="navbar-custom-menu"></code></li>
  </ol>
  <h3>Navbar Custom Dropdown Menus</h3>
  <ol>
    <li>The icons in the notification menu do not need bg-* classes. They should be replaced with contextual text color class such as text-aqua or text-red.</li>
  </ol>
  <h3>Login, Registration and Lockscreen Pages</h3>
  <p>There are major changes to the HTML markup and style to these pages. The best way is to copy the page's code and customize it.</p>
  <p>And you should be set to go!</p>
  <h3>Mailbox</h3>
  <p>Mailbox got an upgrade to include three different views. The views are inbox, read mail, and compose new email. To use these views,
    you should use the provided HTML files in the pages/mailbox folder.</p>
  <p><b class="text-red">Note:</b> the old mailbox layout has been deprecated in favor of the new one and will be removed by the next release.</p>
</section>


<!-- ============================================================= -->

<section id="implementations">
  <h2 class="page-header"><a href="#implementations">Implementations</a></h2>
  <p class="lead">Thanks to many of AdminLTE users, there are multiple implementations of the template
  for easy integration with back-end frameworks. The following are some of them:</p>

  <ul>
    <li><a href="https://github.com/mmdsharifi/AdminLTE-RTL">Persian RTL</a> by <a href="https://github.com/mmdsharifi">Mohammad Sharifi</a></li>
    <li><a href="https://github.com/dmstr/yii2-adminlte-asset" target="_blank">Yii 2</a> by <a href="https://github.com/schmunk42" target="_blank">Tobias Munk</a></li>
    <li><a href="https://github.com/yajra/laravel-admin-template" target="_blank">Laravel</a> by <a href="https://github.com/yajra" target="_blank">Arjay Angeles</a></li>
    <li><a href="https://github.com/acacha/adminlte-laravel" target="_blank">Laravel 5 package</a> by <a href="https://github.com/acacha" target="_blank">Sergi Tur Badenas</a></li>
    <li><a href="https://github.com/jeroennoten/Laravel-AdminLTE" target="_blank">Laravel-AdminLTE</a> by <a href="https://github.com/jeroennoten" target="_blank">Jeroen Noten</a></li>
    <li><a href="https://github.com/avanzu/AdminThemeBundle" target="_blank">Symfony</a> by <a href="https://github.com/avanzu" target="_blank">Marc Bach</a></li>
    <li>Rails gems: <a href="https://github.com/nicolas-besnard/adminlte2-rails" target="_blank">adminlte2-rails</a> by <a href="https://github.com/nicolas-besnard" target="_blank">Nicolas Besnard</a> and <a href="https://github.com/racketlogger/lte-rails" target="_blank">lte-rails</a> (using AdminLTE sources) by <a href="https://github.com/racketlogger" target="_blank">Carlos at RacketLogger</a></li>
  </ul>

  <p><b class="text-red">Note:</b> these implementations are not supported by Almsaeed Studio. However,
    they do provide a good example of how to integrate AdminLTE into different frameworks. For the latest release
    of AdminLTE, please visit our <a href="https://github.com/almasaeed2010/AdminLTE">repository</a> or <a href="https://almsaeedstudio.com">website</a></p>
</section>


<!-- ============================================================= -->

<section id="faq">
  <h2 class="page-header"><a href="#faq">FAQ</a></h2>
  <h3>Can AdminLTE be used with Wordpress?</h3>
  <p class="lead">AdminLTE is an HTML template that can be used for any purpose. However, it is not made to be easily installed on Wordpress. It will require some effort and enough knowledge of the Wordpress script to do so.</p>

  <h3>Is there an integration guide for PHP frameworks such as Yii or Symfony?</h3>
  <p class="lead">Short answer, no. However, there are forks and tutorials around the web that provide info on how to integrate with many different frameworks. There are even versions of AdminLTE that are integrated with jQuery ajax, AngularJS and/or MVC5 ASP .NET.</p>

  <h3>How do I get notified of new AdminLTE versions?</h3>
  <p class="lead">The best option is to subscribe to our mailing list using the <a href="http://almsaeedstudio.com/#subscribe">subscription form on Almsaeed Studio</a>.
    If that's not appealing to you, you may watch the <a href="https://github.com/almasaeed2010/AdminLTE">repository on Github</a> or visit <a href="http://almsaeedstudio.com">Almsaeed Studio</a> every now and then for updates and announcements.</p>
</section>


<!-- ============================================================= -->

<section id="license">
  <h2 class="page-header"><a href="#license">License</a></h2>
  <h3>AdminLTE</h3>
  <p class="lead">
    AdminLTE is an open source project that is licensed under the <a href="http://opensource.org/licenses/MIT">MIT license</a>.
    This allows you to do pretty much anything you want as long as you include
    the copyright in "all copies or substantial portions of the Software."
    Attribution is not required (though very much appreciated).
  </p>
</section>


        </div><!-- /.content -->
