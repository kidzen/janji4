<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    // public $username;
    public $staff_no;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['staff_no', 'trim'],
            ['staff_no', 'required'],
            ['staff_no', 'integer', 'message' => 'Invalid staff no.'],
            // ['staff_no', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Anda telah berdaftar. Sila dapatkan khidmat admin sekiranya anda terlupa kata laluan.'],
            // ['staff_no', 'exist', 'targetClass' => '\common\models\Profile', 'message' => 'Rekod anda tiada dalam simpanan MPSP.'],
            ['staff_no', 'validateStaffNo'],
            ['staff_no', 'string', 'min' => Yii::$app->params['settings']['minPasswordLength'], 'max' => 255],

            // ['username', 'trim'],
            // ['username', 'required'],
            // ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            // ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 5],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function validateStaffNo()
    {
        $profile = \common\models\Profile::findByStaffNo($this->staff_no);
        // var_dump($profile);die;
        $user = \common\models\User::findByUsername($this->staff_no);
        if(!$profile){
            $this->addError('staff_no','Rekod anda tiada dalam simpanan MPSP.');
        }
        if($user){
            $this->addError('staff_no','Anda telah berdaftar. Sila dapatkan khidmat admin sekiranya anda terlupa kata laluan.');
        }
    }
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $profile = \common\models\Profile::findByStaffNo($this->staff_no);
        $user = new User();
        $user->email = $this->email;
        $user->username = $this->staff_no;
        if($profile['department'] === 'JAB UNDANG-UNDANG') {
            $user->role_id = 3;
        } else {
            $user->role_id = 5;
        }
        // $user->staff_no = $this->staff_no;
        // $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }

    public function getClientOptions($model, $attribute)
    {
        $options = parent::getClientOptions($model, $attribute);
        // Modify $options here
        var_dump($options);die;

        return $options;
    }
}
