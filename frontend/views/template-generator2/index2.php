<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TemplateGeneratorForm */
/* @var $form ActiveForm */
?>
<div class="template-generator-index2">


    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Choose Template</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">

                    <?= $form->field($model, 'templateId')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\DraftTemplate::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Choose Draft template'],
                        'pluginOptions' => [
                        'allowClear' => true
                        ],
                        ]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'contractId')->widget(\kartik\widgets\Select2::classname(), [
                            'data' => \yii\helpers\ArrayHelper::map(\common\models\Contract::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                            'options' => ['placeholder' => 'Choose Contract'],
                            'pluginOptions' => [
                            'allowClear' => true
                            ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Contract</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'year') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'month') ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'contractTitle') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'contractNo') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'jobDescription') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'product') ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'contractDurationText') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'contractDuration') ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'contractStartDate') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'contractEndDate') ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'cost') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'bon') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'compensation') ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'rtDuration') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'deliveryDuration') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'warrantyDuration') ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Company</div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-3">
                            <?= $form->field($model, 'companyName') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'companyRegNo') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'companyAddress') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'companyContactNo') ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'companyFaxNo') ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">#1 Staff Detail</div>
                                <div class="panel-body">
                                    <?= $form->field($model, 'contractorName1') ?>
                                    <?= $form->field($model, 'contractorIc1') ?>
                                    <?= $form->field($model, 'contractorPosition1') ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">#2 Staff Detail</div>
                                <div class="panel-body">
                                    <?= $form->field($model, 'contractorName2') ?>
                                    <?= $form->field($model, 'contractorIc2') ?>
                                    <?= $form->field($model, 'contractorPosition2') ?>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Agreement Detail</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'agreementStartDate') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'agreementEndDate') ?>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- template-generator-index2 -->
