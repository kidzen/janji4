<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\DraftDocument */
$this->registerCss('.tab-content {padding: 10px}');
$wizard_config = [
    'id' => 'stepwizard',
    'steps' => [
        1 => [
            'title' => 'Langkah 1 : Vendor',
            'icon' => 'glyphicon glyphicon-cloud-download',
            // 'content' => '<h3>Step 2</h3>This is step 2',
            'content' => '<h2>Daftar Vendor</h2>'.Yii::$app->controller->renderPartial('_formContractor',['model'=>$company]),
            'skippable' => true,
            // 'buttons' => [
            //     'next' => [
            //         'title' => 'Forward',
            //         // 'options' => [
            //         //     'class' => 'disabled'
            //         // ],
            //      ],
            //  ],
        ],
        2 => [
            'title' => 'Langkah 2 : Kontrak',
            'icon' => 'glyphicon glyphicon-cloud-upload',
            // 'content' => '<h3>Step 2</h3>This is step 2',
            'content' => Yii::$app->controller->renderPartial('_formContract',['model'=>$contract]),
            'skippable' => true,
        ],
        3 => [
            'title' => 'Langkah 3 : Janaan Perjanjian',
            'icon' => 'glyphicon glyphicon-transfer',
            // 'content' => '<h3>Step 2</h3>This is step 2',
            'content' => Yii::$app->controller->renderPartial('_form',['model'=>$model]),
        ],
    ],
    'complete_content' => "<h3>You are done!</h3>", // Optional final screen
    'start_step' => 1, // Optional, start with a specific step
];

$this->title = 'Daftar Draf Perjanjian';
$this->params['breadcrumbs'][] = ['label' => 'Draf Perjanjian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-document-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
<?= \drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>


</div>
<?php
$script = <<<JS
  $('.ajax-submit').click(function(){
    alert('hai');
    alert($('#contract-form').serializeArray());
        $.ajax({
                type: 'POST',
                url: '/janji2/frontend/web/index.php?r=template-generator%2Fgenerator',
                data: $('#contractor-form').serializeArray(),
                success: function (data) {
                      alert(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                      ...
                }
        });
       return false;

  });
JS;
$this->registerJs($script);
?>
