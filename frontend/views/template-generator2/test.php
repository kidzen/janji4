<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->registerCss('.tab-content {padding: 10px}');
$wizard_config = [
    'id' => 'stepwizard',
    'steps' => [
        1 => [
            'title' => 'Langkah 1 : Vendor',
            'icon' => 'glyphicon glyphicon-cloud-download',
            // 'content' => '<h3>Step 2</h3>This is step 2',
            'content' => Yii::$app->controller->renderPartial('test1',['model'=>$company]),
            'skippable' => true,
            // 'buttons' => [
            //     'next' => [
            //         'title' => 'Forward',
            //         // 'options' => [
            //         //     'class' => 'disabled'
            //         // ],
            //      ],
            //  ],
        ],
        2 => [
            'title' => 'Langkah 2 : Kontrak',
            'icon' => 'glyphicon glyphicon-cloud-upload',
            // 'content' => '<h3>Step 2</h3>This is step 2',
            'content' => Yii::$app->controller->renderPartial('test2',['model'=>$contract]),
            'skippable' => true,
        ],
        3 => [
            'title' => 'Langkah 3 : Janaan Perjanjian',
            'icon' => 'glyphicon glyphicon-transfer',
            // 'content' => '<h3>Step 2</h3>This is step 2',
            'content' => Yii::$app->controller->renderPartial('test3',['model'=>$model]),
        ],
    ],
    'complete_content' => "<h3>You are done!</h3>", // Optional final screen
    'start_step' => 1, // Optional, start with a specific step
];

?>
    <?php $form = ActiveForm::begin(); ?>

<?= \drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>
    <?php ActiveForm::end(); ?>
