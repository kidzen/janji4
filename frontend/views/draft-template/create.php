<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DraftTemplate */

$this->title = 'Create Draft Template';
$this->params['breadcrumbs'][] = ['label' => 'Draft Template', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="draft-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
