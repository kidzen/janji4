<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->contracts,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'contract_no',
        'name',
        'product',
        'description',
        'bon',
        'bon_text',
        'compensation',
        'compensation_text',
        'insurance',
        'insurance_text',
        'mp_insurance',
        'mp_insurance_text',
        'cost',
        'cost_text',
        'irt_duration',
        'irt_duration_text',
        'irt_duration_type',
        'rt_duration',
        'rt_duration_text',
        'rt_duration_type',
        'contract_duration',
        'contract_duration_text',
        'contract_duration_type',
        'delivery_duration',
        'delivery_duration_text',
        'delivery_duration_type',
        'warranty_duration',
        'warranty_duration_text',
        'warranty_duration_type',
        'deficiency_duration',
        'deficiency_duration_text',
        'deficiency_duration_type',
        'agreement_start_date',
        'agreement_end_date',
        'expected_start_date',
        'expected_end_date',
        'start_date',
        'end_date',
        'remark',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'contract'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
