<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContractorStaff */

$this->title = 'Update Contractor Staff: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Contractor Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contractor-staff-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
