<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApproveLevel */

$this->title = 'Update Approve Level: ' . ' ' . $model->approve_level;
$this->params['breadcrumbs'][] = ['label' => 'Approve Level', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->approve_level, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="approve-level-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
