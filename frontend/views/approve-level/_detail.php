<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ApproveLevel */

?>
<div class="approve-level-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->approve_level) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'approve_level',
        [
            'attribute' => 'draftTemplate.name',
            'label' => 'Draft Template',
        ],
        'remark',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>