<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JtContractMeeting */

$this->title = 'Update Jt Contract Meeting: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jt Contract Meeting', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jt-contract-meeting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
