<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\JtContractMeeting */

$this->title = 'Create Jt Contract Meeting';
$this->params['breadcrumbs'][] = ['label' => 'Jt Contract Meeting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jt-contract-meeting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
