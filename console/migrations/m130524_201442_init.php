<?php

use common\components\Migration;

class m130524_201442_init extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
        // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->customDrop();

        if ($this->db->driverName === 'oci') {

            // $profile = Yii::$app->db->createCommand('create view "profile" as (select rownum as "id",no_pekerja "staff_no", nama "name",jawatan "position",keterangan "department" from estor.mpsp_staff)');
            // $profile = Yii::$app->db->createCommand('create view "profile" as (select rownum as "id",no_pekerja "staff_no", nama "name",jawatan "position",keterangan "department" from paymas.payrol_estor_view)');
            $profile = Yii::$app->db->createCommand('create view "profile" as (select rownum as "id",no_pekerja "staff_no", nama "name",jawatan "position",keterangan "department" from payroll.paymas_estor_view)');
            $profile->execute();
            // $credential = Yii::$app->db->createCommand('create view "credential" as (select rownum as "id",no_pekerja "staff_no", sulit "password" from majlis.pengguna)');
            // $credential->execute();
            $credential = Yii::$app->db->createCommand('create view "credential" as (select rownum as "id",no_pekerja "staff_no", \'admin1\' "password" from payroll.paymas_estor_view)');
            $credential->execute();

        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('{{%profile}}', [
                'id' => $this->customPrimaryKey(),
                'staff_no' => $this->integer()->notNull()->unique(),
                // 'staff_no' => $this->string(20)->notNull()->unique(),
                'name' => $this->string(100)->notNull(),
                'position' => $this->string(100),
                'department' => $this->string(),
                ], $tableOptions);
            $this->createTable('{{%credential}}', [
                'id' => $this->customPrimaryKey(),
                'staff_no' => $this->integer()->notNull()->unique(),
                // 'staff_no' => $this->string(20)->notNull()->unique(),
                'password' => $this->string(100)->notNull(),
                ], $tableOptions);
        }

        $this->createTable('{{%settings}}', [
            'id' => $this->customPrimaryKey(),
            'key' => $this->string()->unique(),
            'value' => $this->string(),
            ], $tableOptions);

        $this->createTable('{{%user}}', [
            'id' => $this->customPrimaryKey(),
            'username' => $this->integer()->notNull()->unique(),
            // 'username' => $this->string(20)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'profile_pic' => $this->string(),
            'role_id' => $this->integer(),
            'email' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%role}}', [
            'id' => $this->customPrimaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%draft_template}}', [
            'id' => $this->customPrimaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'file' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%param_list}}', [
            'id' => $this->customPrimaryKey(),
            'template_id' => $this->integer(),
            'param_id' => $this->integer(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%param_list_lookup}}', [
            'id' => $this->customPrimaryKey(),
            'name' => $this->string(),
            'param' => $this->string(),
            'type' => $this->string(),
            'hint' => $this->string(),
            'description' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%contract}}', [
            'id' => $this->customPrimaryKey(),
            'contract_no' => $this->string()->notNull(),
            'name' => $this->string(),
            'awarded_date' => $this->date(),
            'juu_staff_assigned' => $this->integer(),
            'department_staff_assigned' => $this->integer(),
            'template_id' => $this->integer(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%attachment}}', [
            'id' => $this->customPrimaryKey(),
            'contract_id' => $this->integer(),
            'name' => $this->string(),
            'file' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%draft_param}}', [
            'id' => $this->customPrimaryKey(),
            'draft_document_id' => $this->integer(),
            'param_list_id' => $this->integer(),
            'param' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);


        $this->createTable('{{%draft_status_lookup}}', [
            'id' => $this->customPrimaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%draft_status}}', [
            'id' => $this->customPrimaryKey(),
            'status_id' => $this->integer(),
            'draft_id' => $this->integer(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        /*
        1-step 1:2 hari surat niat dikemukakan kepada kontraktor - dari tarikh mesyuarat
        2-step 2:14 hari jabatan sediakan draf perjanjian - dari tarikh mesyuarat (serentak tempoh bantahan)
        3-step 7:30 hari kontraktor dtg untuk tandatangan dokumen - dari tarikh mesyuarat
        4-step 8:40 hari kontraktor perlu sediakan dokumen lengkap - dari tarikh tandatangan sst
        */
        $this->createTable('{{%date_setting}}', [
            'id' => $this->customPrimaryKey(),
            'name' => $this->string()->notNull(),
            'step_refference' => $this->string(),
            'duration' => $this->string(),
            'duration_type' => $this->string(),
            'from' => $this->string(),
            'description' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%draft_document}}', [
            'id' => $this->customPrimaryKey(),
            'name' => $this->string()->notNull(),
            'file' => $this->string(),
            // 'template_id' => $this->integer(),
            'contract_id' => $this->integer(),
            'draft_status_id' => $this->integer(),
            'description' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%note}}', [
            'id' => $this->customPrimaryKey(),
            'contract_id' => $this->integer(),
            'notes' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%document_checklist_lookup}}', [
            'id' => $this->customPrimaryKey(),
            'category' => $this->string(),
            // 'contract_id' => $this->integer(),
            'name' => $this->string(),
            'file' => $this->string(),
            'description' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%document_checklist}}', [
            'id' => $this->customPrimaryKey(),
            'doc_check_id' => $this->integer(),
            'contract_id' => $this->integer(),
            'file' => $this->string(),
            // 'name' => $this->string(),
            // 'file' => $this->string(),
            // 'required_date' => $this->date(),
            // 'submission_date' => $this->date(),
            // 'required' => $this->smallInteger()->defaultValue(0),
            'completed' => $this->smallInteger()->defaultValue(0),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'deleted_at' => $this->timestamp(),
            'deleted_by' => $this->integer()->defaultValue(0), //required for undo process
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);

        $i = 1;
        if ($this->db->driverName === 'oci') {
            $this->addPrimaryKey('pk'.$i++,'{{user}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{param_list}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{param_list_lookup}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{role}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%contract}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%draft_param}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%draft_template}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%draft_status_lookup}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%draft_status}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%date_setting}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%draft_document}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%attachment}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%note}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%document_checklist}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%document_checklist_lookup}}','id');
            $this->addPrimaryKey('pk'.$i++,'{{%settings}}','id');
        }
        //for profile and staff. only add if mysql
        $j = 1;
        if ($this->db->driverName === 'mysql') {
            $this->addForeignKey('fk'.$j++,'{{user}}','username','{{profile}}','staff_no');
            $this->addForeignKey('fk'.$j++,'{{contract}}','juu_staff_assigned','{{profile}}','staff_no');
            $this->addForeignKey('fk'.$j++,'{{contract}}','department_staff_assigned','{{profile}}','staff_no');
            $this->addForeignKey('fk'.$j++,'{{credential}}','staff_no','{{user}}','username');
        }

        $this->addForeignKey('fk'.$j++,'{{user}}','role_id','{{role}}','id');
        $this->addForeignKey('fk'.$j++,'{{param_list}}','template_id','{{draft_template}}','id');
        $this->addForeignKey('fk'.$j++,'{{param_list}}','param_id','{{param_list_lookup}}','id');
        $this->addForeignKey('fk'.$j++,'{{contract}}','template_id','{{draft_template}}','id');
        $this->addForeignKey('fk'.$j++,'{{draft_param}}','draft_document_id','{{draft_document}}','id');
        $this->addForeignKey('fk'.$j++,'{{draft_param}}','param_list_id','{{param_list}}','id');
        $this->addForeignKey('fk'.$j++,'{{draft_document}}','contract_id','{{contract}}','id');
        $this->addForeignKey('fk'.$j++,'{{draft_document}}','draft_status_id','{{draft_status}}','id');
        $this->addForeignKey('fk'.$j++,'{{draft_status}}','status_id','{{draft_status_lookup}}','id');
        $this->addForeignKey('fk'.$j++,'{{draft_status}}','draft_id','{{draft_document}}','id');
        $this->addForeignKey('fk'.$j++,'{{note}}','contract_id','{{contract}}','id');
        $this->addForeignKey('fk'.$j++,'{{attachment}}','contract_id','{{contract}}','id');
        $this->addForeignKey('fk'.$j++,'{{document_checklist}}','contract_id','{{contract}}','id');
        $this->addForeignKey('fk'.$j++,'{{document_checklist}}','doc_check_id','{{document_checklist_lookup}}','id');

        $this->createIndex('idx'.$j++,'{{user}}','username','unique');
        $this->createIndex('idx'.$j++,'{{user}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{user}}','status');
        $this->createIndex('idx'.$j++,'{{param_list}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{contract}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{contract}}','juu_staff_assigned');
        $this->createIndex('idx'.$j++,'{{contract}}','department_staff_assigned');
        $this->createIndex('idx'.$j++,'{{contract}}','status');
        $this->createIndex('idx'.$j++,'{{draft_param}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{draft_document}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{draft_status}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{draft_status_lookup}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{attachment}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{note}}','deleted_by');
        $this->createIndex('idx'.$j++,'{{document_checklist}}','deleted_by');


        $this->insertDefaultData();
        $this->insertScenario();
        // $this->createCustomView();

        if ($this->db->driverName === 'oci') {
            $alterIdentity = "
            select 'ALTER TABLE \"' || table_name || '\" MODIFY \"id\" GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE)' as sql from user_tables where table_name != 'migration'
            ";
            //drop for confirm
            $tables = Yii::$app->db->createCommand($alterIdentity)->queryAll();
            foreach ($tables as $table) {
                var_dump($table['SQL']);
                $fixTable = Yii::$app->db->createCommand($table['SQL'])->execute();
            }
        }
    }

    public function down()
    {
        $this->customDrop();
    }

// added function
    protected function insertDefaultData() {
        $i = 1;
        $this->batchInsert('{{%settings}}', ['id','key','value'],
            [
            [$i++,'simplifiedVersion',0,],
            [$i++,'templateDirectory','originalTemplate/template/',],
            [$i++,'minPasswordLength',4,],
            [$i++,'minStaffNoLength',4,],
            [$i++,'lateDraftWarningInDays',30,],
            [$i++,'departmentAlterLimit',2,],
            [$i++,'draftParamRequired',0,],
            [$i++,'defaultJuuStaffAssigned',7750,],
            ]
            );
        $i = 1;
        $this->batchInsert('{{%role}}', ['id','name'],
            [
            [$i++,'Super Admin',],
            [$i++,'Admin Undang-Undang',],
            [$i++,'Staf Undang-Undang',],
            [$i++,'Admin Jabatan',],
            [$i++,'Staf Jabatan',],
            [$i++,'Kontraktor',],
            [$i++,'Tetamu',],
            ]
            );

        $i = 1;
        $this->batchInsert('{{%draft_status_lookup}}', ['id','name'],
            [
            [$i++,'Semakan/Pindaan Peringkat Jabatan',],
            [$i++,'Pindah Tugas',],
            [$i++,'Semakan Jabatan Undang-Undang',],
            [$i++,'Surat pangilan kontraktor',],
            [$i++,'Kontraktor hadir tandatangan',],
            [$i++,'DYDP/SUP tandatangan perjanjian',],
            [$i++,'Pangilan ahli majlis untuk tandatangan',],
            [$i++,'Daftar masuk sistem stem (tarikh perjanjian)',],
            [$i++,'Menunggu kelulusan PDS untuk notis taksiran',],
            [$i++,'Hantar perjanjian ke lembaga hasil utk cetakan sijil adjudikasi (2-3hari)',],
            [$i++,'Kembalikan ke jabatan kontraktor dan perbendaharaan',],
            [$i++,'Simpanan JUU',],
            [$i++,'Selesai',],
            ]
            );




        /*
        1-step 1:2 hari surat niat dikemukakan kepada kontraktor - dari tarikh mesyuarat
        2-step 2:14 hari jabatan sediakan draf perjanjian - dari tarikh mesyuarat (serentak tempoh bantahan)
        3-step 7:30 hari kontraktor dtg untuk tandatangan dokumen - dari tarikh mesyuarat
        4-step 8:40 hari kontraktor perlu sediakan dokumen lengkap - dari tarikh tandatangan sst
        */
        $i=1;
        $this->batchInsert('{{%date_setting}}', ['id','name','step_refference','duration','duration_type','from','description'],
            [
            [$i++,'Tempoh Pengeluaran Surat Niat','Step 1','2','Hari','Tarikh Mesyuarat Lembaga','Surat niat dikemukakan kepada kontraktor'],
            [$i++,'Tempoh Penyediaan Draf Perjanjian','Step 2','14','Hari','Tarikh Mesyuarat Lembaga','Jabatan sediakan draf perjanjian'],
            [$i++,'Tempoh Bantahan Perlantikan Kontraktor','Step 6','14','Hari','Tarikh Mesyuarat Lembaga','Tempoh bantahan kontraktor dikemukakan'],
            [$i++,'Tempoh Kontraktor Tandatangan SST','Step 7','30','Hari','Tarikh Mesyuarat Lembaga','Kontraktor perlu hadir tandatangani surat setuju terima dan lain-lain dokumen'],
            [$i++,'Tempoh Peyediaan Dokumen Dari Kontraktor','Step 8','40','Hari','Tarikh Tandatangan SST','Kontraktor perlu sediakan dokumen lengkap'],
            ]
            );
        $i=1;
        $this->batchInsert('{{%draft_template}}', ['id','name','file','description'],
            [
            [$i++,'TEMPLATE BEKALAN BON','originalTemplate/template/TEMPLATE BEKALAN BON.docx','Bekalan ada bon'],
            [$i++,'TEMPLATE BEKALAN-TIADA BON','originalTemplate/template/TEMPLATE BEKALAN-TIADA BON.docx','Bekalan tiada bon'],
            [$i++,'TEMPLATE PENYELENGGARAAN','originalTemplate/template/TEMPLATE PENYELENGGARAAN.docx','Penyelenggaraan'],
            [$i++,'TEMPLATE PERKHIDMATAN BON','originalTemplate/template/TEMPLATE PERKHIDMATAN BON.docx','Perkhidmatan ada bon'],
            [$i++,'TEMPLATE PERKHIDMATAN-TIADA  BON','originalTemplate/template/TEMPLATE PERKHIDMATAN-TIADA  BON.docx','Perkhidmatan tiada bon'],
            [$i++,'TEMPLATE Sistem-Bangun','originalTemplate/template/TEMPLATE Sistem-Bangun.docx','Pembangunan sistem'],
            [$i++,'TEMPLATE Sistem-Selenggara','originalTemplate/template/TEMPLATE Sistem-Selenggara.docx','Selenggara sistem'],
            ]
            );
        // $i=1;
        // $this->batchInsert('{{%approve_level}}', ['id','draft_template_id','approve_level'],
        //     [
        //     [$i++,1,'Pengesahan ketua jabatan'],
        //     [$i++,1,'Pengesahan pengarah'],
        //     [$i++,2,'Pengesahan ketua jabatan'],
        //     [$i++,2,'Pengesahan pengarah'],
        //     [$i++,3,'Pengesahan ketua jabatan'],
        //     [$i++,3,'Pengesahan pengarah'],
        //     [$i++,4,'Pengesahan pengarah'],
        //     ]
        //     );

        $i=1;
        $this->batchInsert('{{%document_checklist_lookup}}', ['id','category','description'],
            [
            [$i++,'','Draf Perjanjian yang lengkap dengan lampiran-lampiran'],
            // [$i++,'Nota liputan (Cover note):-'],
            [$i++,'Nota liputan (Cover note):-','Insurans Pampasan Pekerja (Workmen’s Compensation)'],
            [$i++,'Nota liputan (Cover note):-','Insurans Tanggungan Awam (Public Liability)'],
            [$i++,'Nota liputan (Cover note):-','Insurans Kebakaran dan Kerosakan Harta'],
            [$i++,'Nota liputan (Cover note):-','Insurans “All Risks”'],
            // [$i++,'Dokumen Kelulusan:-'],
            [$i++,'Dokumen Kelulusan:-','Lembaga Tawaran / Jawatankuasa Sebutharga / Lembaga Perolehan'],
            [$i++,'Dokumen Kelulusan:-','Mesyuarat Majlis Penuh / Mesyuarat berkaitan'],
            [$i++,'Dokumen Kelulusan:-','Kelulusan YDP / SU'],

            [$i++,'','Surat Tawaran dan Surat Setujuterima'],
            [$i++,'','Surat Akuan Pembida'],
            [$i++,'','Bon Pelaksanaan / Wang Cagaran'],

            // [$i++,'Sijil Pendaftaran'],
            // [$i++,'SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)'],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','BORANG 9 dan/atau BORANG 13  “Certificate Of Incorporation Of Private Company” (SSM)'],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','BORANG 49  “Return Giving Particulars In Register Of Directors, Managers And Secretaries And Changes Of Particulars”'],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','Memorandum  Of  Association  '],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','Article Of  Association'],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','Resolusi Lembaga Pengarah Syarikat (Director’s Resolution) menurut Article Of  Association syarikat dimana hanya seorang pengarah diberi kuasa untuk tandatangan bagi pihak syarikat & salinan kad pengenalan pengarah tersebut.'],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','BORANG 11 “Notice Of Resolution”'],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','BORANG 24 “Return Of Allotment Of Shares”'],
            [$i++,'Sijil Pendaftaran - SYARIKAT (SENDIRIAN BERHAD ATAU BERHAD)','BORANG 44  “Notice Of Situation Of Registered Office And Office Hours And Particulars Of Changes”'],
            // [$i++,'BADAN PERNIAGAAN (Perseorangan, kumpulan, enterprise dll)'],
            [$i++,'Sijil Pendaftaran - BADAN PERNIAGAAN (Perseorangan, kumpulan, enterprise dll)','BORANG D atau  E  Perakuan Pendaftaran Perniagaan '],
            [$i++,'Sijil Pendaftaran - BADAN PERNIAGAAN (Perseorangan, kumpulan, enterprise dll)','Borang Maklumat Perniagaan '],
            [$i++,'Sijil Pendaftaran - BADAN PERNIAGAAN (Perseorangan, kumpulan, enterprise dll)','Borang Maklumat Pemilik'],
            [$i++,'','Sijil Pendaftaran - Pertubuhan'],
            [$i++,'','Sijil Pendaftaran - Koperasi'],
            [$i++,'','Sijil Pendaftaran - Lain-lain bentuk pertubuhan atau persatuan bersama-sama bukti pendaftaran.'],
            // [$i++,'Pengarah Syarikat / pemilik Perniagaan/ Pertubuhan/ Koperasi  dsb (Kontraktor / Penyewa / Pembekal yang akan menandatangani dokumen perjanjian)'],
            [$i++,'Pengarah Syarikat / pemilik Perniagaan/ Pertubuhan/ Koperasi  dsb (Kontraktor / Penyewa / Pembekal yang akan menandatangani dokumen perjanjian)','Salinan kad pengenalan orang yang akan tandatangan bagi pihak syarikat. '],
            [$i++,'Pengarah Syarikat / pemilik Perniagaan/ Pertubuhan/ Koperasi  dsb (Kontraktor / Penyewa / Pembekal yang akan menandatangani dokumen perjanjian)','Saksi (mesti merupakan salah seorang pengarah atau kakitangan syarikat / perniagaan) perlukan salinan caruman socso yang terkini dan salinan kad pengenalan.'],
            // [$i++,'Bagi Individu / Penyewa'],
            [$i++,'Bagi Individu / Penyewa','Salinan kad pengenalan individu/ penyewa'],
            [$i++,'Bagi Individu / Penyewa','Saksi mestilah mempunyai hubungan persaudaraan/ keluarga/ perkahwinan/ pekerja (disokong dengan bukti caruman socso) dan salinan kad pengenalan.'],
            [$i++,'','Borang Sebut Harga / Borang Tender yang lengkap'],
            // [$i++,'Sijil / Lesen'],
            [$i++,'Sijil / Lesen','Pusat Khidmat Kontraktor (PKK)'],
            [$i++,'Sijil / Lesen','Lembaga Pembangunan Industri Pembinaan Malaysia (CIDB)'],
            [$i++,'Sijil / Lesen','Kementerian Kewangan Malaysia'],
            [$i++,'','Pelan Lantai'],
            [$i++,'','Pelan Tapak'],
            [$i++,'','Syarat-syarat pelan kebenaran merancang berhubung penyerahan tapak dan bangunan kepada Majlis'],
            [$i++,'','Senarai inventori harta Majlis'],
            [$i++,'','Salinan carian / hakmilik tanah'],
            ]
            );



        $this->batchInsert('{{%param_list_lookup}}', ['id','name','param'],
            [
                // general
                [1,'Nama Syarikat','companyName'],
                [2,'No Pendaftaran Syarikat','companyRegNo'],
                [3,'Tajuk Kontrak','contractTitle'],
                [4,'No Kontrak','contractNo'],
                [5,'Tahun','currentYear'],
                [6,'Keterangan Kontrak','jobDescription'],
                [7,'Tarikh Mula Perjanjian','agreementStartDate'],
                [8,'Tarikh Tamat Perjanjian','agreementEndDate'],
                [9,'Tarikh Mula Kontrak','contractStartDate'],
                [10,'Tarikh Tamat Kontrak','contractEndDate'],
                [11,'Kos','cost'],
                [12,'Kos Dalam Perkataan','costText'],
                [13,'Durasi Kontrak','contractDuration'],
                [14,'Durasi Penghantaran','deliveryDuration'],

                [15,'Durasi `Initial Response Time`','irtDuration'],
                [16,'Durasi `Response Time`','rtDuration'],
                [17,'Durasi Jaminan','warrantyDuration'],
                [18,'Durasi Jaminan Kecatatan','deficiencyDuration'],
                [19,'Bon','bon'],
                [20,'Bon Dalam Perkataan','bonText'],
                [21,'Jaminan','compensation'],
                [22,'Jaminan Dalam Perkataan','compensationText'],
                [23,'Alamat Syarikat','companyAddress'],
                [24,'No Tel Syarikat','companyContactNo'],
                [25,'No Fax Syarikat','companyFaxNo'],
                [26,'Penama 1','contractorName1'],
                [27,'Ic Penama 1','contractorIc1'],
                [28,'Jawatan Penama 1','contractorPosition1'],
                [29,'Penama 2','contractorName2'],
                [30,'Ic Penama 2','contractorIc2'],
                [31,'Jawatan Penama 2','contractorPosition2'],

                [32,'Pampasan','penalty'],
            ]
            );
        $i = 1;
        $this->batchInsert('{{%param_list}}', ['id','template_id','param_id'],
            [
                // template bekalan bon
                [$i++,1,1],
                [$i++,1,2],
                [$i++,1,3],
                [$i++,1,4],
                [$i++,1,5],
                [$i++,1,6],
                [$i++,1,7],
                [$i++,1,8],
                [$i++,1,9],
                [$i++,1,10],
                [$i++,1,11],
                [$i++,1,12],
                [$i++,1,13],
                [$i++,1,14],
                [$i++,1,15],
                [$i++,1,16],
                [$i++,1,17],
                [$i++,1,18],
                [$i++,1,19],
                [$i++,1,20],
                [$i++,1,21],
                [$i++,1,22],
                [$i++,1,23],
                [$i++,1,24],
                [$i++,1,25],
                [$i++,1,26],
                [$i++,1,27],
                [$i++,1,28],
                [$i++,1,29],
                [$i++,1,30],
                [$i++,1,31],

                // template bekalan tiada bon
                [$i++,2,1],
                [$i++,2,2],
                [$i++,2,3],
                [$i++,2,4],
                [$i++,2,5],
                [$i++,2,6],
                [$i++,2,7],
                [$i++,2,8],
                [$i++,2,9],
                [$i++,2,10],
                [$i++,2,11],
                [$i++,2,12],
                [$i++,2,13],
                [$i++,2,14],
                [$i++,2,32],

                // template penyelenggaraan
                [$i++,3,1],
                [$i++,3,2],
                [$i++,3,3],
                [$i++,3,4],
                [$i++,3,5],
                [$i++,3,6],
                [$i++,3,7],
                [$i++,3,8],
                [$i++,3,9],
                [$i++,3,10],
                [$i++,3,11],
                [$i++,3,12],
                [$i++,3,13],
                [$i++,3,14],

                // template perkhimatan bon
                [$i++,4,1],
                [$i++,4,2],
                [$i++,4,3],
                [$i++,4,4],
                [$i++,4,5],
                [$i++,4,6],
                [$i++,4,7],
                [$i++,4,8],
                [$i++,4,9],
                [$i++,4,10],
                [$i++,4,11],
                [$i++,4,12],
                [$i++,4,13],
                [$i++,4,14],

                // template perkhimatan tiada bon
                [$i++,5,1],
                [$i++,5,2],
                [$i++,5,3],
                [$i++,5,4],
                [$i++,5,5],
                [$i++,5,6],
                [$i++,5,7],
                [$i++,5,8],
                [$i++,5,9],
                [$i++,5,10],
                [$i++,5,11],
                [$i++,5,12],
                [$i++,5,13],
                [$i++,5,14],

                // template sistem-bangun
                [$i++,6,1],
                [$i++,6,2],
                [$i++,6,3],
                [$i++,6,4],
                [$i++,6,5],
                [$i++,6,6],
                [$i++,6,7],
                [$i++,6,8],
                [$i++,6,9],
                [$i++,6,10],
                [$i++,6,11],
                [$i++,6,12],
                [$i++,6,13],
                [$i++,6,14],

                // template sistem-selenggara
                [$i++,7,1],
                [$i++,7,2],
                [$i++,7,3],
                [$i++,7,4],
                [$i++,7,5],
                [$i++,7,6],
                [$i++,7,7],
                [$i++,7,8],
                [$i++,7,9],
                [$i++,7,10],
                [$i++,7,11],
                [$i++,7,12],
                [$i++,7,13],
                [$i++,7,14],
            ]
            );
        // $i = 1;
        // $this->batchInsert('{{%contract}}', ['id','template_id','name','contract_no','juu_staff_assigned','created_at'],
        //     [
        //     [$i++,1,'MEMBEKAL SERVER BARU','A-09876789','11822',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,2,'SERVIS SERVER BARU','A-0987678','11822',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     // [$i++,1,'K09/2017'],
        //     // [$i++,1,],
        //     // [$i++,1,'MEMBEKAL SERVER BARU'],
        //     ]
        //     );

        // $i = 1;
        // $this->batchInsert('{{%draft_document}}', ['id','contract_id','name','created_at'],
        //     [
        //     [$i++,1,'K09/2017',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,2,'K10/2017',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     ]
        //     );
        // $i = 1;
        // $this->batchInsert('{{%draft_param}}', ['id','draft_document_id','param_list_id','param','created_at'],
        //     [
        //     [$i++,1,1,'MUSAWWIR TRADING',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,1,2,'A-09876789',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,1,3,'MEMBEKAL SERVER BARU',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,1,4,'K09/2017',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,2,1,'MUSAWWIR TRADING',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,2,2,'A-9876789',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,2,3,'SERVIS SERVER BARU',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     [$i++,2,4,'K10/2017',new \yii\db\Expression('CURRENT_TIMESTAMP')],
        //     ]
        //     );


    }
    protected function createCustomView() {
        if ($this->db->driverName === 'oci') {

            $monthReportSql = "";
            $monthReport = Yii::$app->db->createCommand($monthReportSql);
            $monthReport->execute();

            $yearReportSql = "";

            $yearReport = Yii::$app->db->createCommand($yearReportSql);
            $yearReport->execute();
        }
    }
    protected function insertScenario()
    {
        if($this->db->driverName == 'oci') {
            $u = ['7750','11822','4889','10001','10015'];
            $r = 1;
            $u = [
                ['7750','ROSNADA BINTI ABU HASSAN','PEGAWAI UNDANG-UNDANG','JAB UNDANG-UNDANG'],
                ['11822','FAIZAL BIN ABU BAKAR','PENOLONG PEGAWAI UNDANG-UNDANG','JAB UNDANG-UNDANG'],
                ['4889','HASNAH BINTI JUNID','PEMBANTU TADBIR','JAB UNDANG-UNDANG'],
                ['10001','AHMAD FAUZI BIN JOHARI','PEMBANTU KESIHATAN AWAM','JAB PERKHIDMATAN PERBANDARAN'],
                ['10015','AZHAR BIN ABU BAKAR','PEMBANTU KESIHATAN AWAM','JAB PERKHIDMATAN PERBANDARAN'],
            ];

            foreach ($u as $key => $value) {
                # code...
            $this->insert('{{%user}}', [
                'username' => $value[0],
                'auth_key' => \Yii::$app->security->generateRandomString(),
                'role_id' => $key+1,
                'email' => $value[0].'@mail.com',
                'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
                'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
                ]);

                if ($this->db->driverName !== 'oci') {
                    $this->insert('{{%profile}}', [
                        'name' => $value[1],
                        'staff_no' => $value[0],
                        'position' => $value[2],
                        'department' => $value[3],
                        ]);
                    $this->insert('{{%credential}}', [
                        'staff_no' => $value[0],
                        'password' => 'admin1',
                        ]);

                }
            }

        }
        if($this->db->driverName !== 'oci') {
            $this->insert('{{%user}}', [
                'username' => 10000,
                'auth_key' => \Yii::$app->security->generateRandomString(),
                'role_id' => 1,
                'email' => 'admin1@mail.com',
                'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
                'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ]);

            $this->insert('{{%profile}}', [
                'name' => 'ADMIN',
                'staff_no' => 10000,
                'position' => 'PENOLONG PEGAWAI KANAN',
                'department' => 'JAB UNDANG-UNDANG',
                ]);
            $this->insert('{{%credential}}', [
                'staff_no' => 10000,
                'password' => 'admin1',
                ]);


            $profileFaker = [
                ['PEMBANTU TADBIR', 'JAB PERBENDAHARAAN'],
                ['PEMBANTU TADBIR', 'JAB BANGUNAN'],
                ['PENOLONG PEGAWAI TADBIR', 'JAB KEJURUTERAAN'],
                ['PEMBANTU KEMAHIRAN', 'JAB KEJURUTERAAN'],
                ['PEMBANTU TADBIR KANAN', 'JAB PERBENDAHARAAN'],
                ['PENOLONG PEGAWAI TADBIR', 'JAB KEMASYARAKATAN'],
                ['PENGARAH KHIDMAT PENGURUSAN', 'JAB KHIDMAT PENGURUSAN'],
                ['PEMBANTU TADBIR', 'JAB PERANCANG BANDAR '],
                ['PEN. PEGAWAI PERANCANG BANDAR & DESA', 'JAB PERANCANG BANDAR '],
                ['PEGAWAI KEBUDAYAAN ', 'JAB KEMASYARAKATAN'],
                ['PEMBANTU TADBIR KANAN', 'JAB PELESENAN'],
                ['PENOLONG PEGAWAI SENIBINA', 'JAB PERANCANG BANDAR '],
                ['PEMBANTU TADBIR', 'JAB UNDANG-UNDANG'],
                ['PENOLONG PEGAWAI UNDANG-UNDANG', 'JAB UNDANG-UNDANG'],
                ['PENOLONG JURUTERA', 'JAB KEJURUTERAAN'],
                ['PEMBANTU PENGUATKUASA', 'DIREKTORAT PENGUATKUASAAN'],
                ['PEGAWAI TADBIR', 'JAB PERKHIDMATAN PERBANDARAN'],
                ['PEMBANTU TADBIR', 'DIREKTORAT PENGUATKUASAAN'],
                ['PEMBANTU TADBIR RENDAH', 'JAB BANGUNAN'],
                ['PEMBANTU TADBIR', 'JAB KEJURUTERAAN'],
                ['PEMBANTU PENGUATKUASA KANAN', 'DIREKTORAT PENGUATKUASAAN'],
                ['PEN. PEG. KES. PERSEKITARAN', 'JAB PELESENAN'],
                ['PEN. PEG. KES. PERSEKITARAN', 'JAB PERKHIDMATAN KESIHATAN'],
                ['PEMBANTU TADBIR', 'JAB KHIDMAT PENGURUSAN'],
                ['PENOLONG PEGAWAI SENIBINA', 'JAB BANGUNAN'],
                ['PENGAWAL KESELAMATAN', 'DIREKTORAT PENGUATKUASAAN'],
                ['PENOLONG ARKITEK LANDSKAP', 'JAB LANDSKAP'],
                ['PEGAWAI TEKNOLOGI MAKLUMAT', 'JAB KHIDMAT PENGURUSAN'],
            ];

            $faker = \Faker\Factory::create();
            // $profile = [];
            for ($i = 0; $i < 100; ++$i)
            {
                $key = array_rand($profileFaker, 1);
                $staffNo = $faker->biasedNumberBetween($min = 10001, $max = 50000);
                $name = $faker->name;
                $profile[$i] = array_merge($profileFaker[$key],[$faker->name,$staffNo]);
                if($profileFaker[$key][1]  === 'JAB UNDANG-UNDANG') {
                    $user[$i] = [$staffNo,\Yii::$app->security->generateRandomString(),rand(2,3),"email$i@mail.com",new \yii\db\Expression('CURRENT_TIMESTAMP'),new \yii\db\Expression('CURRENT_TIMESTAMP')];
                } else {
                    $user[$i] = [$staffNo,\Yii::$app->security->generateRandomString(),rand(4,5),"email$i@mail.com",new \yii\db\Expression('CURRENT_TIMESTAMP'),new \yii\db\Expression('CURRENT_TIMESTAMP')];
                }
                $credential[$i] = [$staffNo,'admin1'];
            }
            $this->batchInsert('{{%profile}}',['position','department','name','staff_no'],$profile);
            $this->batchInsert('{{%user}}',['username','auth_key','role_id','email','created_at','updated_at'],$user);
            $this->batchInsert('{{%credential}}',['staff_no','password'],$credential);

        }
    }
    protected function insertScenario2()
    {




        // ,\n\s+'\w+'\s=>\s'
         // => \$this->([a-z()->]+)\n\s+
        $a = 1;
        // $this->batchInsert('{{%contractor}}', ['id','name','reg_no','address1','address2','poscode','district1','district2','state','contact_no','fax_no'],
        //     [
        //     [$com++,'MUSAWWIR TRADING','AS0148834-M','No. 7','Lebuh Kampung Benggali','12000','Butterworth','Seberang Perai Utara','Pulau Pinang','012-4780614 / 04-3334296 / 04-3332696 / \n\t\t04-3237105','04-3326512'],
        //     [$con++,'S0116002(M)','MEMBEKAL ALAT TULIS KEPADA MPSP UNTUK TEMPOH SETAHUN','membekal Barang-Barang',$com,300.00,'Ringgit Malaysia Tiga Ratus',24943.92,'Ringgit Malaysia  Dua Puluh Empat Ribu Sembilan Ratus Empat Puluh Tiga Dan Sen Sembilan Puluh Dua','membekal Barang-Barang','satu (1) tahun','setahun','20','Dua Puluh (20) hari','2016-05-05','2017-05-04'],
        //     ]
        // $this->batchInsert('{{%contract}}', ['id','name','reg_no','address1','address2','poscode','district1','district2','state','contact_no','fax_no'],
        //     [
        //     [$com++,'MUSAWWIR TRADING','AS0148834-M','No. 7','Lebuh Kampung Benggali','12000','Butterworth','Seberang Perai Utara','Pulau Pinang','012-4780614 / 04-3334296 / 04-3332696 / \n\t\t04-3237105','04-3326512'],
        //     [$con++,'S0116002(M)','MEMBEKAL ALAT TULIS KEPADA MPSP UNTUK TEMPOH SETAHUN','membekal Barang-Barang',$com,300.00,'Ringgit Malaysia Tiga Ratus',24943.92,'Ringgit Malaysia  Dua Puluh Empat Ribu Sembilan Ratus Empat Puluh Tiga Dan Sen Sembilan Puluh Dua','membekal Barang-Barang','satu (1) tahun','setahun','20','Dua Puluh (20) hari','2016-05-05','2017-05-04'],
        //     ]
        //     );


            // 'id','contract_no','name','description','company_id','bon','bon_text','compensation','compensation_text','compensation_type','penalty','penalty_text','penalty_type','cost','cost_text','description','contract_duration','contract_duration_text','contract_duration_type','delivery_duration','delivery_duration_text','delivery_duration_type','agreement_start_date','agreement_end_date','expected_start_date','expected_end_date','start_date','end_date',//for template 1
        $i = 1;
        $this->insert('{{%contractor}}', [
            'id' => $i,
            'name' => 'MUSAWWIR TRADING',
            'reg_no' => 'AS0148834-M',
            'address1' => 'No. 7',
            'address2' => 'Lebuh Kampung Benggali',
            'poscode' => '12000',
            'district1' => 'Butterworth',
            'district2' => 'Seberang Perai Utara',
            'state' => 'Pulau Pinang',
            'contact_no' => '012-4780614 / 04-3334296 / 04-3332696 / \n\t\t04-3237105',
            'fax_no' => '04-3326512',

            ]);
        $this->insert('{{%contractor_staff}}', [
            'id' => 1,
            'name' => 'ANIZ FATHIMAH BINTI GHANI',
            'ic' => '730806-07-5656',
            'position' => 'Penolong Pengurus',
            'company_id' => $i,
            ]);
        $this->insert('{{%contractor_staff}}', [
            'id' => 2,
            'name' => 'SALIM BIN YAHAYA',
            'ic' => '560820-07-5717',
            'position' => 'Pengurus',
            'company_id' => $i,
            ]);
        $this->insert('{{%contract}}', [
            'id' => 1,
            'contract_no' => 'S0116002(M)',
            'name' => 'MEMBEKAL ALAT TULIS KEPADA MPSP UNTUK TEMPOH SETAHUN',
            'description' => 'membekal Barang-Barang',
            'company_id' => $i,
            'bon' => 300.00,
            'bon_text' => 'Ringgit Malaysia Tiga Ratus',
            'cost' => 24943.92,
            'cost_text' => 'Ringgit Malaysia  Dua Puluh Empat Ribu Sembilan Ratus Empat Puluh Tiga Dan Sen Sembilan Puluh Dua',
            'description' => 'membekal Barang-Barang',
            'contract_duration' => '1',
            'contract_duration_text' => 'satu',
            'contract_duration_type' => 'tahun',
            'delivery_duration' => '20',
            'delivery_duration_text' => 'Dua Puluh',
            'delivery_duration_type' => 'hari',
            'expected_start_date' => '2016-05-05',
            'expected_end_date' => '2017-05-04',
            ]);

        //for template 2
        $i++;
        $this->insert('{{%contractor}}', [
            'id' => $i,
            'name' => 'KULIM ADVANCED TECHNOLOGIES SDN. BHD.',
            'reg_no' => '373476-T',
            'address1' => '1st Floor',
            'address2' => 'Pusat Teknologi Maklumat',
            'poscode' => '09000',
            'district1' => 'Kulim High-Tech Park',
            'district2' => 'Kulim',
            'state' => 'Kedah',
            'contact_no' => '019-9507070 / 04-4031633 / 04-4031634 / 04-4031635',
            'fax_no' => '04-4031628',

            ]);
        $this->insert('{{%contractor_staff}}', [
            'id' => 3,
            'name' => 'AHMAD MOKHTAR BIN AZIZAN',
            'ic' => '620614-07-5195',
            'position' => 'Pengurus Besar',
            'company_id' => $i,
            ]);
        $this->insert('{{%contractor_staff}}', [
            'id' => 4,
            'name' => 'ANNUAR BIN MOHD SAFFAR',
            'ic' => '620801-02-5177',
            'position' => 'Pengarah',
            'company_id' => $i,
            ]);
        $this->insert('{{%contract}}', [
            'id' => 2,
            'contract_no' => 'IT-14-2016',
            'name' => 'MEMBEKAL PERALATAN VPN MODEL PEPLINK BALANCE 30',
            'description' => 'membekal peralatan',
            'company_id' => $i,
            // 'bon' => ,
            // 'bon_text' => '',
            'compensation' => '',
            'compensation_text' => '',
            'cost' => '',
            'cost_text' => '',
            'contract_duration' => '30',
            'contract_duration_text' => 'tiga puluh',
            'contract_duration_type' => 'hari',
            'delivery_duration' => '',
            'delivery_duration_text' => '',
            'agreement_start_date' => '2016-05-02',
            'agreement_end_date' => '2016-05-31',
            'expected_start_date' => '2016-05-02',
            'expected_end_date' => '2016-05-31',
            ]);

        // $i++;
        // $this->insert('{{%contractor}}', [
        //     'id' => $i,
        //     'name' => '',
        //     'reg_no' => '',
        //     'address1' => '',
        //     'address2' => '',
        //     'poscode' => '',
        //     'district1' => '',
        //     'district2' => '',
        //     'state' => '',
        //     'contact_no' => '',
        //     'fax_no' => '',

        //     ]);
        // $this->insert('{{%contractor_staff}}', [
        //     'id' => ,
        //     'name' => '',
        //     'ic' => '',
        //     'position' => '',
        //     'company_id' => $i,
        //     ]);
        // $this->insert('{{%contractor_staff}}', [
        //     'id' => ,
        //     'name' => '',
        //     'ic' => '',
        //     'position' => '',
        //     'company_id' => $i,
        //     ]);
        // $this->insert('{{%contract}}', [
        //     'id' => ,
        //     'contract_no' => '',
        //     'name' => '',
        //     'description' => '',
        //     'company_id' => $i,
        //     'bon' => ,
        //     'bon_text' => '',
        //     'cost' => ,
        //     'cost_text' => '',
        //     'description' => '',
        //     'contract_duration' => '',
        //     'contract_duration_text' => '',
        //     'delivery_duration' => '',
        //     'delivery_duration_text' => '',
        //     'expected_start_date' => '',
        //     'expected_end_date' => '',
        //     ]);

    }



}



        // $profile = [
        //     ['PEMBANTU TADBIR', 'JAB PERBENDAHARAAN'],
        //     ['PEMBANTU TADBIR', 'JAB BANGUNAN'],
        //     ['PENOLONG PEGAWAI TADBIR', 'JAB KEJURUTERAAN'],
        //     ['PEMBANTU KEMAHIRAN', 'JAB KEJURUTERAAN'],
        //     ['PEMBANTU TADBIR KANAN', 'JAB PERBENDAHARAAN'],
        //     ['PENOLONG PEGAWAI TADBIR', 'JAB KEMASYARAKATAN'],
        //     ['PENGARAH KHIDMAT PENGURUSAN', 'JAB KHIDMAT PENGURUSAN'],
        //     ['PEMBANTU TADBIR', 'JAB PERANCANG BANDAR '],
        //     ['PEN. PEGAWAI PERANCANG BANDAR & DESA', 'JAB PERANCANG BANDAR '],
        //     ['PEGAWAI KEBUDAYAAN ', 'JAB KEMASYARAKATAN'],
        //     ['PEMBANTU TADBIR KANAN', 'JAB PELESENAN'],
        //     ['PENOLONG PEGAWAI SENIBINA', 'JAB PERANCANG BANDAR '],
        //     ['PEMBANTU TADBIR', 'JAB UNDANG-UNDANG'],
        //     ['PENOLONG PEGAWAI UNDANG-UNDANG', 'JAB UNDANG-UNDANG'],
        //     ['PENOLONG JURUTERA', 'JAB KEJURUTERAAN'],
        //     ['PEMBANTU PENGUATKUASA', 'DIREKTORAT PENGUATKUASAAN'],
        //     ['PEGAWAI TADBIR', 'JAB PERKHIDMATAN PERBANDARAN'],
        //     ['PEMBANTU TADBIR', 'DIREKTORAT PENGUATKUASAAN'],
        //     ['PEMBANTU TADBIR RENDAH', 'JAB BANGUNAN'],
        //     ['PEMBANTU TADBIR', 'JAB KEJURUTERAAN'],
        //     ['PEMBANTU PENGUATKUASA KANAN', 'DIREKTORAT PENGUATKUASAAN'],
        //     ['PEN. PEG. KES. PERSEKITARAN', 'JAB PELESENAN'],
        //     ['PEN. PEG. KES. PERSEKITARAN', 'JAB PERKHIDMATAN KESIHATAN'],
        //     ['PEMBANTU TADBIR', 'JAB KHIDMAT PENGURUSAN'],
        //     ['PENOLONG PEGAWAI SENIBINA', 'JAB BANGUNAN'],
        //     ['PENGAWAL KESELAMATAN', 'DIREKTORAT PENGUATKUASAAN'],
        //     ['PENOLONG ARKITEK LANDSKAP', 'JAB LANDSKAP'],
        //     ['PEGAWAI TEKNOLOGI MAKLUMAT', 'JAB KHIDMAT PENGURUSAN'],
        // ];

        // $faker = \Faker\Factory::create();

        // for ($i = 0; $i < 100; ++$i)
        // {
        //     $key = array_rand($profile, 1);
        //     $staffNo = $faker->biasedNumberBetween($min = 10000, $max = 50000);
        //     $name = $faker->name;
        //     $user[$i] = array_merge($profile[$key],[$faker->name,$staffNo]);

        // }
